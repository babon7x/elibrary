<?php 
namespace Add\Models; 
use Illuminate\Database\Eloquent\Model; 

class ProyekDetail extends Model 
{ 

protected $table="proyek_detail"; 
protected $fillable=["proyek_id","posisi","keahlian","jumlah_orang","created_by"];

public static function getTableName() { return (new self())->getTable();} 
public function proyek() { return $this->belongsTo(Proyek::class,"proyek_id"); } 

} 