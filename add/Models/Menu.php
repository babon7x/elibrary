<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
	protected $table='menu';
	protected $fillable=['urutan','url','nama','tipe','icon','color'];

	public static function getTableName()
	{
		return (new self())->getTable();
	}

}

