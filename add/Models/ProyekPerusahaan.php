<?php 
namespace Add\Models; 
use Illuminate\Database\Eloquent\Model; 

class ProyekPerusahaan extends Model 
{ 

protected $table="proyek_perusahaan"; 
protected $fillable=["proyek_id","nama","asing","indonesia","created_by"];

public static function getTableName() { return (new self())->getTable();} 
public function proyek() { return $this->belongsTo(Proyek::class,"proyek_id"); } 

} 