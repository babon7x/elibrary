<?php 
namespace Add\Models; 
use Illuminate\Database\Eloquent\Model; 

class Testing extends Model 
{ 

protected $table="testing"; 
protected $fillable=["a","b","created_by"];

public static function getTableName() { return (new self())->getTable();} 
} 