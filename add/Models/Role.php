<?php 
namespace Add\Models; 
use Illuminate\Database\Eloquent\Model; 

class Role extends Model 
{ 

protected $table="role"; 
protected $fillable=["nama","created_by"];

public static function getTableName() { return (new self())->getTable();} 
} 