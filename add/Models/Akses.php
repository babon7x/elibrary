<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class Akses extends Model
{
	protected $table='akses';
	protected $fillable=['users_id','akses'];

	public static function getTableName()
	{
		return (new self())->getTable();
	}

}

