<?php 
namespace Add\Models; 
use Illuminate\Database\Eloquent\Model; 

class Chart extends Model 
{ 

	protected $table="chart"; 
	protected $fillable=["user_id","proyek_id"];

	public static function getTableName() { return (new self())->getTable();} 
	public function proyek() { return $this->belongsTo(Proyek::class,"proyek_id"); } 

	public function user() { return $this->belongsTo(User::class,"user_id"); } 
} 