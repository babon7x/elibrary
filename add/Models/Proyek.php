<?php 
namespace Add\Models; 
use Illuminate\Database\Eloquent\Model; 

class Proyek extends Model 
{ 

	protected $table="proyek"; 
	protected $fillable=["pengguna_jasa","nama_paket_pekerjaan","upload_cover_proyek","upload_file_kontrak","lingkup_produk_utama","lokasi_proyek","nilai_kontrak","nomor_kontrak","waktu_pelaksanaan_mulai","waktu_pelaksanaan_selesai","nama_pimpinan_kontrak","alamat","negara_asal","created_by","top_proyek"];

	public static function getTableName() { return (new self())->getTable();} 
	public function perusahaanChild() {
		return $this->hasMany(ProyekPerusahaan::class, 'proyek_id')->orderBy('id', 'asc');
	}
	public function detailChild() {
		return $this->hasMany(ProyekDetail::class, 'proyek_id')->orderBy('id', 'asc');
	}
} 