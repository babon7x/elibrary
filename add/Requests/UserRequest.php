<?php

namespace Add\Requests;
use Illuminate\Foundation\Http\FormRequest;
class UserRequest extends FormRequest
{

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		if($this->method() == "POST"){
			return [
				"nama_lengkap" => "required",
				"perusahaan" => "required",
				'email' => 'required|unique:users,email'
			];
		}
		else{
			return [
				"nama" => "required",
			];
		}
	}

	public function messages()
	{
		return [
			"nama_lengkap.required" => "tidak boleh kosong !",
			"perusahaan.required" => "tidak boleh kosong !",
			"email.required" => "tidak boleh kosong !",
			"email.unique" => "email sudah terdaftar !",
		];
	}
}