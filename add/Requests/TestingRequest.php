<?php

namespace Add\Requests;
use Illuminate\Foundation\Http\FormRequest;
class TestingRequest extends FormRequest
{

public function authorize()
{
return true;
}

public function rules()
{
if($this->method() == "POST"){
return [
"a" => "required",
"b" => "required|date",
];
}
else{
return [
"a" => "required",
"b" => "required|date",
];
}
}

public function messages()
{
return [
"a.required" => "tidak boleh kosong !",
"b.required" => "tidak boleh kosong !",
 ];
}
}