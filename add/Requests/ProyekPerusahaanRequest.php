<?php

namespace Add\Requests;
use Illuminate\Foundation\Http\FormRequest;
class ProyekPerusahaanRequest extends FormRequest
{

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		if($this->method() == "POST"){
			return [
				"nama" => "required",
				"asing" => "required",
				"indonesia" => "required",
			];
		}
		else{
			return [
				"nama" => "required",
				"asing" => "required",
				"indonesia" => "required",
			];
		}
	}

	public function messages()
	{
		return [
			"nama.required" => "tidak boleh kosong !",
			"asing.required" => "tidak boleh kosong !",
			"indonesia.required" => "tidak boleh kosong !",
		];
	}
}