<?php

namespace Add\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MenuRequest extends FormRequest
{

  public function authorize()
  {
    return true;
  }

  public function rules()
  {

    if($this->method() == 'POST'){
      return [
        
        'url' => 'required',
        'nama' => 'required|unique:menu,nama'
      ];
    }
    else{
      return [
        'nama' => 'required',
        'nama' => 'required|unique:menu,nama,'.$this->get('id')
      ];
    }

  }

  public function messages()
  {
    return [
      
      'url.required' => 'tidak boleh kosong !',
      'nama.required' => 'tidak boleh kosong !',
      'nama.unique' => 'tidak boleh sama !',    ];
    }
  }
