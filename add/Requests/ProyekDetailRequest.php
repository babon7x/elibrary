<?php

namespace Add\Requests;
use Illuminate\Foundation\Http\FormRequest;
class ProyekDetailRequest extends FormRequest
{

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		if($this->method() == "POST"){
			return [
				"posisi" => "required",
				"keahlian" => "required",
				"jumlah_orang" => "required",
			];
		}
		else{
			return [
				"posisi" => "required",
				"keahlian" => "required",
				"jumlah_orang" => "required",
			];
		}
	}

	public function messages()
	{
		return [
			"posisi.required" => "tidak boleh kosong !",
			"keahlian.required" => "tidak boleh kosong !",
			"jumlah_orang.required" => "tidak boleh kosong !",
		];
	}
}