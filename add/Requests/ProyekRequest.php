<?php

namespace Add\Requests;
use Illuminate\Foundation\Http\FormRequest;
class ProyekRequest extends FormRequest
{

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		if($this->method() == "POST"){
			return [
				"pengguna_jasa" 			=> "required",
				"nama_paket_pekerjaan" 		=> "required",
				// "upload_cover_proyek" 		=> "required",
				// "upload_file_kontrak" 		=> "required",
				"lingkup_produk_utama" 		=> "required",
				"lokasi_proyek" 			=> "required",
				"nilai_kontrak" 			=> "required",
				"nomor_kontrak" 			=> "required",
				"waktu_pelaksanaan_mulai" 	=> "required|date",
				"waktu_pelaksanaan_selesai" => "required|date",
				"nama_pimpinan_kontrak" 	=> "required",
				"alamat" 					=> "required",
				"negara_asal" 				=> "required",
			];
		}
		else{
			return [
				"pengguna_jasa" => "required",
				"nama_paket_pekerjaan" => "required",
				// "upload_cover_proyek" => "required",
				// "upload_file_kontrak" => "required",
				"lingkup_produk_utama" => "required",
				"lokasi_proyek" => "required",
				"nilai_kontrak" => "required",
				"nomor_kontrak" => "required",
				"waktu_pelaksanaan_mulai" => "required|date",
				"waktu_pelaksanaan_selesai" => "required|date",
				"nama_pimpinan_kontrak" => "required",
				"alamat" => "required",
				"negara_asal" => "required",
			];
		}
	}

	public function messages()
	{
		return [
			"pengguna_jasa.required"=> "tidak boleh kosong !",
			"nama_paket_pekerjaan.required"=> "tidak boleh kosong !",
			// "upload_cover_proyek.required" => "tidak boleh kosong !",
			// "upload_cover_proyek.mimes" => "harus berupa docx atau pdf !",
			// "upload_file_kontrak.required" => "tidak boleh kosong !",
			// "upload_file_kontrak.mimes" => "harus berupa docx atau pdf !",
			"lingkup_produk_utama.required" => "tidak boleh kosong !",
			"lokasi_proyek.required" => "tidak boleh kosong !",
			"nilai_kontrak.required" => "tidak boleh kosong !",
			"nomor_kontrak.required" => "tidak boleh kosong !",
			"waktu_pelaksanaan_mulai.required" => "tidak boleh kosong !",
			"waktu_pelaksanaan_mulai.date" => "harus tanggal valid !",
			"waktu_pelaksanaan_selesai.required" => "tidak boleh kosong !",
			"waktu_pelaksanaan_selesai.date" => "harus tanggal valid !",
			"nama_pimpinan_kontrak.required" => "tidak boleh kosong !",
			"alamat.required" => "tidak boleh kosong !",
			"negara_asal.required" => "tidak boleh kosong !",

		];
	}
}