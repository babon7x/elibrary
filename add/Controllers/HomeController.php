<?php

namespace Add\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Add\Models\Proyek;
use Add\Models\ProyekDetail;
use Add\Models\Chart;
use App\Models\User;

class HomeController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
    // $page ='dashboard';
    // $user_id = Auth::id();
    // $topproyek=Proyek::where("is_deleted",0)->where('top_proyek',1)->orderBy("created_at", "desc")->get();
    // $top_proyek_count = 0;

    // $data_user= '';
    // $chart_count= 0;
    // $new_user=0;
    // if ($user_id !=''){
    //   $data_user = User::where('id',$user_id)->first();
    //   $chart_list = Chart::whereIn('user_id',[$user_id])->get();
    //   $chart_count = $chart_list->count();
    //   $top_proyek_list = Proyek::where('top_proyek',1)->get();
    //   $top_proyek_count = $top_proyek_list->count();
    //   $new_user_list = User::where('aktif',0)->where('is_deleted',0)->get();
    //   $new_user = $new_user_list->count();
    // }

    // return view('home.dashboard',compact('user_id','topproyek','data_user','page','chart_count','new_user','top_proyek_count'));
  // return view('/pdf.'.$user_id);
    return redirect('/dashboard');
  }

  public function list(Request $request)
  {
    $yearStart = $request->selectYearStart.'-01-01';
    $year = $request->selectYear.'-12-31';
    $dataSearch = $request->dataSearch;
    if ($dataSearch == ''){
      $list=Proyek::where("is_deleted",0)->whereBetween('waktu_pelaksanaan_mulai',[$yearStart,$year])->orderBy("waktu_pelaksanaan_mulai", "asc")->get();
    }
    else{
      $list=Proyek::where("is_deleted",0)->where('nama_paket_pekerjaan', 'LIKE', '%' . $dataSearch . '%')->orWhere('lokasi_proyek', 'LIKE', '%' . $dataSearch . '%')->orderBy("waktu_pelaksanaan_mulai", "asc")->get();
    }
    return DataTables()->of($list)->make(true);
  }

  public function toLogin(){
    return view('auth.login');
  }

  public function toRegister(){
    return view('auth.login');
  }

}
