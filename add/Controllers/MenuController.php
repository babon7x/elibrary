<?php

namespace Add\Controllers;

use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Add\Models\Menu;
use Add\Models\Akses;
use Add\Requests\MenuRequest;
use Illuminate\Support\Facades\Auth;
use Artisan;


class MenuController extends Controller
{
	public function index()
	{
		$nama_menu 	= Menu::getTableName();
		$db_name 	= DB::getDatabaseName();
		$db_name 	= 'Tables_in_' . $db_name;
		$tables 	= DB::select('SHOW TABLES');
		$datas 		= Menu::orderBy('created_at', 'asc')->get();

		$path 	= public_path() . '/../add/Views/template/sidebar.php';
		$sidebar = File::get($path);
		

		return view('Menu.index',compact('tables','db_name','datas','nama_menu','sidebar'));
	}

	public function store(MenuRequest $request)
	{
		$x = '$';
		$nama_model = $request->nama_model;

		$newMenu = new Menu;
		$newMenu->url = $request->url;
		$newMenu->nama = $request->nama;
		$newMenu->tipe = 'menu';
		$newMenu->icon = $request->icon;
		$newMenu->color = $request->color;
		$newMenu->save();

		$createRoute = createRoute($x, $request->url , $request->nama , $request->nama_model);
		$createRequest = createRequestFull($x, $request->table);
		$createModel = createModel($x, $request->table);
		$createMigration = createMigration($x, $request->table);
		if ($request->totaltable == 0){
			$createController = createControllerMaster($x, $request->table,$nama_model,$request->url);
			$createController = createViewMaster($x, $request->table,$nama_model,$request->url);
		}
		else{
			$createController = createController($x, $request->table,$nama_model,$request->url);
		}

		return response()->json($request->totaltable);
	}

	public function newSidebar(MenuRequest $request)
	{
		$newMenu = new Menu;

		$newMenu->url = $request->url;
		$newMenu->nama = $request->nama;
		$newMenu->tipe = 'blank';
		$newMenu->icon = $request->icon;
		$newMenu->color = $request->color;
		$newMenu->save();

		return response()->json($newMenu);
	}


	public function storeSidebar(Request $request)
	{

		$head = 
		'<div class="main-sidebar" style="z-index: 9999;">'. "\r\n" . "\r\n".
		'<aside id="sidebar-wrapper">'. "\r\n" . "\r\n".
		'<div class="sidebar-brand">'. "\r\n" . "\r\n".
		'<a href="index.html">SimpleDev</a>'. "\r\n" . "\r\n".
		'</div>'. "\r\n" . "\r\n".
		'<div class="sidebar-brand sidebar-brand-sm">'. "\r\n" . "\r\n".
		'<a href="index.html">SD</a>'. "\r\n" . "\r\n".
		'</div>'. "\r\n" . "\r\n".

		'<ul class="sidebar-menu">' . "\r\n" . "\r\n" ;
		
		$foot =  "\r\n" .'</ul>'. "\r\n" . "\r\n".
		'<div class="mt-4 mb-4 p-3 hide-sidebar-mini">'. "\r\n" . "\r\n".
		'<a href="#" class="btn btn-primary btn-lg btn-block btn-icon-split">'. "\r\n" . "\r\n".
		'<i class="fas fa-phone"></i> Contact Us'. "\r\n" . "\r\n".
		'</a>'. "\r\n" . "\r\n".
		'</div>'. "\r\n" . "\r\n".
		'</aside>'. "\r\n" . "\r\n".
		'</div>';

		$content = $head . $request->result . $foot;
		$path = public_path() . '/../add/Views/template/sidebar.blade.php';
		File::put($path, $content);

		$path = public_path() . '/../add/Views/template/sidebar.php';
		File::put($path, $request->sidebar);
		
		$response = ['sidebar' => $request->sidebar, 'result' => $request->result];
		return response()->json($response);
	}

	public function update(MenuRequest $request)
	{
		$datas = Menu::where('id', $request->id)->first();
		$update = Menu::where('id', $request->id)->update(['nama' => $request->nama, 'url' => $request->url, 'tipe' => $request->tipe,'color'=>$request->color,'icon'=>$request->icon]);
		if ($update) {
			$response = ['code' => 200, 'status' => 'success', 'message' => 'berhasil ubah', 'data' => $request->all()];
			if ($request->tipe != 'Blank') {
				$url = ucwords($datas->url);
				$url = preg_replace("/[^a-zA-Z]+/", "", $url);
				$new_url = ucwords($request->url);
				$new_url = preg_replace("/[^a-zA-Z]+/", "", $new_url);

				$path = public_path() . '/../routes/web.php';


				$all_route = File::get($path);

				$old_route = "
				Route::resource('/" . strtolower($url) . "', '" . $url . "Controller');
				Route::post('/" . strtolower($url) . "/list', '" . $url . "Controller@list');
				Route::post('/" . strtolower($url) . "/getdata', '" . $url . "Controller@getData');";

				$new_route = "
				Route::resource('/" . strtolower($new_url) . "', '" . $new_url . "Controller');
				Route::post('/" . strtolower($new_url) . "/list', '" . $new_url . "Controller@list');
				Route::post('/" . strtolower($new_url) . "/getdata', '" . $new_url . "Controller@getData');";

				$old_route_changed =

				$new_route = str_replace($old_route, $new_route, $all_route);
				File::put($path, $new_route);
			}
		} else {
			$response = ['code' => 999, 'status' => 'danger', 'message' => 'gagal ubah', 'data' => $request->all()];
		}
		return response()->json($response);
	}

	public function destroy(Request $request)
	{

		$url = $request->id;
		$namatable = $url; 
		$nama = str_replace("_", " ", $url);
		$nama = ucwords($nama);
		$nama = str_replace(" ", "_", $nama);
		$nama = ucwords($nama);
		$nama = preg_replace("/[^a-zA-Z0-9]+/", "", $nama);

		$path = public_path() . '/../routes/web.php';
		$old_route = File::get($path);

		$new_route =
		"Route::resource('/" . $namatable . "', '" . $nama . "Controller');" . "\r\n" .
		"Route::post('/" .  $namatable . "/list', '" . $nama . "Controller@list');" . "\r\n" .
		"Route::post('/" .  $namatable . "/getdata', '" . $nama . "Controller@getData');"."\r\n" ;
		$new_route = str_replace($new_route, "", $old_route);


		$nama2 = $url;
		$nama3 = str_replace(" ", "_", strtolower($url)).'_table';
		$nama4 = '_create_'.$nama3;

		$files = File::files(public_path() . '/../database/migrations/');

		$modelFile =  public_path() . '/../add/Models/'.$nama.'.php';
		$controllerFile =  public_path() . '/../add/Controllers/'.$nama.'Controller.php';
		$requestFile =  public_path() . '/../add/Requests/'.$nama.'Request.php';
		$viewFile =  public_path() . '/../add/Views/'.$url;

		$response = ['code' => 200, 'status' => 'success', 'message' => 'berhasil hapus'];
		
		$migrationFile =  public_path() . '/../database/migrations/';
		foreach($files as $key => $item){
			$file = pathinfo($item);
			$filename = $file['filename'];
			$namaDepan =  substr($filename, 0, 25);
			$namaBelakang =  substr($filename, 17, 200);
			if ($namaBelakang == $nama4){						
				File::delete($file);
			}
			$response['file'] = $namaDepan.$nama3;
			$response['target'] = $filename;
		}
		
		// File::delete($modelFile);
		// File::delete($controllerFile);
		// File::delete($requestFile);
		// File::deleteDirectory($viewFile);
		// File::put($path, $new_route);

		// DB::statement('drop table if exists '.$url);
		// DB::statement("delete from migrations where migration like '%_create_". $url ."_table%' ");
		
		// Artisan::call('optimize');

		// $delete = Menu::where('url', $request->id)->delete();
		return response()->json($nama4);
	}

	public function fixsidebar(Request $request) {
		$response = [];
		DB::statement('truncate menu');

		foreach ($request->data as $key => $value){
			if (array_key_exists('children', $value)){
				$store = Menu::create($value);

				foreach ($value['children'] as $key2 =>$value2){
					if (array_key_exists('children', $value2)){
						$store = Menu::create($value2);

						foreach ($value2['children'] as $key3 =>$value3){
							if (array_key_exists('children', $value3)){
								$store = Menu::create($value3);

								foreach ($value3['children'] as $key4 =>$value4){
									$store = Menu::create($value4);
								}
							}
							else{				
								$store = Menu::create($value3);
							}
						}
					}
					else{				
						$store = Menu::create($value2);
					}
				}
			}
			else{				
				$store = Menu::create($value);
			}

			// $store = Menu::create($value);
			// if ($store){
			// 	$response= 'success';
			// }
			// else{
			// 	$response='danger';
			// }

		}

		return $request->data;
	}


	public function getData(Request $request)
	{
		$datas = Menu::where('id', $request->id)->get();
		return response()->json($datas);
	}
}
