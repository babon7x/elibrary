<?php

namespace Add\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Add\Models\MyTestMail;
use Illuminate\Support\Facades\Mail;


use Add\Requests\UserRequest;

use App\Models\User;
use Add\Models\Chart;

class UserController extends Controller
{

	public function index()
	{
		$datas = '';
		$user_id = Auth::id();
		$chart_count=0;
		$new_user=0;
		$target_id = '';
		if ($user_id == ''){
			return redirect()->route('login');
		}
		else{
			$data_user = '';
			$data_user = User::where('id',$user_id)->where('is_deleted',0)->first();
			$pass = User::where('id',$user_id)->where('is_deleted',0)->first(); 
			$chart_list = Chart::whereIn('user_id',[$user_id])->get();
			$chart_count = $chart_list->count();

			$new_user_list = User::where('aktif',0)->where('is_deleted',0)->get();
			$new_user = $new_user_list->count();

			return view('user.index',compact('user_id','pass','data_user','datas','chart_count','new_user','target_id'));
		}
	}

	public function akun($id)
	{
		$datas = User::where('id',$id)->where('is_deleted',0)->first();
		$target_id = $datas->id;
		$user_id = Auth::id();
		$data_user = '';
		$data_user = User::where('id',$user_id)->where('is_deleted',0)->first();
		$chart_list = Chart::whereIn('user_id',[$user_id])->get();
		$chart_count = $chart_list->count();

		$new_user_list = User::where('aktif',0)->where('is_deleted',0)->get();
		$new_user = $new_user_list->count();

		return view('user.akun',compact('datas','user_id','data_user','chart_count','new_user','target_id'));
	}

	public function list(Request $request)
	{
		$list=User::where("id",'>',2)->where('is_deleted',0)->orderBy("created_at", "desc")->get();
		return DataTables()->of($list)->make(true);
	}

	public function store(UserRequest $request)
	{
		$store = User::create([
			'role_id' => $request['role_id'],
			'nama_lengkap' => $request['nama_lengkap'],
			'perusahaan' => $request['perusahaan'],
			'email' => $request['email'],
			'password' => Hash::make('ciriajasa'),
		]);

		return response()->json($store);
	}
	public function update(Request $request)
	{
		$data = $request->all();
		$data["updated_by"] = Auth::id();
		$update = User::where("id", $request->id)->update($data);
		return response()->json($update);
	}
	public function destroy(Request $request)
	{
		$id = $request->ids[0];
		$deleted_by = Auth::id();
		// $delete = User::whereIn("id", request("ids"))->update(["is_deleted"=>1,"aktif"=>0,"updated_by"=>$deleted_by]);
		$delete = User::whereIn("id", request("ids"))->delete();
		return response()->json($delete);
	}
	public function getData(Request $request)
	{
		$datas = User::where("id", $request->id)->where("is_deleted",0)->get();
		return response()->json($datas);
	}
	public function lupaSandi()
	{

		return view('auth.passwords.email');
	}
	public function ubahSandi(Request $request)
	{
		$user = User::findOrFail($request->id);
		if (!(Hash::check($request->sandi_saat_ini, $user->password))) {
			$response['status'] = 'Gagal';
			$response['icon'] = 'error';
			$response['pesan'] = 'Sandi Saat ini salah !';
			return response()->json($response);
		}

		if(strcmp($request->get('sandi_saat_ini'), $request->get('sandi_baru')) == 0){
			$response['status'] = 'Gagal';
			$response['icon'] = 'error';
			$response['pesan'] = 'Sandi baru tidak boleh sama dengan yang lama !';
			return response()->json($response);
		}
		if(!(strcmp($request->get('sandi_baru'), $request->get('sandi_baru_konfirm'))) == 0){
			$response['status'] = 'Gagal';
			$response['icon'] = 'error';
			$response['pesan'] = 'Sandi Baru tidak cocok dengan Konfirmasi Sandi Baru !';
			return response()->json($response);
		}

		$user->password = Hash::make($request->sandi_baru);
		$user->save();

		$response['status'] = 'Berhasil';
		$response['icon'] = 'success';
		$response['pesan'] = 'Sandi Berhasil di ubah , harap login kembali';
		return response()->json($response);
	}

	public function resetSandi(Request $request)
	{
		$user = User::Where('email',$request->email)->first();

		$response['status'] = 'success';

		if(!(strcmp($request->get('password2'), $request->get('password3'))) == 0){
			$response['status'] = 'Gagal';
			$response['icon'] = 'error';
			$response['pesan'] = 'Sandi Baru tidak cocok dengan Konfirmasi Sandi Baru !';
			return response()->json($response);
		}

		$user->password = Hash::make($request->password2);
		$user->save();

		$response['status'] = 'Berhasil';
		$response['icon'] = 'success';
		$response['pesan'] = 'Sandi Berhasil di ubah , silahkan login kembali';
		return response()->json($request);
	}

	public function approveUser(Request $request){

		$email = User::where('id',$request->ids)->first('email');
		User::where('id',$request->ids)->update(['aktif'=>1]);

		$new_user_list = User::where('aktif',0)->where('is_deleted',0)->get();
		$new_user = $new_user_list->count();

		$details = [
			'title' => 'Selamat akun anda sudah di aktifkan',
			'body' => 'silahkan klik ',
			'body2' => ' untuk mengakses elibrary.ciriajasa'
		];

		// \Mail::to($email)->send(new \Add\Models\MyTestMail($details));
		$data['details'] = $details;
		Mail::send('myTestMail', $data, function ($message) use ($email) {
            $message->to($email->email)
                ->subject('Elibrary');

        });


		return response()->json($new_user);
	}
}