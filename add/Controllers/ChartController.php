<?php

namespace Add\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Filesystem\Filesystem;
use File;

use Add\Requests\ProyekRequest;
use Add\Requests\ProyekDetailRequest;
use Add\Requests\ProyekPerusahaanRequest;

use Add\Models\Proyek;
use Add\Models\ProyekDetail;
use Add\Models\ProyekPerusahaan;
use Add\Models\Chart;
use App\Models\User;

class ChartController extends Controller
{

	public function index()
	{
		$page = 'chart';
		$user_id = Auth::id();   
		$data_user = '';
		$new_user = 0;
		$chart_count=0;
		$topproyek=Proyek::where("is_deleted",0)->where('top_proyek',1)->orderBy("created_at", "desc")->get();

		if ($user_id == ''){
			return redirect()->route('login');
		}
		else{
			$data_user = User::where('id',$user_id)->first();
			$chart_list=Chart::whereIn('user_id',[$user_id])->orderBy("created_at", "desc")->get();
			$chart_count = $chart_list->count();
			$new_user_list = User::where('aktif',0)->where('is_deleted',0)->get();
			$new_user = $new_user_list->count();
			return view('chart.index',compact('user_id','chart_list','page','data_user','chart_count','topproyek','new_user'));
		}
	}

	public function list(Request $request)
	{
		$user_id = Auth::id();
		$list=Chart::whereIn("user_id",[$user_id])->with('proyek')->orderBy("created_at", "desc")->get();
		return DataTables()->of($list)->make(true);
	}

	public function store(Request $request){
		$user_id = Auth::id();



		$DeleteChart = Chart::WhereIn('user_id',[$user_id])->whereIn('proyek_id',$request->ids)->delete();

		foreach ($request->ids as $key => $value){
			$store['user_id'] = $user_id;
			$store['proyek_id'] = $value;
			$ChartStore = Chart::create($store);
		}
		$chart_list=Chart::whereIn('user_id',[$user_id])->orderBy("created_at", "desc")->get();
		$chart_count = $chart_list->count();
		return response()->json($chart_count);
	}

	
	public function destroy(Request $request)
	{
		$id= $request->ids[0];
		$user_id = Auth::id();

		$delete = Chart::whereIn("id", request("ids"))->delete();
		$chart_list=Chart::whereIn('user_id',[$user_id])->orderBy("created_at", "desc")->get();
		$chart_count = $chart_list->count();
		return response()->json($chart_count);
	}

	public function getData(Request $request)
	{
		$datas = Chart::where("id",$request->id)->get();
		return response()->json($datas);
	}


}