<?php

namespace Add\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use DataTables;
use Response;
use File;
use PDF;
use Spipu\Html2Pdf\Html2Pdf;

use Illuminate\Support\Facades\Auth;
use Add\Models\Proyek;
use Add\Models\ProyekDetail;
use Add\Models\ProyekPerusahaan;
use Add\Models\Chart;
use App\Models\User;
use Dompdf\Options;
use Org_Heigl\Ghostscript\Ghostscript;

class DashboardController extends Controller
{

  public function updateDB(){
    DB::statement("ALTER TABLE proyek CHANGE pengguna_jasa pengguna_jasa LONGTEXT");
  }

  public function index()
  {
    $page ='dashboard';
    $user_id = Auth::id();
    $topproyek=Proyek::where("is_deleted",0)->where('top_proyek',1)->orderBy("waktu_pelaksanaan_mulai", "asc")->get();
    $top_proyek_count = 0;

    $data_user= '';
    $chart_count= 0;
    $new_user=0;
    if ($user_id !=''){
      $data_user = User::where('id',$user_id)->first();
      $chart_list = Chart::whereIn('user_id',[$user_id])->get();
      $chart_count = $chart_list->count();
      $top_proyek_list = Proyek::where('top_proyek',1)->get();
      $top_proyek_count = $top_proyek_list->count();
      $new_user_list = User::where('aktif',0)->where('is_deleted',0)->get();
      $new_user = $new_user_list->count();
    }

    return view('home.dashboard',compact('user_id','topproyek','data_user','page','chart_count','new_user','top_proyek_count'));
  // return public_path('cover_proyek');
  }

  public function list(Request $request)
  {
    $yearStart = $request->selectYearStart.'-01-01';
    $year = $request->selectYear.'-12-31';
    $dataSearch = $request->dataSearch;
    if ($dataSearch == ''){
      $list=Proyek::where(function($query) {
        $query->where("is_deleted",0);
      })
      ->whereBetween('waktu_pelaksanaan_mulai',[$yearStart,$year])->orderBy("waktu_pelaksanaan_mulai", "asc")->get();
    }
    else{
      $list=Proyek::where(function($query) use ($dataSearch) {
        $query->where('nama_paket_pekerjaan', 'LIKE', '%' . $dataSearch . '%')
        ->orWhere('lokasi_proyek', 'LIKE', '%' . $dataSearch . '%');
      })
      ->where("is_deleted",0)
      ->orderBy("waktu_pelaksanaan_mulai", "asc")->get();
    }
    return DataTables()->of($list)->make(true);
  }

  public function toLogin(){
    return view('auth.login');
  }

  public function toRegister(){
    return view('auth.login');
  }

  public function downloadKontrak(Request $request){

    $pdf = new \Clegginabox\PDFMerger\PDFMerger;

    $user_id = Auth::id();
    $data_user = User::where('id',$user_id)->first();



  // return response()->json(public_path().'/file_kontrak/kontrak_'.$data_user->nama_lengkap.'.pdf');

    if (public_path() == '/var/www/vhosts/ciriajasaec.com/httpdocs/public/elibrary/public'){
      foreach ($request->ids as $key => $value ){
        $pdf->addPDF('/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/file_kontrak/'.$value.'.pdf', 'all');

      }
      $pdf->merge('file', '/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/file_kontrak/kontrak_'.$data_user->nama_lengkap.'.pdf', 'P');
      $file = '/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/file_kontrak/kontrak_'.$data_user->nama_lengkap.'.pdf';
    }
    else{ 
      foreach ($request->ids as $key => $value ){

        $pdf->addPDF(public_path('/file_kontrak/'.$value.'.pdf'), 'all');
      }
      $pdf->merge('file', public_path('/file_kontrak/kontrak_'.$data_user->nama_lengkap.'.pdf'), 'P');
      $file = public_path('/file_kontrak/kontrak_'.$data_user->nama_lengkap.'.pdf');
    }


    return response()->json($data_user->nama_lengkap);

  }

  public function createDownloadProyek(Request $request){

    $user_id = Auth::id();
    $data_user = User::where('id',$user_id)->first();
    $data_proyek = Proyek::whereIn('id',$request->ids)->where('is_deleted',0)->with('perusahaanChild')->with('detailChild')->OrderBy('waktu_pelaksanaan_mulai','asc')->get();
    $response['data_user']              = $data_user;
    $response['data_proyek']            = $data_proyek;
    return response()->json($response);

  }

  public function downloadProyek(Request $request){
    $user_id = Auth::id();
    $data_user = User::where('id',$user_id)->first();

    $head = '
    <!DOCTYPE html>
    <html lang="en">
    <head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ELibrary &mdash; Entliven</title>

    <!-- General CSS Files -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>E-Library - Home</title>

    <style>
    @media print {
      footer {
        position: fixed;
        bottom: 0;
      }
      html, body {
        width: 210mm;
        height: 297mm;
      }

      p{margin-top:2px !important;margin-bottom:2px !important;}
      span{margin-top:2px !important;margin-bottom:2px !important;}

      div.row{
        border-bottom: solid 1px ;
        padding: 5px;
        display: inline-block;
        width:695px;

      }

      div.col1{
        display: inline-block;
        grid-column: 1;
        width: 20px;
        vertical-align:top;
      }

      div.col2{
        display: inline-block;
        grid-column: 2;
        width: 200px;
        vertical-align:top;
      }

      div.col3{
        display: inline-block;
        grid-column: 3;
        width: 20px;
        vertical-align:top;
      }      
      
      div.col4{
        display: inline-block;
        grid-column: 4;
        width: 400px !important;
        background:yellow;
        vertical-align:top;
      }

      .ruanglingkup4{padding-left: 241px;border-bottom:none;margin-top:-18px;}
      div.row:nth-child(3){
        border-bottom:none !important;
      } 

     #garishead{
      position: fixed;
      top:98px;
      left:-1px;
      width:100%;
    }

    .table-print{

      width: 100%;
    }

    .table-print>tbody>tr>td{
      padding:2px;
      padding-left:5px;
      vertical-align: top;
    }

    .table-print>tbody>tr{
      border:1px;
    }

    .table-print>tbody>tr>td:nth-child(1){
      width: 15px;
    }

       .rl{
           position:relative;
      left:250px;
      top:-18px;
      display:block !important;

    }

  }

   .rl{
      position:relative;
      left:250px;
      top:-18px;
      display:block !important;
    }

  div.row{
    border-bottom: solid 1px ;
    padding: 5px;
    display: inline-block;
    width:695px;
  }

  div.col1{
    display: inline-block;
    grid-column: 1;
    width: 20px;
    padding-left:8px;
    vertical-align:top;
  }

  div.col2{
    display: inline-block;
    grid-column: 2;
    width: 200px;
    vertical-align:top;
  }

  div.col3{
    display: inline-block;
    grid-column: 3;
    width: 20px;
    vertical-align:top;
  }      

  div.col4{
    display: inline-block;
    grid-column: 4;
    width: 400px;
    vertical-align:top;

  }

  .ruanglingkup4{padding-left: 241px;border-bottom:none;margin-top:-18px;}
  // div.row:nth-child(3){
  //   border-bottom:none !important;
  // }


  .kotak{
      // border-left: solid 1px ;
      // border-top: solid 1px ;
      // border-right: solid 1px ;
    margin:-1px;
  }

  p{margin-top:2px !important;margin-bottom:2px !important;}
  span{margin-top:2px !important;margin-bottom:2px !important;}

  body{
    padding-top: 106px;
    font-family: "Arial Narrow" !important;
    font-size:11pt !important;
    background-color: transparent !important;
    z-index:999;
  }

  tr:not(:nth-child(1)){
    page-break-before: avoid;
    page-break-after: avoid;
    page-break-inside: avoid;

  }

  tr-lingkup{
    page-break-before: always;
  }

  table { 
    border-collapse: collapse; 
  }



  .headprint{
    position: fixed;
    top:0px;
    width: 100%;
    padding-bottom: 10px;
    background: #BFBFBF;
    font-weight:bold;
    padding-top:5px;
  }

  #garishead{
  position: fixed;
  top:98px;
  left:-1px;
  width:100%;
}

  #garisfoot{
position: fixed;
bottom:-7px;
left:-1px;
width:100%;
}

.footerprint{
 position: fixed;
 font-size:12px;
 bottom: 0px;
 right: 10px;
 background: white;
}

td{
  padding-left:-10px !important;
}

.headprint1{
  text-align: right;
}

.headprint2{
  text-align: center;
}

.table-print{
  width: 100%;
}

.tr9>td{
  border-top: solid 0.5px;
}

.table-print>tbody>tr>td{
  padding:2px;
  padding-left:5px;
  vertical-align: top;
  width:100%;

}

.table-print>tbody>tr{
  border:1px;
}

.table-print>tbody>tr>td:nth-child(1){
  width: 15px;
}

.table-print>tbody>tr>td:nth-child(2),
.table-print>tbody>tr>td:nth-child(2),
.table-print>tbody>tr>td:nth-child(2),
.table-print>tbody>tr>td:nth-child(2){
  width: 180px !important;

}

.table-print>tbody>tr>td:nth-child(3){
  width:10px;
}
.table-print>tbody>tr>td:nth-child(4){
}


.footers{
  position: fixed;
  bottom: -20;
  right: 0;
  font-size:12px;
  font-style: italic;
}

tr:nth-child(4)>td{
  border-bottom: solid 0.5px;
}

tr:nth-child(5)>td{
  border-top: solid 0.5px;
}

.tr10>td,
.tr11>td{
  border-top: solid 0.5px;
  width:auto;
}

</style>  

</head>

<body >
'
;

$foot='
<script>

// console.log(document.getElementById("ruanglingkup").offsetHeight);

// ruanglingkupheight = document.getElementById("ruanglingkup").offsetHeight;

// const note = document.querySelector(".ruanglingkup");
// const note2 = document.querySelector(".ruanglingkup2");
// const note3 = document.querySelector(".ruanglingkup3");

// note.style.backgroundColor = "yellow";
// note2.style.backgroundColor = "yellow";
// note3.style.backgroundColor = "yellow";

// note.style.height = ruanglingkupheight+"px";
// note2.style.height = ruanglingkupheight+"px";
// note3.style.height = ruanglingkupheight+"px";

</script>
</body>
</html>';

$pdfOutput = new \Clegginabox\PDFMerger\PDFMerger;


if (public_path() == '/var/www/vhosts/ciriajasaec.com/httpdocs/public/elibrary/public'){
  foreach($request->html as $key => $value){
    $requestPath = '/var/www/vhosts/ciriajasaec.com/httpdocs/public/elibrary/add/Views/pdf/'.$user_id.'_'. $key .'.blade.php';

    // $requestPath = public_path('/../add/Views/pdf/'.$user_id.'_'. $key .'.blade.php');

    $requestFull = $head . "\r\n" .$value . $foot;
    File::put($requestPath, $requestFull);

    $pdf = pdf::loadView('/pdf.'.$user_id.'_'. $key)->setPaper('a4', 'potrait');

    $output = $pdf->output();

    if (file_exists('projectionsheet/'.$user_id.'.pdf')) {
      unlink('projectionsheet/'.$user_id.'.pdf');
    }

    file_put_contents('projectionsheet/'.$user_id.'_'. $key.'.pdf', $output);

    $pdfOutput->addPDF('/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/projectionsheet/'.$user_id.'_'. $key.'.pdf', 'all');
    // $pdfOutput->addPDF(public_path('/projectionsheet/'.$user_id.'_'. $key.'.pdf'), 'all');

  }
  $pdfOutput->merge('file', '/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/projectionsheet/'.$data_user->nama_lengkap.'_'.$user_id.'.pdf', 'P');
}
else{
  foreach($request->html as $key => $value){

    $requestPath = public_path('/../add/Views/pdf/'.$user_id.'_'. $key .'.blade.php');

    $requestFull = $head . "\r\n" .$value . $foot;
    File::put($requestPath, $requestFull);

    $pdf = pdf::loadView('/pdf.'.$user_id.'_'. $key)->setPaper('a4', 'potrait');

    $output = $pdf->output();

    if (file_exists('projectionsheet/'.$user_id.'.pdf')) {
      unlink('projectionsheet/'.$user_id.'.pdf');
    }

    file_put_contents('projectionsheet/'.$user_id.'_'. $key.'.pdf', $output);

    ;
    $pdfOutput->addPDF(public_path('/projectionsheet/'.$user_id.'_'. $key.'.pdf'), 'all');

  }
  $pdfOutput->merge('file', public_path().'/projectionsheet/'.$data_user->nama_lengkap.'_'.$user_id.'.pdf', 'P');
}


return response()->json('success');

}

public function downloadProyekBeneran(Request $request){
  $user_id = Auth::id();
  $data_user = User::where('id',$user_id)->first();

  if (public_path() == '/var/www/vhosts/ciriajasaec.com/httpdocs/public/elibrary/public'){
    $file = '/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/projectionsheet/'.$data_user->nama_lengkap.'_'.$user_id.'.pdf';
  }
  else{
    $file = public_path().'/projectionsheet/'.$data_user->nama_lengkap.'_'.$user_id.'.pdf';
  }

  $headers = array(
    'Content-Type: application/pdf',
  );
  return response()->download($file, 'project sheet.pdf', $headers);

  // return response()->file('projectionsheet/'.$data_user->nama_lengkap.'_'.$user_id.'.pdf');

  // return view('pdf.'.$user_id.'_0');

}

public function downloadProyekBeneran2(){
  $user_id = Auth::id();
  $data_user = User::where('id',$user_id)->first();
  if (public_path() == '/var/www/vhosts/ciriajasaec.com/httpdocs/public/elibrary/public'){
    $file = '/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/projectionsheet/'.$data_user->nama_lengkap.'_'.$user_id.'.pdf';
  }
  else{
    $file = public_path().'/projectsheet/'.$data_user->nama_lengkap.'_'.$user_id.'.pdf';
  }

  $headers = array(
    'Content-Type: application/pdf',
  );
  // return response()->download($file, 'projectionsheet.pdf', $headers);

  // return response()->file('projectionsheet/'.$data_user->nama_lengkap.'_'.$user_id.'.pdf');

  return view('pdf.'.$user_id.'_0');

}

}
