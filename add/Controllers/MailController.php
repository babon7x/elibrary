<?php

namespace Add\Controllers;

use Illuminate\Http\Request;
use Add\Models\MyTestMail;
use Illuminate\Support\Facades\Mail;
use App\Models\User;

class MailController extends Controller
{
    
    public function kirimEmail(Request $requset){

    $email = $requset->email;

	$details = [
    'title' => 'Selamat akun anda sudah di aktifkan',
    'body' => 'silahkan login ke elibrary.ciriajasa untuk mengakses '
    ];
   
    \Mail::to($email)->send(new \Add\Models\MyTestMail($details));
   
    return redirect()->route('login');

	}
}