<?php

namespace Add\Controllers\Auth;

use Auth;
use Add\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    protected $redirectTo = RouteServiceProvider::HOME;

    // public function username()
    // {
    //     return 'username';
    // }
    protected function credentials(Request $request)
    {        

        return ['email' => $request->{$this->username()}, 'password' => $request->password, 'aktif' => 1];
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request) {
      Auth::logout();
      return redirect('/login');
  }

  
}
