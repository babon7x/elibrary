<?php

namespace Add\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Filesystem\Filesystem;
use File;
use Response;
use PDF;
use Dompdf\Options;

use Add\Requests\ProyekRequest;
use Add\Requests\ProyekDetailRequest;
use Add\Requests\ProyekPerusahaanRequest;

use Add\Models\Proyek;
use Add\Models\ProyekDetail;
use Add\Models\ProyekPerusahaan;
use App\Models\User;
use Add\Models\Chart;

class ProyekController extends Controller
{


	public function index()
	{
		$page = 'list';
		$user_id = Auth::id();   
		$chart_count = 0;
		$top_proyek_count = 0;
		$new_user = 0;
		if ($user_id == ''){
			return redirect()->route('login');
		}
		else{
			$data_user = '';
			$data_user = User::where('id',$user_id)->first();
			$chart_list = Chart::whereIn('user_id',[$user_id])->get();
			$chart_count = $chart_list->count();
			$top_proyek_list = Proyek::where('top_proyek',1)->where('is_deleted',0)->get();
			$top_proyek_count = $top_proyek_list->count();
			$new_user_list = User::where('aktif',0)->where('is_deleted',0)->get();
			$new_user = $new_user_list->count();
			$topproyek=Proyek::where("is_deleted",0)->where('top_proyek',1)->orderBy("created_at", "desc")->get();
			return view('proyek.index',compact('user_id','topproyek','page','data_user','chart_count','new_user','top_proyek_count'));
		}
	}

	public function list(Request $request)
	{
		$yearStart = $request->selectYearStart.'-01-01';
		$year = $request->selectYear.'-12-31';
		$dataSearch = $request->dataSearch;
		if ($dataSearch == ''){
			$list=Proyek::where("is_deleted",0)->whereBetween('waktu_pelaksanaan_mulai',[$yearStart,$year])->orderBy("waktu_pelaksanaan_mulai", "asc")->get();
		}
		else{
			$list=Proyek::where(function($query) use ($dataSearch) {
				$query->where('nama_paket_pekerjaan', 'LIKE', '%' . $dataSearch . '%')
				->orWhere('lokasi_proyek', 'LIKE', '%' . $dataSearch . '%');
			})
			->where("is_deleted",0)
			->orderBy("waktu_pelaksanaan_mulai", "asc")->get();
		}
		return DataTables()->of($list)->make(true);
	}

	public function listTopProyek(Request $request)
	{
		$list=Proyek::where("is_deleted",0)->where('top_proyek',1)->orderBy("created_at", "desc")->get();
		return DataTables()->of($list)->make(true);
	}

	public function create(){
		$page = 'create';
		$user_id = Auth::id();   
		$chart_count = 0;
		$new_user=0;
		if ($user_id == ''){
			return redirect()->route('login');
		}
		else{

			$data_user = '';
			$data_user = User::where('id',$user_id)->first();
			$chart_list = Chart::whereIn('user_id',[$user_id])->get();
			$chart_count = $chart_list->count();
			$new_user_list = User::where('aktif',0)->where('is_deleted',0)->get();
			$new_user = $new_user_list->count();
			return view('proyek.form',compact('user_id','data_user','page','chart_count','new_user'));
		}
	}

	public function show($id){
		$page = 'edit';
		$user_id = Auth::id();   
		$chart_count = 0;
		$new_user=0;
		if ($user_id == ''){
			return redirect()->route('login');
		}
		else{

			$data_user = '';
			$data_user = User::where('id',$user_id)->first();
			$chart_list = Chart::whereIn('user_id',[$user_id])->get();
			$chart_count = $chart_list->count();
			$new_user_list = User::where('aktif',0)->where('is_deleted',0)->get();
			$new_user = $new_user_list->count();
			$datas = Proyek::where("id",$id)->where("is_deleted",0)->with('perusahaanChild')->with('detailChild')->get();

			$totalJumlahOrang = ProyekDetail::Where("proyek_id",$id)->sum('jumlah_orang');
			$totalTenagaAsing = ProyekPerusahaan::Where("proyek_id",$id)->sum('asing');
			$totalTenagaIndonesia = ProyekPerusahaan::Where("proyek_id",$id)->sum('indonesia');
			
			$test = $datas[0]->perusahaan_child;

			$datas[0]['totalJumlahOrang'] =$totalJumlahOrang;
			$datas[0]['totalTenagaAsing'] =$totalTenagaAsing;
			$datas[0]['totalTenagaIndonesia'] =$totalTenagaIndonesia;
			// $datas[0]['file_kontrak'] = public_path('/file_kontrak/'.$id.'.pdf');
			// $datas[0]['cover_proyek'] = public_path('/cover_proyek/'.$id.'.jpg');
			$datas[0]['upload_cover_proyek'] = '';
			$datas[0]['upload_file_kontrak'] = '';
			return view('proyek.edit',compact('user_id','data_user','page','chart_count','datas','test','new_user'));
		}
	}

	public function store(Request $request){

		$pdf = new \Clegginabox\PDFMerger\PDFMerger;
		$upload = $request->all();

		$user_id = Auth::id();
		$data_user = User::where('id',$user_id)->first();

		

		if (public_path() == '/var/www/vhosts/ciriajasaec.com/httpdocs/public/elibrary/public'){
			$request->upload_file_kontrak->move('/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/file_kontrak2', $user_id.'.pdf');
			$pdf->addPDF('/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/file_kontrak2/'.$user_id.'.pdf', 'all');
			$pdf->merge('file', '/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/file_kontrak2/kontrak_'.$user_id.'_cek.pdf', 'P');
		}
		else{					
			$request->upload_file_kontrak->move(public_path('file_kontrak2'),  $user_id.'.pdf');
			$pdf->addPDF(public_path('/file_kontrak2/'.$user_id.'.pdf'), 'all');
			$pdf->merge('file', public_path('/file_kontrak2/kontrak_'.$user_id.'_cek.pdf'), 'P');
		}




		$upload2 = $upload['lingkup_produk_utama'];
		if  (substr($upload2,0,2) == '<p'){
			$position = strpos($upload2, '>');
			$find = substr($upload2,0,$position+1);
			$upload2 = str_replace($find,'', $upload2);
			$upload2 = substr($upload2,0,-4);
		}
		$upload['lingkup_produk_utama'] = $upload2;

		$upload['waktu_pelaksanaan_mulai'] = date("Y-m-d", strtotime($upload['waktu_pelaksanaan_mulai']));
		$upload['waktu_pelaksanaan_selesai'] = date("Y-m-d", strtotime($upload['waktu_pelaksanaan_selesai']));

		$post_detail = [];
		$post_perushaan = [];
		$ProyekRequest = ProyekRequest::class;
		$ProyekRules = (new $ProyekRequest)->rules();
		$ProyekError = (new $ProyekRequest)->messages();

		Validator::make($upload,$ProyekRules,$ProyekError)->validate();



		if ($request['nama'] != null){	

			$ProyekPerusahaanRequest = ProyekPerusahaanRequest::class;
			$ProyekPerusahaanRules = (new $ProyekPerusahaanRequest)->rules();
			$ProyekPerusahaanError = (new $ProyekPerusahaanRequest)->messages();

			foreach ($upload['nama'] as $key => $value){

				$post_perushaan = array(
					'proyek_id' => 'cek',
					'nama' => $upload['nama'][$key],
					'asing' => $upload['asing'][$key],
					'indonesia' => $upload['indonesia'][$key],
				);

				Validator::make($post_perushaan,$ProyekPerusahaanRules,$ProyekPerusahaanError)->validate();
				$post_perushaan = [];
			}
		}

		if ($request['posisi'] != null){	

			$ProyekDetailRequest = ProyekDetailRequest::class;
			$ProyekDetailRules = (new $ProyekDetailRequest)->rules();
			$ProyekDetailError = (new $ProyekDetailRequest)->messages();

			foreach ($upload['posisi'] as $key => $value){

				$post_detail = array(
					'proyek_id' => 'cek',
					'posisi' => $upload['posisi'][$key],
					'keahlian' => $upload['keahlian'][$key],
					'jumlah_orang' => $upload['jumlah_orang'][$key],
				);

				Validator::make($post_detail,$ProyekDetailRules,$ProyekDetailError)->validate();
				$post_detail = [];
			}
		}

		DB::beginTransaction();
		try {
			$proyek = $upload;
			$user_id = Auth::id();
			$proyek["created_by"] = $user_id;
			$ProyekStore = Proyek::create($proyek);

			if ($ProyekStore){

				

				if ($request['nama'] != null){	

					$post_perushaan = [];
					foreach ($request['nama'] as $key => $value){

						$post_perushaan = array(
							'proyek_id' => $ProyekStore["id"],
							'nama' => $upload['nama'][$key],
							'asing' => $upload['asing'][$key],
							'indonesia' => $upload['indonesia'][$key],
						);

						$ProyekPerusahaanStore = ProyekPerusahaan::create($post_perushaan);
						$post_perushaan = [];

					}
				}

				if ($request['posisi'] != null){	


					$post_detail = [];
					foreach ($request['posisi'] as $key => $value){

						$post_detail = array(
							'proyek_id' => $ProyekStore["id"],
							'posisi' => $upload['posisi'][$key],
							'keahlian' => $upload['keahlian'][$key],
							'jumlah_orang' => $upload['jumlah_orang'][$key],
						);

						$ProyekDetailStore = ProyekDetail::create($post_detail);
						$post_detail = [];

					}
				}

			}

			DB::commit();
		} catch (\Exception $ex) {
			DB::rollback();
			return response()->json(["errors" => $ex], 500);
		}

		if (public_path() == '/var/www/vhosts/ciriajasaec.com/httpdocs/public/elibrary/public'){
			$request->upload_cover_proyek->move('/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/cover_proyek', $ProyekStore["id"].".jpg");
			File::move('/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/file_kontrak2/'.$user_id.'.pdf', '/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/file_kontrak/'. $ProyekStore["id"].'.pdf');
		}
		else{					
			$request->upload_cover_proyek->move(public_path('cover_proyek'), $ProyekStore["id"].".jpg");
			File::move(public_path('file_kontrak2/'.$user_id.'.pdf'), public_path('file_kontrak/'.$ProyekStore["id"].'.pdf'));
		}

		return response()->json($request);
	}

	public function updates(Request $request){

		$upload = $request->all();

		$upload2 = $upload['lingkup_produk_utama'];
		if  (substr($upload2,0,2) == '<p'){
			$position = strpos($upload2, '>');
			$find = substr($upload2,0,$position+1);
			$upload2 = str_replace($find,'', $upload2);
			$upload2 = substr($upload2,0,-4);
		}
		$upload['lingkup_produk_utama'] = $upload2;

		if ($request->upload_cover_proyek == null){
			$upload['upload_cover_proyek'] = 'skip';
		}
		if ($request->upload_file_kontrak==null){
			$upload['upload_file_kontrak'] = 'skip';
		}

		$upload['waktu_pelaksanaan_mulai'] = date("Y-m-d", strtotime($request->waktu_pelaksanaan_mulai));
		$upload['waktu_pelaksanaan_selesai'] = date("Y-m-d", strtotime($request->waktu_pelaksanaan_selesai));

		$post_detail = [];
		$post_perushaan = [];
		$ProyekRequest = ProyekRequest::class;
		$ProyekRules = (new $ProyekRequest)->rules();
		$ProyekError = (new $ProyekRequest)->messages();

		Validator::make($upload,$ProyekRules,$ProyekError)->validate();

		if ($request['nama'] != null){	

			$ProyekPerusahaanRequest = ProyekPerusahaanRequest::class;
			$ProyekPerusahaanRules = (new $ProyekPerusahaanRequest)->rules();
			$ProyekPerusahaanError = (new $ProyekPerusahaanRequest)->messages();

			foreach ($upload['nama'] as $key => $value){
				$post_perushaan = array(
					'proyek_id' => 'cek',
					'nama' => $upload['nama'][$key],
					'asing' => $upload['asing'][$key],
					'indonesia' => $upload['indonesia'][$key],
				);

				Validator::make($post_perushaan,$ProyekPerusahaanRules,$ProyekPerusahaanError)->validate();
				$post_perushaan = [];
			}
		}

		if ($request['posisi'] != null){	


			$ProyekDetailRequest = ProyekDetailRequest::class;
			$ProyekDetailRules = (new $ProyekDetailRequest)->rules();
			$ProyekDetailError = (new $ProyekDetailRequest)->messages();

			foreach ($upload['posisi'] as $key => $value){

				$post_detail = array(
					'proyek_id' => 'cek',
					'posisi' => $upload['posisi'][$key],
					'keahlian' => $upload['keahlian'][$key],
					'jumlah_orang' => $upload['jumlah_orang'][$key],
				);

				Validator::make($post_detail,$ProyekDetailRules,$ProyekDetailError)->validate();
				$post_detail = [];
			}

		}

		$proyek = $upload;
		unset($proyek['_token']);
		unset($proyek['nama']);
		unset($proyek['asing']);
		unset($proyek['indonesia']);
		unset($proyek['jumlah_orang']);
		unset($proyek['keahlian']);
		unset($proyek['posisi']);

		DB::beginTransaction();
		try {
			$user_id = Auth::id();
			$proyek["updated_by"] = $user_id;
			$ProyekStore = Proyek::where("id", $request->id)->update($proyek);

			if ($ProyekStore){

				

				ProyekPerusahaan::where('proyek_id',$request->id)->delete();

				if ($request['nama'] != null){	

					$post_perushaan = [];
					foreach ($request['nama'] as $key => $value){

						$post_perushaan = array(
							'proyek_id' => $request->id,
							'nama' => $upload['nama'][$key],
							'asing' => $upload['asing'][$key],
							'indonesia' => $upload['indonesia'][$key],
						);

						$ProyekPerusahaanStore = ProyekPerusahaan::create($post_perushaan);
						$post_perushaan = [];

					}

				}

				ProyekDetail::where('proyek_id',$request->id)->delete();

				if ($request['posisi'] != null){	

					$post_detail = [];
					foreach ($request['posisi'] as $key => $value){

						$post_detail = array(
							'proyek_id' => $request->id,
							'posisi' => $upload['posisi'][$key],
							'keahlian' => $upload['keahlian'][$key],
							'jumlah_orang' => $upload['jumlah_orang'][$key],
						);

						$ProyekDetailStore = ProyekDetail::create($post_detail);
						$post_detail = [];

					}
				}

			}

			DB::commit();
		} catch (\Exception $ex) {
			DB::rollback();
			return response()->json(["errors" => $ex], 500);
		}




		if ($upload['upload_cover_proyek']!='skip'){
			$upload['upload_cover_proyek'] = $request->id.'.'.$request->upload_cover_proyek->getClientOriginalExtension();
			if (public_path() == '/var/www/vhosts/ciriajasaec.com/httpdocs/public/elibrary/public'){
				$request->upload_cover_proyek->move('/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/cover_proyek', $ProyekStore["id"].".jpg");

			}
			else{		
				$request->upload_cover_proyek->move(public_path('cover_proyek'), $request->id.".jpg");
			}
		}


		if ($upload['upload_file_kontrak']!='skip'){

			$pdf = new \Clegginabox\PDFMerger\PDFMerger;
			$user_id = Auth::id();
			$data_user = User::where('id',$user_id)->first();

			if (public_path() == '/var/www/vhosts/ciriajasaec.com/httpdocs/public/elibrary/public'){
				$request->upload_file_kontrak->move('/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/file_kontrak2', $user_id.'.pdf');
				$pdf->addPDF('/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/file_kontrak2/'.$user_id.'.pdf', 'all');
				$pdf->merge('file', '/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/file_kontrak2/kontrak_'.$user_id.'_cek.pdf', 'P');
			}
			else{					
				$request->upload_file_kontrak->move(public_path('file_kontrak2'),  $user_id.'.pdf');
				$pdf->addPDF(public_path('/file_kontrak2/'.$user_id.'.pdf'), 'all');
				$pdf->merge('file', public_path('/file_kontrak2/kontrak_'.$user_id.'_cek.pdf'), 'P');
			}


			$upload['upload_file_kontrak'] = $request->id.'.'.$request->upload_file_kontrak->getClientOriginalExtension();
			// if (public_path() == '/var/www/vhosts/ciriajasaec.com/httpdocs/public/elibrary/public'){
			// 	$request->upload_file_kontrak->move('/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/file_kontrak', $upload['upload_file_kontrak']."pdf");
			// }
			// else{		
			// 	$request->upload_file_kontrak->move(public_path('file_kontrak'), $request->id.".pdf");
			// }

			if (public_path() == '/var/www/vhosts/ciriajasaec.com/httpdocs/public/elibrary/public'){
				File::move('/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/file_kontrak2/'.$user_id.'.pdf', '/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/file_kontrak/'. $request->id.'.pdf');
			}
			else{					
				File::move(public_path('file_kontrak2/'.$user_id.'.pdf'), public_path('file_kontrak/'.$request->id.'.pdf'));
			}


		}
		return response()->json($proyek);
	}
	public function destroy(Request $request)
	{
		$id= $request->ids[0];
		$deleted_by = Auth::id();
		$delete = Chart::WhereIn("proyek_id",request("ids"))->delete();
		$delete = Proyek::whereIn("id", request("ids"))->update(["is_deleted"=>1,"updated_by"=>$deleted_by]);
		$delete = ProyekDetail::whereIn("proyek_id", request("ids"))->update(["is_deleted"=>1,"updated_by"=>$deleted_by]);
		$delete = ProyekPerusahaan::whereIn("proyek_id", request("ids"))->update(["is_deleted"=>1,"updated_by"=>$deleted_by]);

		return response()->json('success');
	}

	public function getData(Request $request)
	{
		$datas = Proyek::where("id",$request->ids)->where("is_deleted",0)->with('perusahaanChild')->with('detailChild')->get();
		$totalJumlahOrang = ProyekDetail::Where("proyek_id",$request->ids)->sum('jumlah_orang');
		$totalTenagaAsing = ProyekPerusahaan::Where("proyek_id",$request->ids)->sum('asing');
		$totalTenagaIndonesia = ProyekPerusahaan::Where("proyek_id",$request->ids)->sum('indonesia');


		$datas[0]['totalJumlahOrang'] =$totalJumlahOrang;
		$datas[0]['totalTenagaAsing'] =$totalTenagaAsing;
		$datas[0]['totalTenagaIndonesia'] =$totalTenagaIndonesia;
		if (public_path() == '/var/www/vhosts/ciriajasaec.com/httpdocs/public/elibrary/public'){
			$datas[0]['file_kontrak'] = '/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/file_kontrak/'.$request->ids.'.pdf';
			$datas[0]['cover_proyek'] = '/var/www/vhosts/ciriajasaec.com/httpdocs/public/E-Library/cover_proyek/'.$request->ids.'.jpg';
		}
		else{		
			$datas[0]['file_kontrak'] = public_path('/file_kontrak/'.$request->ids.'.pdf');
			$datas[0]['cover_proyek'] = public_path('/cover_proyek/'.$request->ids.'.jpg');
		}
		// $datas['cover_proyek_file'] = File::get(public_path('cover_proyek/'.$request->ids.'.jpg'));
		// $datas['kontrak_file'] = File::get(public_path('/file_kontrak/'.$request->ids.'.pdf'));

		return response()->json($datas);
	}

	public function DetailProyek($id)
	{

		$page = 'list';
		$user_id = Auth::id();   
		$chart_count = 0;
		$new_user = 0;
		$data_user = '';
		$data_user = User::where('id',$user_id)->first();
		$chart_list = Chart::whereIn('user_id',[$user_id])->get();
		$chart_count = $chart_list->count();
		$new_user_list = User::where('aktif',0)->where('is_deleted',0)->get();
		$new_user = $new_user_list->count();
		$topproyek=Proyek::where("is_deleted",0)->where('top_proyek',1)->orderBy("created_at", "desc")->get();

		$datas = Proyek::where("id",$id)->where("is_deleted",0)->with('perusahaanChild')->with('detailChild')->get();

		$totalJumlahOrang = ProyekDetail::Where("proyek_id",$id)->sum('jumlah_orang');
		$totalTenagaAsing = ProyekPerusahaan::Where("proyek_id",$id)->sum('asing');
		$totalTenagaIndonesia = ProyekPerusahaan::Where("proyek_id",$id)->sum('indonesia');

		unset($datas[0]['upload_cover_proyek']);
		unset($datas[0]['upload_file_kontrak']);

		$datas[0]['totalJumlahOrang'] =$totalJumlahOrang;
		$datas[0]['totalTenagaAsing'] =$totalTenagaAsing;
		$datas[0]['totalTenagaIndonesia'] =$totalTenagaIndonesia;
		// $datas[0]['file_kontrak'] = public_path('/file_kontrak/'.$id.'.pdf');
		// $datas[0]['cover_proyek'] = public_path('/cover_proyek/'.$id.'.jpg');

		return view('proyek.detail',compact('datas','user_id','topproyek','page','data_user','chart_count','new_user'));

	}

	public function setTopProyek(Request $request)
	{
		$updated_by = Auth::id();
		// $removeTopProyek = Proyek::where('is_deleted',0)->update(["top_proyek"=>0]);

		// $file = new Filesystem;
		// $file->cleanDirectory('public/top_proyek');

		$setTopProyek = Proyek::whereIn("id",request("ids"))->update(["updated_by"=>$updated_by,"top_proyek"=>1]);

		// $from_path = 
		// $to_path = public_path();

		// foreach ($request->ids as $value){
		// 	File::copy(public_path('cover_proyek/'.$value.'.jpg'), public_path('top_proyek/'.$value.'.jpg'));
		// }

		return response()->json('success');
	}

	public function removeTopProyek(Request $request)
	{
		$updated_by = Auth::id();
		$setTopProyek = Proyek::whereIn("id",request("ids"))->update(["updated_by"=>$updated_by,"top_proyek"=>0]);

		return response()->json('success');
	}


}