<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>ELibrary &mdash; Entliven</title>
 

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('bootstrap/bootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('fontawesome/fontawesome.all.css') }}">

  <!-- Template CSS -->
{{--   <link rel="stylesheet" href="{{ asset('stisla/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('stisla/css/components.css') }}"> --}}
  <link rel="stylesheet" href="{{ asset('assets/mystyle.css') }}">
</head>

<body>
 
  <!-- General JS Scripts -->
  <script src="{{ asset('jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/popper.js') }}"></script>
  <script src="{{ asset('bootstrap/bootstrap.bundle.js') }}"></script>
  <script src="{{ asset('jquery/jquery-nicescroll.min.js') }}"></script>
  <script src="{{ asset('assets/moment.js') }}"></script>

  <!-- Template JS Scripts -->
  {{-- <script src="{{ asset('stisla/js/stisla.js') }}"></script> --}}
  <script src="{{ asset('stisla/js/scripts.js') }}"></script>
  <script src="{{ asset('stisla/js/custom.js') }}"></script>

</body>
</html>
