<!DOCTYPE html>
<html lang="id">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>E-Library PT.Ciriajasa E.C.</title>
  <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="{{ asset('ciriajasa-assets/img/favicons/apple-icon-180x180.png') }}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('ciriajasa-assets/img/favicons/favicon-16x16.png') }}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('ciriajasa-assets/img/favicons/favicon-32x32.png') }}">
  <link rel="icon" type="image/png" sizes="180x180" href="{{ asset('ciriajasa-assets/img/favicons/apple-icon-180x180.png') }}">
  <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('ciriajasa-assets/img/favicons/android-icon-192x192.png') }}">
  <link rel="stylesheet" href="{{ asset('ciriajasa-assets/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
  <link rel="stylesheet" href="{{ asset('ciriajasa-assets/fonts/fontawesome-all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('ciriajasa-assets/fonts/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('select2/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('bootstrap/bootstrap-datepicker.min.css') }}">
  {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/suneditor@latest/dist/css/suneditor.min.css"> --}}
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.0/dist/sweetalert2.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="{{ asset('ciriajasa-assets/css/Button-Icon-Round.css') }}">
  <link rel="stylesheet" href="{{ asset('ciriajasa-assets/css/Ludens-Client---Login-Dropdown.css') }}">
  <link rel="stylesheet" href="{{ asset('ciriajasa-assets/css/Footer-Dark.css') }}">
  <link rel="stylesheet" href="{{ asset('ciriajasa-assets/css/vanilla-zoom.min.css') }}">
  <link rel="stylesheet" href="{{ asset('ciriajasa-assets/css/scss/my-custom.compiled.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/mystyle.css') }}">
  @yield('customStyles')
</head>

<body >
  <div id="app">

    @include('template.header')
    @yield('content')

  </div>
 

  <script src="{{ asset('assets/moment.js') }}"></script>
  <script src="{{ asset('ciriajasa-assets/js/jquery.min.js?version=1.1') }}"></script>
  <script src="{{ asset('ciriajasa-assets/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('ciriajasa-assets/js/bs-init.js?version=1.1') }}"></script>
  <script src="{{ asset('bootstrap/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('bootstrap/bootstrap-notify.min.js') }}"></script>
  {{-- <script src="https://cdn.jsdelivr.net/npm/suneditor@latest/dist/suneditor.min.js"></script> --}}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.6.0/umd/popper.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/dataTables.bootstrap4.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.0/dist/sweetalert2.all.min.js"></script>
  <script src="{{ asset('jquery/jquery-nicescroll.min.js') }}"></script>
  <script src="{{ asset('jquery/jquery.toast.js') }}"></script>
  <script src="{{ asset('select2/select2.full.min.js') }}"></script>
  <script src="{{ asset('autonumeric/AutoNumeric.js') }}"></script>

  <!-- Custom Scripts -->
  <script src="{{ asset('ciriajasa-assets/js/vanilla-zoom.js') }}"></script>
  <script src="{{ asset('ciriajasa-assets/js/theme.js?version=1.1') }}"></script>
  <script src="{{ asset('ciriajasa-assets/js/custom-scripts.js?version=1.1') }}"></script>
  {{-- <script src="{{ asset('ciriajasa-assets/js/Suneditor-WYSIWYG.js') }}"></script> --}}

  <script src="{{ asset('assets/customnotif.js') }}"></script>
  <script src="{{ asset('assets/global.js') }}"></script>

  @yield('js')

</body>
</html>
