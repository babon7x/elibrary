<div class="main-sidebar" style="z-index: 9999;">

<aside id="sidebar-wrapper">

<div class="sidebar-brand">

<a href="index.html">SimpleDev</a>

</div>

<div class="sidebar-brand sidebar-brand-sm">

<a href="index.html">SD</a>

</div>

<ul class="sidebar-menu">

<li class="nav-item link" nama="Dashboard">
<a class="nav-link" href="/dashboard">
<i class="fa fa-circle text-primary"></i><span class="nav-link-text">Dashboard</span>
</a>
</li>

<li class="nav-item" nama="Akses">
<a href="#akses" data-toggle="collapse" aria-expanded="false" class="nav-link dropdown-toggle">
<i class="fa fa-circle text-primary"></i><span class="nav-link-text">Akses</span>
</a>
<ul class="navbar-nav collapse" id="akses">
<li class="nav-item link" nama="undefined">
<a class="nav-link" href="/role">
<i class="fa fa-circle text-primary"></i><span class="nav-link-text">Role</span>
</a>
</li>

<li class="nav-item link" nama="undefined">
<a class="nav-link" href="/user">
<i class="fa fa-circle text-primary"></i><span class="nav-link-text">User</span>
</a>
</li>

</ul>
</li>

<li class="nav-item link" nama="Test">
<a class="nav-link" href="/test">
<i class="fa fa-circle text-primary"></i><span class="nav-link-text">Test</span>
</a>
</li>

<li class="nav-item link" nama="Testing">
<a class="nav-link" href="/testing">
<i class="fa fa-circle text-primary"></i><span class="nav-link-text">Testing</span>
</a>
</li>
</ul>

<div class="mt-4 mb-4 p-3 hide-sidebar-mini">

<a href="#" class="btn btn-primary btn-lg btn-block btn-icon-split">

<i class="fas fa-phone"></i> Contact Us

</a>

</div>

</aside>

</div>