 <!-- Start: search -->
 <div class="block-heading search-header with-bg mobile__plr-15 mobile__mb-0"
 style="padding-right: 20%;padding-left: 20%;">
 <div class="d-flex flex-column flex-md-row">
    <div class="input-group input-search-container mobile__mb-10" style="flex: 5;">
        <input class="form-control form-control-lg" type="text" placeholder="Cari Proyek" id="cari">
        <span class="input-group-text">
            <a href="#" id="btnCari"><i class="icon ion-android-search"></i></a>
        </span>
    </div>
    <select class="form-select form-select-lg form-select select-search mobile__mlr-0" style="flex: 1;margin-left: 10px;" id="selectYearStart">
       @for($x = 2000; $x <= 2050; $x++)
       @if($x == date("Y")-7)
       <option value="{{ $x }}" selected>{{ $x }}</option>
       @else
       <option value="{{ $x }}">{{ $x }}</option>
       @endif
       @endfor 
   </select>
   <label class="mobile__mlr-0" style="flex: 0.1;margin-left: 10px;padding-top: 10px;">s/d</label>
   <select class="form-select form-select-lg form-select select-search mobile__mlr-0" style="flex: 1;margin-left: 10px;" id="selectYear">
       @for($x = 2000; $x <= 2050; $x++)
       @if($x == date("Y"))
       <option value="{{ $x }}" selected>{{ $x }}</option>
       @else
       <option value="{{ $x }}">{{ $x }}</option>
       @endif
       @endfor 
   </select>
</div>
</div><!-- End: search -->