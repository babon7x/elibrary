<nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
	<div class="container"><a class="navbar-brand logo" href="/dashboard"><img src="{{ asset('assets/FE/assets/img/ciriajasa-1.png') }}"></a><button data-bs-toggle="collapse" class="navbar-toggler" data-bs-target="#navcol-1"><span class="visually-hidden">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
		<div class="collapse navbar-collapse" id="navcol-1">
	
			<ul class="navbar-nav ms-auto">
				<li class="nav-item"><a class="nav-link btn my-button-outline-primary" href="/register" style="padding-left: 3rem;padding-right: 3rem;">Register</a>
				</li>
				<li class="nav-item"><a class="nav-link btn my-button-primary text-white" id="loginBtn" href="/login" style="padding-left: 2rem;padding-right: 2rem;">Login</a>
				</li>
				{{-- <a href="/lupasandi" class="btn btn-primary btn-round">Lupa Password</a> --}}

			</ul>
	


		</div>
	</div>
</nav>