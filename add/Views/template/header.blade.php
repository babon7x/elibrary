<nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
    <div class="container">
        {{-- <a class="navbar-brand logo" href="/dashboard"><img src="{{ asset('assets/FE/assets/img/ciriajasa-1.png') }}"></a><button
            data-bs-toggle="collapse" class="navbar-toggler" data-bs-target="#navcol-1"><span
                class="visually-hidden">Toggle navigation</span><span class="navbar-toggler-icon"></span></button> --}}
        <div class="d-flex d-sm-flex justify-content-between justify-content-sm-between" style="width: 100%;"><a
                class="navbar-brand logo" href="{{ route('dashboard') }}"><img class="img-fluid"
                    src="{{ asset('assets/FE/assets/img/ciriajasa-1.png') }}"></a><button data-bs-toggle="collapse"
                class="navbar-toggler" data-bs-target="#navcol-1" style="border-width: 0px;"><span
                    class="visually-hidden">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
        </div>
		@if($user_id == '')
       	 	<div class="collapse navbar-collapse navbar-collapse-menu-container no-login" id="navcol-1">
		@else
			<div class="collapse navbar-collapse navbar-collapse-menu-container logged-in" id="navcol-1">
		@endif
            @if($user_id == '')
            <ul class="navbar-nav ms-auto">
                <li class="nav-item"><a class="nav-link" href="{{ route('dashboard') }}">Home</a></li>
                <li class="nav-item"><a class="nav-link btn my-button-outline-primary" href="{{ route('register') }}"
                        style="padding-left: 3rem;padding-right: 3rem;">Register</a>
                </li>
                <li class="nav-item"><a class="nav-link btn my-button-primary text-white" id="loginBtn" href="{{ route('login') }}"
                        style="padding-left: 2rem;padding-right: 2rem;">Login</a>
                </li>
                {{-- <a href="/lupasandi" class="btn btn-primary btn-round">Lupa Password</a> --}}

            </ul>
            @else

            <ul class="navbar-nav d-lg-flex ms-auto align-items-lg-center">
                <li class="nav-item d-inline-block d-sm-inline-block d-md-none justify-content-center align-items-center"
                    style="padding-right: 13px;text-align: center;">
                    <!-- Start: Gravatar Image --><img class="rounded-circle"
                        src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200" width="92"
                        height="92"><!-- End: Gravatar Image -->
                </li>
                <li class="nav-item" style="padding-right: 13px;"><a class="nav-link" id="header_home" href="{{ route('dashboard') }}">Home</a>
                </li>
               
                <li class="nav-item" style="padding-right: 13px;"><a class="nav-link d-lg-flex align-items-lg-center"
                        href="{{ route('chart.index') }}" id="header_chart"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"
                            fill="currentColor" viewBox="0 0 16 16" class="bi bi-cart3"
                            style="width: 1.5em;height: 1.5em;">
                            <path
                                d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z">
                            </path>
                        </svg><span style="margin-right: 7px;margin-left: 7px;">CART</span><span
                            class="badge rounded-pill bg-danger" id="chart_count">{{ $chart_count }}</span></a></li>
                <li class="nav-item" style="padding-right: 13px;"><a class="nav-link" id="header_upload" href="{{ route('proyek.create') }}">Upload</a>
                </li>
                <li class="nav-item" style="padding-right: 13px;"><a class="nav-link" href="{{ route('proyek.index') }}" id="header_view">View</a></li>
                <li class="nav-item dropdown" style="padding-right: 13px;">
                    <a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><img
                            class="d-none d-md-inline-block rounded-circle"
                            src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200" width="28"
                            height="28">&nbsp;{{ $data_user->nama_lengkap }}
                        @if($data_user->role->id != 4)
                        <span class="badge rounded-pill bg-danger" id="new_user">{{ $new_user }}</span>
                        @endif
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('akun',$user_id) }}">Akun</a>
                        @if($data_user->role->id != 4)
                        <a class="dropdown-item" href="{{ route('user.index') }}">Pengguna</a>
                        @endif
                        <hr class="dropdown-divider">
                        <a class="dropdown-item link-danger" id="logout" href="{{ route('logout') }}"
                            onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>Keluar
                        </a>
                    </div>
                </li>
            </ul>
            @endif
        </div>
    </div>
</nav>