<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>E-Library PT.Ciriajasa E.C.</title>

  <!-- General CSS Files -->
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>E-Library - Home</title>
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/ionicons.min.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.0/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/suneditor@latest/dist/css/suneditor.min.css">
    <link rel="stylesheet" href="{{ asset('assets/mystyle.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/styles.min.css') }}">

    <link rel="stylesheet" href="{{ asset('select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap/bootstrap-datepicker.min.css') }}">

  </head>


</head>

<body >
  <div id="app">

    @include('template.header_login')
    {{-- @include('template.sidebar') --}}

  </div>
  <!-- General JS Scripts -->
  <script src="{{ asset('jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/popper.js') }}"></script>
  <script src="{{ asset('bootstrap/bootstrap.bundle.js') }}"></script>
  <script src="{{ asset('bootstrap/bootstrap-notify.min.js') }}"></script>
  <script src="{{ asset('bootstrap/bootstrap-datepicker.min.js') }}"></script>

  <script src="{{ asset('jquery/jquery-nicescroll.min.js') }}"></script>
  <script src="{{ asset('jquery/jquery.toast.js') }}"></script>
  <script src="{{ asset('assets/moment.js') }}"></script>
  <script src="{{ asset('swal2/sweetalert2.all.min.js') }}"></script>
  <script src="{{ asset('select2/select2.full.min.js') }}"></script>
  <script src="{{ asset('autonumeric/AutoNumeric.js') }}"></script>
  <script src="{{ asset('summernote/summernote.min.js') }}"></script>

  <!-- Template JS Scripts -->
  <script src="{{ asset('stisla/js/scripts.js') }}"></script>

  <script src="{{ asset('assets/customnotif.js') }}"></script>
  <script src="{{ asset('assets/global.js') }}"></script>

  {{-- FE --}}
  <script src="{{ asset('assets/js/jquery.min.js')}}"></script>
  <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>

  <script src="{{ asset('assets/moment.js') }}"></script>
  <script src="{{ asset('select2/select2.full.min.js') }}"></script>
  <script src="{{ asset('autonumeric/AutoNumeric.js') }}"></script>
  <script src="{{ asset('bootstrap/bootstrap-datepicker.min.js') }}"></script>

  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.6.0/umd/popper.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/dataTables.bootstrap4.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.0/dist/sweetalert2.all.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/suneditor@latest/dist/suneditor.min.js"></script>
  <script src="{{ asset('assets/js/script.min.js')}}"></script>
  <script src="{{ asset('assets/js/home-script.min.js')}}"></script>
</body>
</html>
