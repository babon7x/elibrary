<!DOCTYPE html>
<html lang="id">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>E-Library PT.Ciriajasa E.C.</title>
  <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="{{ asset('ciriajasa-assets/img/favicons/apple-icon-180x180.png') }}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('ciriajasa-assets/img/favicons/favicon-16x16.png') }}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('ciriajasa-assets/img/favicons/favicon-32x32.png') }}">
  <link rel="icon" type="image/png" sizes="180x180" href="{{ asset('ciriajasa-assets/img/favicons/apple-icon-180x180.png') }}">
  <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('ciriajasa-assets/img/favicons/android-icon-192x192.png') }}">
  <link rel="stylesheet" href="{{ asset('ciriajasa-assets/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
  <link rel="stylesheet" href="{{ asset('ciriajasa-assets/fonts/fontawesome-all.min.css') }}">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.0/dist/sweetalert2.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
  <link rel="stylesheet" href="{{ asset('ciriajasa-assets/css/scss/my-custom.compiled.css') }}">
</head>

<body style="background: rgb(255,255,255);">
  <!-- Start: Header Login -->
  <header>
    <!-- Start: 1 Row 1 Column -->
    <div class="container" style="padding-top: 20px;">
      <div class="row">
        <div class="col-md-12"><a href="{{url('')}}"><img class="img-fluid" src="{{ asset('ciriajasa-assets/img/ciriajasa-1.png') }}"></a></div>
      </div>
    </div><!-- End: 1 Row 1 Column -->
  </header><!-- End: Header Login -->
  <main class="page login-page main-content-page">
    <section class="clean-block clean-form dark" style="background: rgb(255,255,255);">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-sm-12 col-md-6 order-2 order-md-1">
            <div class="block-heading" style="margin-bottom: 0px;">
              <h2 class="text-start text-info my-primary-color" style="font-weight: bold;">E-Library Register</h2>
              <p class="text-start" style="margin: 0px;">Untuk akses lengkap E-Library Ciriajasa,&nbsp;<br>Silahkan daftar terlebih dahulu dengan melengkapi form berikut. Anda sudah memiliki akun?&nbsp;&nbsp;<a class="my-primary-color" href="{{ route('login') }}" style="font-weight: bold;">Login di sini.</a></p>
            </div>
            <form class="custom-style-form-login" style="margin: 0px;">
              <div class="mb-3"><label class="form-label" for="fullname" style="font-weight: bold;">Nama Lengkap</label><input class="form-control form-control-lg" type="text" id="fullname" placeholder="Nama Lengkap" store="nama_lengkap"></div>
              <div class="mb-3"><label class="form-label" for="companyName" style="font-weight: bold;">Nama Perusahaan</label><input class="form-control form-control-lg" type="text" id="companyName" placeholder="Nama Perusahaan" store="perusahaan"></div>
              <div class="mb-3"><label class="form-label" for="email" style="font-weight: bold;">Email</label><input class="form-control form-control-lg item" type="email" id="email" inputmode="email" autocomplete="off" placeholder="Email" store="email"></div><button class="btn btn-primary my-button-primary mt-4" type="button" onclick="register()" style="width: 100%;height: 3rem;">Register</button>
            </form>
          </div>
          <div class="col-sm-12 col-md-6 order-1 order-md-2"><img class="img-fluid" src="{{ asset('ciriajasa-assets/img/queue.gif') }}" alt="login-image"></div>
        </div>
      </div>
    </section>
  </main>
  @include('template.footer')
  <script src="{{ asset('ciriajasa-assets/js/jquery.min.js') }}"></script>
  <script src="{{ asset('ciriajasa-assets/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.6.0/umd/popper.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.0/dist/sweetalert2.all.min.js"></script>
  <script src="{{ asset('ciriajasa-assets/js/auth.js') }}"></script>
  <script>

    function register(){
      var nama_lengkap = $('[store="nama_lengkap"]').val()
      var perusahaan = $('[store="perusahaan"]').val()
      var email = $('[store="email"]').val()
      var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
      var datas = {nama_lengkap:nama_lengkap,perusahaan:perusahaan,email:email,role_id:'4'}
      var baseurl = "{{ url('') }}"


      $('[store="nama_lengkap"]').removeClass('is-invalid')
      $('[store="perusahaan"]').removeClass('is-invalid')
      $('[store="email"]').removeClass('is-invalid')


      if (nama_lengkap == ''){
        $('[store="nama_lengkap"]').addClass('is-invalid')
        Swal.fire({
          title: 'Validation',
          text: 'Nama Lengkap Tidak Boleh Dikosongkan !',
          icon: 'warning',
          showCancelButton: false,
          confirmButtonText: 'OK',
          cancelButtonText: 'Ya',
          customClass: {
            confirmButton: 'btn btn-primary mr-2',
            cancelButton: 'btn btn-danger'
          },
          buttonsStyling: false
        })

        return false;
      }

      if (perusahaan == ''){
        $('[store="perusahaan"]').addClass('is-invalid')
        Swal.fire({
          title: 'Validation',
          text: 'Perusahaan Tidak Boleh Dikosongkan !',
          icon: 'warning',
          showCancelButton: false,
          confirmButtonText: 'OK',
          cancelButtonText: 'Ya',
          customClass: {
            confirmButton: 'btn btn-primary mr-2',
            cancelButton: 'btn btn-danger'
          },
          buttonsStyling: false
        })
        return false;
      }

      if (testEmail.test(email)){
        console.log('valid')
      }
      else{
        $('[store="email"]').addClass('is-invalid')
        Swal.fire({
          title: 'Validation',
          text: 'Email anda tidak valid !',
          icon: 'warning',
          showCancelButton: false,
          confirmButtonText: 'OK',
          cancelButtonText: 'Ya',
          customClass: {
            confirmButton: 'btn btn-primary mr-2',
            cancelButton: 'btn btn-danger'
          },
          buttonsStyling: false
        })
        return false;
      }

      console.log(datas)

      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{ route('user.store') }}",
        method: 'post',
        data:datas,
        success: function(response){

         Swal.fire({
          title: 'Berhasil',
          text: 'Mohon tunggu beberapa saat untuk proses data oleh admin !',
          icon: 'warning',
          showCancelButton: false,
          confirmButtonText: 'OK',
          cancelButtonText: 'Ya',
          customClass: {
            confirmButton: 'btn btn-primary mr-2',
            cancelButton: 'btn btn-danger'
          },
          buttonsStyling: false
        }).then(function() {
          location.href = baseurl+'/';
        })
      },
      error: function(response){
        Swal.fire({
          title: 'Gagal',
          text: 'Email Sudah Terdaftar , silakan lakukan verifikasi !',
          icon: 'error',
          showCancelButton: false,
          confirmButtonText: 'OK',
          cancelButtonText: 'Ya',
          customClass: {
            confirmButton: 'btn btn-primary mr-2',
            cancelButton: 'btn btn-danger'
          },
          buttonsStyling: false
        })
      }

    })

    }
  </script>
</body>

</html>