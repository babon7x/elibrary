@include('template.app_fe_login')
<br>
<br>
<br>

<div class="container">
 @if (count($errors) > 0)
 <script>
   Swal.fire({
    title: 'Gagal !',
    text: 'Alamat Email Tidak Ditemukan !',
    icon: 'error',
    showCancelButton: false,
    confirmButtonText: 'OK',
    cancelButtonText: 'Ya',
    customClass: {
        confirmButton: 'btn btn-primary mr-2',
        cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
})
</script>
@endif
@if (session('status'))

<script>
   Swal.fire({
    title: 'Berhasil !',
    text: 'Link Reset Password telah dikirim ke email kamu !',
    icon: 'success',
    showCancelButton: false,
    confirmButtonText: 'OK',
    cancelButtonText: 'Ya',
    customClass: {
        confirmButton: 'btn btn-primary mr-2',
        cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
})
</script>
@endif

<div class="row justify-content-center">
    <div class="col-md-8">

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">

                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        KIRIM
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

