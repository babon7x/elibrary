<!DOCTYPE html>
<html lang="id">

<head>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
 <meta name="csrf-token" content="{{ csrf_token() }}">
 <title>E-Library PT.Ciriajasa E.C.</title>
 <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}">
 <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
 <link rel="stylesheet" href="{{ asset('assets/fonts/fontawesome-all.min.css') }} ">
 <link rel="stylesheet" href="{{ asset('assets/fonts/ionicons.min.css ') }}">
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css">
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css">
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.0/dist/sweetalert2.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
 <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/css/dataTables.bootstrap4.min.css">
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/suneditor@latest/dist/css/suneditor.min.css">
 <link rel="stylesheet" href="{{ asset('assets/css/styles.min.css') }} ">
 <link rel="stylesheet" href="{{ asset('swal2/sweetalert2.min.css') }}">
</head>

<body style="background: rgb(255,255,255);">
  <main class="page login-page">
    <section class="clean-block clean-form dark" style="background: rgb(255,255,255);">
      <!-- Start: header-logo -->
      <div class="container" style="padding-top: 20px;">
        <div class="row">
          <div class="col-md-12"><img src="{{ asset('assets/img/ciriajasa-1.png') }} "></div>
        </div>
      </div><!-- End: header-logo -->
      <div class="container">
        <div class="row align-items-center">
          <div class="col-sm-12 col-md-6">
            <div class="block-heading" style="margin-bottom: 0px;">
              <h2 class="text-start text-info my-primary-color">Buat Password Baru</h2>
              <p class="text-start" style="margin: 0px;">Silahkan masukkan password baru anda, untuk melakukan update perubahan password. <br>
               <a class="my-primary-color" href="{{ route('register') }}" style="font-weight: bold;">Register Here !</a><br>
               <a class="my-primary-color" href="{{ route('login') }}" style="font-weight: bold;">Login Here !</a>
             </p>
           </div>
           {{-- <form class="custom-style-form-login" style="margin: 0px;" method="POST" action="/ubahsandi"> --}}
            {{-- <input type="hidden" name="token" value="{{ $token }}"> --}}
            {{-- @csrf --}}
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" readonly style="display: none;">
            <div class="mb-3"><label class="form-label" for="newPassword" style="font-weight: bold;">Password Baru</label>
              <input class="form-control form-control-lg" type="password" id="password2" placeholder="Password Baru" name="password2" required>
            </div>
            <div class="mb-3"><label class="form-label" for="confirmNewPass" style="font-weight: bold;">Konfirmasi Password Baru</label>
              <input class="form-control form-control-lg" type="password" id="password3" placeholder="Konfirmasi Password Baru" name="password3" required>
            </div>
            <button class="btn btn-primary my-button-primary mt-4" type="button" style="width: 100%;height: 3rem;" onclick="resetPassword()">Kirim</button>
          {{-- </form> --}}
        </div>
        <div class="col-sm-12 col-md-6"><img class="img-fluid" src="{{ asset('assets/img/auth.gif') }} " alt="login-image"></div>
      </div>
    </div>
  </section>
</main><!-- Start: Footer Dark -->
@include('template.footer')
<script src="{{ asset('assets/js/jquery.min.js') }} "></script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }} "></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.6.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.0/dist/sweetalert2.all.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/suneditor@latest/dist/suneditor.min.js"></script>
<script src="{{ asset('assets/js/script.min.js') }} "></script>
<script src="{{ asset('assets/js/auth.min.js') }} "></script>
<script src="{{ asset('swal2/sweetalert2.all.min.js') }}"></script>

<script>
  function resetPassword(){
    let email = $('#email').val();
    let password2 = $('#password2').val();
    let password3 = $('#password3').val();
    var baseurl = "{{ url('') }}"

    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: "{{ route('reset_sandi') }}",
      method: 'post',
      data: {email:email,password2:password2,password3:password3},
      success: function(response){
       Swal.fire({
        title:'success !',
        text: 'Password Baru Berhasil Dibuat !',
        icon: 'success',
        showCancelButton: false,
        confirmButtonText: 'OK',
        cancelButtonText: 'Ya',
        customClass: {
          confirmButton: 'btn btn-primary mr-2',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      }).then((result) => {
        location.href = baseurl+'/login';
      })

    }
  })
  }
</script>

</body>

</html>