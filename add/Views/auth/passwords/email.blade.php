<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>E-Library PT.Ciriajasa E.C.</title>
    <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="{{ asset('ciriajasa-assets/img/favicons/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('ciriajasa-assets/img/favicons/favicon-16x16.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('ciriajasa-assets/img/favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="180x180" href="{{ asset('ciriajasa-assets/img/favicons/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('ciriajasa-assets/img/favicons/android-icon-192x192.png') }}">
    <link rel="stylesheet" href="{{ asset('ciriajasa-assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
    <link rel="stylesheet" href="{{ asset('ciriajasa-assets/fonts/fontawesome-all.min.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.0/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
    <link rel="stylesheet" href="{{ asset('ciriajasa-assets/css/scss/my-custom.compiled.css') }}">
</head>

<body style="background: rgb(255,255,255);">
    <!-- Start: Header Login -->
    <header>
        <!-- Start: 1 Row 1 Column -->
        <div class="container" style="padding-top: 20px;">





            <div class="row">
                <div class="col-md-12"><a href="{{url('')}}"><img class="img-fluid" src="{{ asset('ciriajasa-assets/img/ciriajasa-1.png') }}"></a></div>
            </div>
        </div><!-- End: 1 Row 1 Column -->
    </header><!-- End: Header Login -->
    <main class="page login-page main-content-page">
        <section class="clean-block clean-form dark" style="background: rgb(255,255,255);">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-sm-12 col-md-6 order-2 order-md-1">
                        <div class="block-heading" style="margin-bottom: 0px;">
                            <h2 class="text-start text-info my-primary-color" style="font-weight: bold;">Lupa Password</h2>
                            <p class="text-start" style="margin: 0px;">Silahkan masukkan alamat email anda, untuk&nbsp;<br>Mereset password anda.</p>
                        </div>
                        <form class="custom-style-form-login" style="margin: 0px;" method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <div class="mb-3"><label class="form-label" for="email" style="font-weight: bold;">Email</label><input class="form-control form-control-lg item" type="email" id="email" name='email' inputmode="email" autocomplete="off" placeholder="Email"></div><button class="btn btn-primary my-button-primary mt-4" type="submit" style="width: 100%;height: 3rem;">Kirim</button>
                        </form>
                    </div>
                    <div class="col-sm-12 col-md-6 order-1 order-md-2"><img class="img-fluid" src="{{ asset('ciriajasa-assets/img/auth.gif') }}" alt="login-image"></div>
                </div>
            </div>
        </section>
    </main>
    @include('template.footer')
    <script src="{{ asset('ciriajasa-assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('ciriajasa-assets/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.6.0/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.0/dist/sweetalert2.all.min.js"></script>
    <script src="{{ asset('ciriajasa-assets/js/auth.js') }}"></script>
    @if (count($errors) > 0)
    <script>
     Swal.fire({
        title: 'Gagal !',
        text: 'Alamat Email Tidak Ditemukan !',
        icon: 'error',
        showCancelButton: false,
        confirmButtonText: 'OK',
        cancelButtonText: 'Ya',
        customClass: {
            confirmButton: 'btn btn-primary mr-2',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    })
</script>
@endif
@if (session('status'))

<script>
 Swal.fire({
    title: 'Berhasil !',
    text: 'Link Reset Password telah dikirim ke email kamu !',
    icon: 'success',
    showCancelButton: false,
    confirmButtonText: 'OK',
    cancelButtonText: 'Ya',
    customClass: {
        confirmButton: 'btn btn-primary mr-2',
        cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
}).then(function() {
    location.href = '/';
})
</script>
@endif
</body>

</html>