@include('template.app-login')

<div class="container">

  <a href="/login" class="btn btn-primary btn-round">Login</a>
  <a href="/dashboard" class="btn btn-primary btn-round">Dashboard</a>
  <a href="#" class="btn btn-primary btn-round">Lupa Password</a>
  
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">Register</div>

        <div class="card-body">
         <div class="form-group">
           <label>Nama Lengkap</label>
           <input type="text" store="nama_lengkap" class="form-control" placeholder="nama lengkap">
         </div>
         <div class="form-group">
           <label>Perusahaan</label>
           <input type="text" store="perusahaan" class="form-control" placeholder="perusahaan">
         </div>
         <div class="form-group">
           <label>Email</label>
           <input type="email" store="email" class="form-control" placeholder="email">
         </div>
         <center>
           <button class="btn btn-primary" onclick="register()">register</button>
         </center>
       </div>
     </div>
   </div>
 </div>
</div>

<link rel="stylesheet" href="{{ asset('swal2/sweetalert2.min.css') }}">
<script src="{{ asset('swal2/sweetalert2.all.min.js') }}"></script>


<script>

  function register(){
    var nama_lengkap = $('[store="nama_lengkap"]').val()
    var perusahaan = $('[store="perusahaan"]').val()
    var email = $('[store="email"]').val()
    var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    var datas = {nama_lengkap:nama_lengkap,perusahaan:perusahaan,email:email,role_id:'4'}

    $('[store="nama_lengkap"]').removeClass('is-invalid')
    $('[store="perusahaan"]').removeClass('is-invalid')
    $('[store="email"]').removeClass('is-invalid')


    if (nama_lengkap == ''){
      $('[store="nama_lengkap"]').addClass('is-invalid')
      Swal.fire({
        title: 'Validation',
        text: 'Nama Lengkap Tidak Boleh Dikosongkan !',
        icon: 'warning',
        showCancelButton: false,
        confirmButtonText: 'OK',
        cancelButtonText: 'Ya',
        customClass: {
          confirmButton: 'btn btn-primary mr-2',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })

      return false;
    }

    if (perusahaan == ''){
      $('[store="perusahaan"]').addClass('is-invalid')
      Swal.fire({
        title: 'Validation',
        text: 'Perusahaan Tidak Boleh Dikosongkan !',
        icon: 'warning',
        showCancelButton: false,
        confirmButtonText: 'OK',
        cancelButtonText: 'Ya',
        customClass: {
          confirmButton: 'btn btn-primary mr-2',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      return false;
    }

    if (testEmail.test(email)){
      console.log('valid')
    }
    else{
      $('[store="email"]').addClass('is-invalid')
      Swal.fire({
        title: 'Validation',
        text: 'Email anda tidak valid !',
        icon: 'warning',
        showCancelButton: false,
        confirmButtonText: 'OK',
        cancelButtonText: 'Ya',
        customClass: {
          confirmButton: 'btn btn-primary mr-2',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      return false;
    }

    console.log(datas)

    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: 'user',
      method: 'post',
      data:datas,
      success: function(response){

       Swal.fire({
        title: 'Berhasil',
        text: 'Mohon tunggu beberapa saat untuk proses data oleh admin !',
        icon: 'warning',
        showCancelButton: false,
        confirmButtonText: 'OK',
        cancelButtonText: 'Ya',
        customClass: {
          confirmButton: 'btn btn-primary mr-2',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })

     }
   })

  }
</script>
