<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>E-Library PT.Ciriajasa E.C.</title>
    <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="{{ asset('ciriajasa-assets/img/favicons/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('ciriajasa-assets/img/favicons/favicon-16x16.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('ciriajasa-assets/img/favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="180x180" href="{{ asset('ciriajasa-assets/img/favicons/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('ciriajasa-assets/img/favicons/android-icon-192x192.png') }}">
    <link rel="stylesheet" href="{{ asset('ciriajasa-assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
    <link rel="stylesheet" href="{{ asset('ciriajasa-assets/fonts/fontawesome-all.min.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.0/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
    <link rel="stylesheet" href="{{ asset('ciriajasa-assets/css/scss/my-custom.compiled.css') }}">
</head>

<body style="background: rgb(255,255,255);">
    <!-- Start: Header Login -->
    <header>
        <!-- Start: 1 Row 1 Column -->
        <div class="container" style="padding-top: 20px;">
            <div class="row">
                <div class="col-md-12"><a href="{{url('')}}"><img class="img-fluid" src="{{ asset('ciriajasa-assets/img/ciriajasa-1.png') }}"></a></div>
            </div>
        </div><!-- End: 1 Row 1 Column -->
    </header><!-- End: Header Login -->
    <main class="page login-page main-content-page">
        <section class="clean-block clean-form dark" style="background: rgb(255,255,255);">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-sm-12 col-md-6 order-2 order-md-1">
                        <div class="block-heading" style="margin-bottom: 0px;">
                            <h2 class="text-start text-info my-primary-color" style="font-weight: bold;">E-Library Login</h2>
                            <p class="text-start" style="margin: 0px;text-align: justify;">E-library Ciriajasa merupakan platform untuk mengakses perpustakaan digital,berisikan pengalaman serta proyek -proyek yang sudah diselesaikan oleh PT. Ciriajasa E.C.&nbsp; klik di sini untuk&nbsp;<a class="my-primary-color" href="{{ route('register') }}" style="font-weight: bold;">Register!</a></p>
                        </div>
                        <form class="custom-style-form-login" style="margin: 0px;" method="post" action="{{ route('login') }}" id="form-id">
                            @csrf

                            

                            <div class="mb-3"><label class="form-label" for="email" style="font-weight: bold;">Email</label><input class="form-control form-control-lg item" name='email' type="email" id="email" inputmode="email" autocomplete="off" placeholder="Email" value="">
                                <div class="invalid-feedback"><span>Email is not valid</span></div>
                            </div>
                            <div class="mb-3"><label class="form-label" for="password" style="font-weight: bold;">Password</label>
                                <div class="input-group"><input class="form-control form-control-lg" name='password' type="password" id="password" placeholder="Password" autocomplete="off" value=""><span class="input-group-text bg-white"><i class="far fa-eye-slash toggle-password-icon"></i></span></div>
                            </div>

                            <div class="d-flex justify-content-between mb-3">
                                <div class="form-check"><input class="form-check-input" type="checkbox" name="remember" id="remember">
                                    <label class="form-check-label" for="checkbox">Ingatkan Saya</label>
                                </div>
                                <a class="my-primary-color" href="{{ route('lupasandi') }}" style="font-weight: bold;">Lupa Password?</a>
                            </div><button class="btn btn-primary my-button-primary" type="submit" style="width: 100%;height: 3rem;">Log In</button>
                        </form>
                    </div>
                    <div class="col-sm-12 col-md-6 order-1 order-md-2"><img class="img-fluid" src="{{ asset('ciriajasa-assets/img/library.gif') }}" alt="login-image"></div>
                </div>
            </div>
        </section>
    </main>
    @include('template.footer')
    <script src="{{ asset('ciriajasa-assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('ciriajasa-assets/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.6.0/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.0/dist/sweetalert2.all.min.js"></script>
    <script src="{{ asset('ciriajasa-assets/js/auth.js') }}"></script>

    @if (count($errors) > 0)
    <script>
       Swal.fire({
          title: 'Login Gagal !',
          text: 'Mohon Masukan Alamat Email dan Password Akun anda yang sesuai !',
          icon: 'error',
          showCancelButton: false,
          confirmButtonText: 'OK',
          cancelButtonText: 'Ya',
          customClass: {
            confirmButton: 'btn btn-primary mr-2',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    })
</script>
@endif

<script>
    $('#form-id').submit(function (e) {
        e.preventDefault();

        var baseurl = "{{ url('') }}"

        if ($('#remember').is(':checked') == true){
            localStorage.setItem("login_elibrary_remember", 'true');
            localStorage.setItem("login_elibrary_username", $('#email').val());
            localStorage.setItem("login_elibrary_password", $('#password').val());

        }
        else{
            localStorage.setItem("login_elibrary_remember", 'false');
            localStorage.setItem("login_elibrary_username", '');
            localStorage.setItem("login_elibrary_password", '');
        }



        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ route('login') }}",
            method: 'post',
            data:{email:$('#email').val(),password:$('#password').val()},
            success:function(response){

                Swal.fire({
                    title: 'Login Berhasil !',
                    text: 'Selamat Datang !',
                    icon: 'success',
                    showCancelButton: false,
                    confirmButtonText: 'OK',
                    cancelButtonText: 'Ya',
                    customClass: {
                        confirmButton: 'btn btn-primary mr-2',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                })
                window.location = baseurl+'/dashboard';
            },
            error:function(response){

                Swal.fire({
                  title: 'Login Gagal !',
                  text: 'Mohon Masukan Alamat Email dan Password Akun anda yang sesuai !',
                  icon: 'error',
                  showCancelButton: false,
                  confirmButtonText: 'OK',
                  cancelButtonText: 'Ya',
                  customClass: {
                    confirmButton: 'btn btn-primary mr-2',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })
            }
        })

        return false;
    })


    $(function () {

        if(localStorage.getItem("login_elibrary_remember") == 'true'){            
            $('#remember').attr('checked','checked')
            $('#email').val(localStorage.getItem("login_elibrary_username"))
            $('#password').val(localStorage.getItem("login_elibrary_password"))
        }

       

    });


</script>
</body>

</html>