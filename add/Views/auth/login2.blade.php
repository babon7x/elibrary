@include('template.app-login')



<br><br>
<div class="container-fluid" >
  <a href="/register" class="btn btn-primary btn-round">Register</a>
  <a href="/dashboard" class="btn btn-primary btn-round">Dashboard</a>
  <a href="/lupasandi" class="btn btn-primary btn-round">Lupa Password</a>

  <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
      <div class="card">
        <div class="card-body">
          <div class="card-title">
            <center>
              <h4>Login</h4>
            </center>
          </div>

          <br>
          <br>


          <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group row undisplay">
              <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('email') }}</label>

              <div class="col-md-6">
                <input id="email2" type="text" class="form-control @error('email') is-invalid @enderror" name="email2" value="{{ old('email') }}" autocomplete="email" disabled>

                @error('email')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row undisplay">
              <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

              <div class="col-md-6">
                <input id="password2" type="password" class="form-control @error('password') is-invalid @enderror" name="password2" autocomplete="current-password" disabled="">

                @error('password')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>


            <div class="form-group mb-3">
              <div class="input-group input-group-merge input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text text-primary"><i class="fa fa-user"></i></span>
                </div>
                <input id="email" type="text" class="form-control {{-- @error('email') is-invalid @enderror --}}" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="email">


               {{--  @error('email')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror --}}
              </div>
            </div>

            <br>


            <div class="form-group">
              <div class="input-group input-group-merge input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text text-primary"><i class="fa fa-lock"></i></span>
                </div>
                <input id="password" type="text" class="form-control {{-- @error('password') is-invalid @enderror --}}" name="password" required autocomplete="current-password" placeholder="password" style="-webkit-text-security: disc;">

{{--                 @error('password')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror --}}
              </div>
            </div>

            <br><br>

            <div class="custom-control custom-control-alternative custom-checkbox undisplay">
              <input class="custom-control-input" id=" customCheckLogin" type="checkbox">
              <label class="custom-control-label" for=" customCheckLogin">
                <span class="text-muted">Remember me</span>
              </label>
            </div>
            <div class="text-center">
              <button type="submit" class="btn btn-primary my-4" style="width: 50%;">Masuk</button>
            </div>
          </form>
        </div>


      </div>
    </div>
    <div class="col-md-4"></div>
  </div>
</div>

<link rel="stylesheet" href="{{ asset('swal2/sweetalert2.min.css') }}">
<script src="{{ asset('swal2/sweetalert2.all.min.js') }}"></script>

@if (count($errors) > 0)
<script>
 Swal.fire({
  title: 'Login Gagal !',
  text: 'Mohon Masukan Alamat Email dan Password Akun anda yang sesuai !',
  icon: 'error',
  showCancelButton: false,
  confirmButtonText: 'OK',
  cancelButtonText: 'Ya',
  customClass: {
    confirmButton: 'btn btn-primary mr-2',
    cancelButton: 'btn btn-danger'
  },
  buttonsStyling: false
})
</script>
@endif
