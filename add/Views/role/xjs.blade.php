<link href="{{asset('datatable/datatables.css')}}"></link>
<script src="{{asset('datatable/datatables.js')}}"></script>
<script src="{{asset('assets/dt_master.js')}}"></script>
<script src="{{asset('assets/data.js')}}"></script>

<script>
	$(".select2").select2({dropdownAutoWidth : true});
	$(".numberFormat").each(function() {
		new AutoNumeric(this,
		{
			watchExternalChanges: true,
			allowDecimalPadding: false
		});
	});
	$(".datepicker").datepicker({
		format: "dd M yyyy"
	});

	var url = "role";
	var dataColum = [
	{id:null,
		"render": function ( data, type, full, meta ) {
			let baris =meta.row+1;
			let defaultContent = baris+
			'<div class="btn-group">'+
			'<div class="dropdown-menu animated bounceIn rowid" id="rowid'+full.id+'">'+
			'<a class="dropdown-item text-primary" href="#" data-id="'+full.id+'" onclick=editDataRole(this)>Lihat/Ubah</a>'+
			'<a class="dropdown-item text-danger" href="#" data-id="'+full.id+'" onclick=deleteData(this)>Hapus</a>'+
			'</div>';
			return  defaultContent;
		}
	}];
	dataColum.push({data: 'nama', name: 'nama'})
	dtMaster(url,dataColum);

	var datas = {}

	$('.akses').on('change',function(){
		if ($(this).is(':checked') == true){
			$(this).parent().next().children().prop('checked',true)
			$(this).parent().next().next().children().prop('checked',true)
			$(this).parent().next().next().next().children().prop('checked',true)
			$(this).parent().next().next().next().next().children().prop('checked',true)
			$(this).parent().next().next().next().next().next().children().prop('checked',true)
		}
		if ($(this).is(':checked') == false){
			$(this).parent().next().children().prop('checked',false)
			$(this).parent().next().next().children().prop('checked',false)
			$(this).parent().next().next().next().children().prop('checked',false)
			$(this).parent().next().next().next().next().children().prop('checked',false)
			$(this).parent().next().next().next().next().next().children().prop('checked',false)
		}
	})

	function checkAll(){
		$('.store').prop('checked',true)
	}

	function uncheckAll(){
		$('.store').prop('checked',false)
	}

	function editDataRole(ini){
		$('.list').removeClass('fadeInDown')
		$('.list').addClass('undisplay')

		$('.form').removeClass('undisplay')
		$('.form').addClass('fadeInDown')

		spinnerStart();

		let id = $(ini).attr('data-id');

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: url+'/getdata',
			method: 'POST',
			data:{id:id},
			success: function(response){
				$.each(response.role[0],function(i,v){
					$('[store="'+i+'"]').val(v).change();

					if ($('[store="'+i+'"]').hasClass('select2')){
						$('[store="'+i+'"]').val(v)
					}
				})

				$.each(response.role_akses,function(i,v){
					$("[data-url="+ v.url +"]").children(':nth-child(3)').children().prop('checked',true)
					if (v.lihat == '1'){$("[data-url="+ v.url +"]").children(':nth-child(4)').children().prop('checked',true)}
						if (v.tambah == '1'){$("[data-url="+ v.url +"]").children(':nth-child(5)').children().prop('checked',true)}
							if (v.ubah == '1'){$("[data-url="+ v.url +"]").children(':nth-child(6)').children().prop('checked',true)}
								if (v.hapus == '1'){$("[data-url="+ v.url +"]").children(':nth-child(7)').children().prop('checked',true)}
									if (v.download == '1'){$("[data-url="+ v.url +"]").children(':nth-child(8)').children().prop('checked',true)}
								})

			}
		})
		spinnerEnd();

	}

	function goSaveRole(){

		spinnerStart()

		var data = []
		var dataRow = {}
		var value = ''
		var valueRow = ''
		var akses = ''
		var lihat = 0
		var tambah = 0
		var ubah = 0
		var hapus = 0
		var download = 0
		var method = 'post'
		var targetURL = 'role'

		$('#form-table>tbody>tr').each(function(i,v){
			value = $(v).attr('data-url')
			akses = $(v).children(':nth-child(3)').children().is(':checked')
			dataRow = {}
			if (akses == true){

				if ($(v).children(':nth-child(4)').children().is(':checked') == true)
					{lihat = 1}
				if ($(v).children(':nth-child(5)').children().is(':checked') == true)
					{tambah = 1}
				if ($(v).children(':nth-child(6)').children().is(':checked') == true)
					{ubah = 1}
				if ($(v).children(':nth-child(7)').children().is(':checked') == true)
					{hapus = 1}
				if ($(v).children(':nth-child(8)').children().is(':checked') == true)
					{download = 1}

				dataRow = {
					url:$(v).children(':nth-child(2)').children().val(),
					lihat:lihat,
					tambah:tambah,
					ubah:ubah,
					hapus:hapus,
					download:download
				}
				data.push(dataRow)
			}
		})

		if ($('[store="nama"]').val() == ''){
			notify(
				"top",
				"center",
				"danger",
				"inverse",
				"animated fadeInDown",
				"animated fadeOutDown",
				"bg-danger",
				"gagal",
				"isi nama role !"
				);
		}

		if ($('[store="id"]').val() != ''){
			method = 'patch'
			targetURL = 'role/update'
			datas = {id:$('[store="id"]').val(),nama:$('[store="nama"]').val(),data:data}
		}
		else{	
			datas = {nama:$('[store="nama"]').val(),data:data}
		}


		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: targetURL,
			method: method,
			data:datas,
			success: function(response){

				console.log(response)
				notify('top', 'center', 'success', 'inverse', 'animated fadeInDown', 'animated fadeOutDown' , 'bg-success','berhasil','simpan data');

				$('[store]').not('[store="id"]').each(function(i,v){
					$(v).removeClass('is-invalid');
					$(v).next('.invalid-feedback').addClass('invalid-feedback');
				});

				backToList();
				dtMaster('role',dataColum);
				spinnerEnd();

			},
			error: function(response){
				console.log(response)
				let errors = response.responseJSON.errors;
				let urutan = 0;
				let focus = '';

				$.each(errors,function(i,v){
					urutan=urutan+1;
					if (urutan==1){
						index_response = i;
						info_response = v[0];
					}
				})

				$('[store]').not('[store="id"]').each(function(i,v){
					$(v).removeClass('is-invalid');
					$(v).next('.invalid-feedback').addClass('invalid-feedback');

				});

				urutan = 0;
				$.each(errors,function(i,v){
					urutan=urutan+1;
					$('[store="'+i+'"]').addClass('is-invalid');
					// $('[store="'+i+'"]').next().text(v);
					if (urutan == 1){
						focus = i;
					}
				})

				notify('top', 'center', 'danger', 'inverse', 'animated fadeInDown', 'animated fadeOutDown' , 'bg-danger',index_response,info_response);

				spinnerEnd();
				$('[store="'+focus+'"]').focus();
			}
		})
	}
	
</script>