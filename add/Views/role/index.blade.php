@include("template.app")

<div class="main-content container">

	<div class="card list animated fadeInDown">
		<div class="card-body">
			<div class="card-title">
				<div class="row">
					<div class="col-md-6">
						<h5>List Data Role</h5>
					</div>
					<div class="col-md-6 text-right">
						<button class="btn btn-primary btn-round" onclick="newData()">New Data</button>

					</div>
				</div>
			</div>

			<table id="datatable" class="table table-sm table-bordered">
				<thead>
					<tr>
						<th with="80">no</th>
						<th>nama</th>
					</tr>
				</thead>

			</table>

		</div>
	</div>

	@include("role.form")
	@include("role.xjs")

</div>