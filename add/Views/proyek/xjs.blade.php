<script src="{{asset('assets/dt_master.js?t=date("YmdHis")')}}"></script>
<script src="{{asset('assets/data.js?version=1.1')}}"></script>
<script src="{{asset('jquery/jquery.form.min.js')}}"></script>
<script src="{{asset('jquery/jquery.jspdf.umd.min.js')}}"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js'></script>
<script>
	
	$(".select2").select2({dropdownAutoWidth : true});
	// $(".numberFormat").each(function() {
	// 	new AutoNumeric(this,
	// 	{
	// 		watchExternalChanges: true,
	// 		allowDecimalPadding: false,

	// 	});
	// });
	$(".datepicker").datepicker({
		format: "dd M yyyy",
		autoClose: true
	})

	var baseurl = "{{ url('') }}"
	console.log(baseurl)

	var user_id = '<?php echo($user_id); ?>';


	var topProyekCount = '<?php echo($top_proyek_count); ?>';
	var page = '<?php echo($page); ?>';

	if(page == 'chart'){
		$('#header_chart').addClass('btn btn-primary text-white')
	}
	else if (page =='dashboard'){
		$('#header_home').addClass('btn btn-primary text-white')
	}
	else if (page =='create'){
		$('#header_upload').addClass('btn btn-primary text-white')
	}
	else if (page =='list'){
		$('#header_view').addClass('btn btn-primary text-white')
	}
	else if (page !='dashboard'){
		$('#header_view').addClass('btn btn-primary text-white')
	}

	var dataSearch = '';
	var selectYearStart = $('#selectYearStart').val()
	var selectYear = $('#selectYear').val()
	var url = "{{ route('proyek.list') }}";
	url = url.replace('/list','');
	if (user_id != ''){		
		var dataColum = [
		{id:null,
			"render": function ( data, type, full, meta ) {
				let baris =meta.row+1;
				let defaultContent = '<input type="checkbox" class="checkRow dt-id" is_top_proyek="'+full.top_proyek+'" data-nama-paket-pekerjaan="'+full.nama_paket_pekerjaan+'"></input>';
				return  defaultContent;
			}	
		},
		{id:null,
			"render": function ( data, type, full, meta ) {
				let baris =meta.row+1;
				let defaultContent = baris;
				return  defaultContent;
			}
		}];
	}
	else{
		var dataColum = [
		{id:null,
			"render": function ( data, type, full, meta ) {
				let baris =meta.row+1;
				let defaultContent = baris;
				return  defaultContent;
			}
		}];
	}

	dataColum.push({data: 'nama_paket_pekerjaan', name: 'nama_paket_pekerjaan'})
	dataColum.push({data: 'lokasi_proyek', name: 'lokasi_proyek'})
	dataColum.push({id:null,'render': function ( data, type, full, meta ) {
		let tahunAwal = moment(full.waktu_pelaksanaan_mulai).format('yyyy');
		let tahunAkhir = moment(full.waktu_pelaksanaan_selesai).format('yyyy');
		if (tahunAwal == tahunAkhir){
			return  tahunAwal;
		}
		else{
			return tahunAwal+' - '+tahunAkhir
		}
	} });

	dtMasterProyek(url,dataColum,selectYearStart,selectYear,'');

	function listTopProyek(){
		dtMasterProyek(url+'/topproyek',dataColum,selectYearStart,selectYear,'');
		$('#ddListTopProyek').addClass('bg-primary text-white')
		$('#btnEditProyek').addClass('undisplay')
		$('#btnDeleteProyek').addClass('undisplay')
		$('#btnRemoveTopProyek').removeClass('undisplay')
	}

	function listAllProyek(){
		dtMasterProyek(url,dataColum,selectYearStart,selectYear,'');
	}

	function removeTopProyek(){
		var checkes = []
		var rowid = ''
		var datas = {}

		$('#datatable tbody tr').each(function(i,v){
			rowid = $(v).attr('rowid')
			if ($(v).children(':nth-child(1)').children().is(':checked') == true){
				checkes.push(rowid)
			}
		})

		if (checkes.length <1){
			Swal.fire({
				title: 'Gagal',
				text: 'Pilih Minimal 1 Data !',
				icon: 'warning',
				showCancelButton: false,
				confirmButtonText: 'OK',
				cancelButtonText: 'Ya',
				customClass: {
					confirmButton: 'btn btn-primary mr-2',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})
		}
		else{
			datas = {ids:checkes}
			Swal.fire({
				title: 'Hapus '+checkes.length+' Data',
				text: 'data tidak dapat dikembalikan !',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Tidak',
				cancelButtonText: 'Ya',
				customClass: {
					confirmButton: 'btn btn-primary mr-2',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			}).then((result) => {
				if (!result.isConfirmed) {
					spinnerStart();

					$.ajax({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
						url: "{{ route('proyek.remove') }}",
						method: 'post',
						data:datas,
						success: function(response){

							listTopProyek();
							$('.checkRow').prop('checked',false)
							$('#selectAll').prop('checked',false)
							// dtMasterProyek(url,dataColum,selectYearStart,selectYear);

							spinnerEnd()
							Swal.fire({
								title: 'Berhasil',
								text: 'Hapus Data Top Proyek!',
								icon: 'success',
								showCancelButton: false,
								confirmButtonText: 'OK',
								cancelButtonText: 'Ya',
								customClass: {
									confirmButton: 'btn btn-primary mr-2',
									cancelButton: 'btn btn-danger'
								},
								buttonsStyling: false
							})
						},
						error: function(response){
							spinnerEnd()
							Swal.fire({
								title: 'Gagal',
								text: 'Terjadi Kesalahan !',
								icon: 'error',
								showCancelButton: false,
								confirmButtonText: 'OK',
								cancelButtonText: 'Ya',
								customClass: {
									confirmButton: 'btn btn-primary mr-2',
									cancelButton: 'btn btn-danger'
								},
								buttonsStyling: false
							})
						}
					})


				}
			});


		}
	}
	

</script>