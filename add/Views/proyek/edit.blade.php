@extends("template.app_fe")
@section('content')

<!-- include summernote css/js -->

@section('customStyles')
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
@endsection

<main class="page landing-page main-content-page">
	<section class="clean-block about-us">
		<div class="container">
			<!-- Start: required for first child -->
			<div class="block-heading mobile__mb-0"></div><!-- End: required for first child -->
			<div class="row justify-content-center">
				<div class="col-12 col-md-8">
					<form action="/proyek" enctype="multipart/form-data" method="POST" id="formProyek">
						@csrf
						<input type="text" name="id" class="undisplay" id="idProyek" value="{{ $datas[0]->id }}">
						<div class="mb-3"><label class="form-label" for="upload">Upload Cover Proyek</label>
							<div class="custom-file-container input-group">
								<div class='undisplay'>
									<input class="form-control form-control-lg custom-file-input upload"
									name="upload_cover_proyek" id="upload" type="file" style="flex: 5;"><button
									class="btn btn-primary btn-lg form-control-lg my-button-primary select-file-btn"
									type="button" style="flex: 1;"><i class="fas fa-upload fa-fw"></i>Upload</button>
								</div>
								<input type="text" value="file.jpg" id="coverkw" class="form-control form-control-lg" readonly>
							</div>
						</div>
						<div class="mb-3"><label class="form-label" for="upload2">Upload File Kontrak</label>
							<div class="custom-file-container input-group">
								<div class='undisplay'>
									<input class="form-control form-control-lg custom-file-input" type="file"
									name="upload_file_kontrak" id="upload2" style="flex: 5;"><button
									class="btn btn-primary btn-lg form-control-lg my-button-primary select-file-btn"
									type="button" style="flex: 1;"><i class="fas fa-upload fa-fw"></i>Upload</button>
								</div>
								<input type="text" value="kontrak.pdf" id="kontrakkw" class="form-control form-control-lg" readonly>
							</div>
						</div>
						<div class="mb-3"><label class="form-label" for="penggunaJasa">Pengguna Jasa</label><input
							class="form-control form-control-lg form-control" type="text" value="{{$datas[0]->pengguna_jasa}}" name="pengguna_jasa" id="penggunaJasa"
							placeholder="Masukkan pengguna jasa"></div>
							
							<div class="mb-3"><label class="form-label" for="paketPekerjaan">Nama Paket
							Pekerjaan</label><input class="form-control form-control-lg form-control" type="text"
							id="paketPekerjaan" placeholder="Masukkan nama paket pekerjaan" value="{{$datas[0]->nama_paket_pekerjaan}}" name="nama_paket_pekerjaan"></div>
							
							<div class="mb-3"><label class="form-label" for="lingkupProdukUtama">Lingkup Produk
							Utama</label>
							<!-- Start: Suneditor WYSIWYG -->
							<div>
								<div id="toolbar_container-1" class="sun-editor"></div><textarea class="form-control"
								id="lingkupProdukUtama"
								placeholder="Masukkan lingkup produk utama" name="lingkup_produk_utama">{{$datas[0]->lingkup_produk_utama}}</textarea>
							</div><!-- End: Suneditor WYSIWYG -->
						</div>
						
						<div class="mb-3"><label class="form-label" for="lokasiProyek">Lokasi Proyek</label><input
							class="form-control form-control-lg form-control" type="text" id="lokasiProyek"
							placeholder="Masukkan lokasi proyek" name="lokasi_proyek" value="{{$datas[0]->lokasi_proyek}}"></div>
							
							<div class="mb-3"><label class="form-label" for="nilaiKontrak">Nilai Kontrak</label><input
								class="form-control form-control-lg form-control" type="text" id="nilaiKontrak"
								placeholder="Masukkan nilai kontrak" name="nilai_kontrak" value="{{$datas[0]->nilai_kontrak}}"></div>
								
								<div class="mb-3"><label class="form-label" for="nomorKontrak">Nomor Kontrak</label><input
									class="form-control form-control-lg form-control" type="text" id="nomorKontrak"
									placeholder="Masukkan nomor kontrak" name="nomor_kontrak" value="{{$datas[0]->nomor_kontrak}}"></div>
									
									<div class="mb-3">
										<div class="row">
											<div class="col"><label class="form-label" for="waktu_pelaksanaan_mulai">Waktu Pelaksanaan
											- Mulai</label><input class="form-control form-control-lg datepicker"
											id="waktu_pelaksanaan_mulai" autocomplete="off" name="waktu_pelaksanaan_mulai"></div>
											<div class="col"><label class="form-label" for="waktu_pelaksanaan_selesai">Waktu Pelaksanaan -
											Selesai</label><input class="form-control form-control-lg datepicker"
											id="waktu_pelaksanaan_selesai" autocomplete="off" name="waktu_pelaksanaan_selesai"></div>
										</div>
									</div>
									
									<div class="mb-3"><label class="form-label" for="namaPimpinanKontrak">Nama Pimpinan
									Kontrak</label><input class="form-control form-control-lg form-control" type="text"
									id="namaPimpinanKontrak" placeholder="Masukkan nama pimpinan kontrak" name="nama_pimpinan_kontrak" value="{{$datas[0]->nama_pimpinan_kontrak}}" ></div>
									

									<div class="mb-3">
										<label class="form-label" for="alamat">Alamat</label><input class="form-control form-control-lg form-control" type="text" id="alamat" name="alamat" placeholder="Masukkan alamat" value="{{$datas[0]->alamat}}">
									</div>
									<div class="mb-3">
										<label class="form-label" for="negaraAsal">Negara Asal</label><input class="form-control form-control-lg form-control" type="text" id="negaraAsal" name="negara_asal" value="{{$datas[0]->negara_asal}}" placeholder="Masukkan negara asal">
									</div>
									<br>
									<div class="row">
										<div class="col-md-12">

											<label class="form-label">Jumlah Tenaga Ahli</label>
										</div>
									</div>
									<div class="row">                                        
										<div class="col-md-4">        
											<div class="mb-3">
												<label class="form-label" for="namaPimpinanKontrak">Tenaga Ahli Asing</label>
												<input class="form-control form-control-lg form-control" type="text"
												id="jumlahTenagaAsing" readonly >
											</div>
										</div>
										<div class="col-md-4">        
											<div class="mb-3">
												<label class="form-label" for="namaPimpinanKontrak">Tenaga Ahli Indonesia</label>
												<input class="form-control form-control-lg form-control" type="text"
												id="jumlahTenagaIndonesia" readonly  >
											</div>
										</div>
										<div class="col-md-4">        
											<div class="mb-3">
												<label class="form-label" for="namaPimpinanKontrak">Total Tenaga Ahli</label>
												<input class="form-control form-control-lg form-control" type="text"
												id="jumlahTenagaAhli" readonly  >
											</div>
										</div>
									</div>
									
							{{-- <div class="mb-3">
								<div class="row">
									<div class="col-12 col-md-6 mobile__mb-1rem"><label class="form-label"
											for="waktuPelaksanaanStart">Jumlah Tenaga Ahli - Tenaga Ahli Asing</label><input
											class="form-control form-control-lg" type="number" id="waktuPelaksanaanStart">
									</div>
									<div class="col-12 col-md-6"><label class="form-label" for="waktuPelaksanaanEnd">Jumlah
											Tenaga Ahli - Tenaga Ahli indonesia</label><input
											class="form-control form-control-lg" type="number" id="waktuPelaksanaanEnd">
									</div>
								</div>
							</div> --}}
							
							<div id="mitraContainer" class="mt-4">
								<!-- Start: mitra header -->
								<div class="d-flex justify-content-between align-items-center">
									<h5 style="margin: 0px;">Perusahaan mitra kerja</h5><!-- Start: Button Icon Round -->
									<div><a onclick="addPerusahaan()" class="btn btn-info btn-floating" role="button" id="addMitraKerjaBtn"><i
										class="fas fa-plus"></i></a></div><!-- End: Button Icon Round -->
									</div><!-- End: mitra header -->
									<hr class='row-perusahaan' style="margin-top: 0px;">
								</div>
								
								<div id="tenagaAhliContainer" class="mt-4">
									<!-- Start: tenaga ahli header -->
									<div class="d-flex justify-content-between align-items-center">
										<h5 style="margin: 0px;">Tenaga Ahli Tetap Yang Terlibat</h5>
										<!-- Start: Button Icon Round -->
										<div><a onclick="addDetail()" class="btn btn-info btn-floating" role="button"><i class="fas fa-plus"
											id="addTenagaAhli"></i></a></div><!-- End: Button Icon Round -->
										</div><!-- End: tenaga ahli header -->
										<hr class='row-detail' style="margin-top: 0px;">
										
									</div>
									
									<div class="d-flex flex-column flex-md-row mt-5">{{-- <button
										class="btn btn-outline-primary my-button-outline-primary mobile__mb-1rem mobile__mlr-0"
										type="button"
										style="margin-right: 10px;padding-right: 10%;padding-left: 10%;">Kembali</button> --}}<button
										class="btn btn-primary my-button-primary" type="submit"
										style="padding-right: 10%;padding-left: 10%;">Simpan</button></div>
									</form>
								</div>
							</div>
						</div>
					</section>
				</main><!-- Start: Footer Dark -->

				@include("template.footer")
				@endsection

				@section('js')

				<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
				<script>
					$(function(){
						$('#lingkupProdukUtama').summernote({
							placeholder: 'Masukkan lingkup produk utama',
							tabsize: 2,
							height: 200,
							toolbar: [
							['style', ['style']],
							['font', ['bold', 'underline', 'clear']],
							['color', ['color']],
							['para', ['ul', 'ol', 'paragraph']],
							['table', ['table']],
							['insert', ['link', 'picture', 'video']],
							['view', ['fullscreen', 'codeview', 'help']]
							]
						});
					})
				</script>

				@include("proyek.yjs")
				@include("proyek.zjs")

				<script>
					
					$('#coverkw').on('click',function(){
						$('#upload').click()
					})

					$('#upload').on('change',function(){
						val = $('#upload').val()
						$('#coverkw').val(val.replace(/C:\\fakepath\\/i, ''))
					})

					$('#kontrakkw').on('click',function(){
						$('#upload2').click()
					})

					$('#upload2').on('change',function(){
						val = $('#upload2').val()
						$('#kontrakkw').val(val.replace(/C:\\fakepath\\/i, ''))
					})


					datas = <?php echo($datas); ?>;


					datas = JSON.stringify(datas);
					datas = JSON.parse(datas);

					

					$('#waktu_pelaksanaan_mulai').val(moment(datas[0].waktu_pelaksanaan_mulai).format('DD MMM yyyy'))
					$('#waktu_pelaksanaan_selesai').val(moment(datas[0].waktu_pelaksanaan_selesai).format('DD MMM yyyy'))
					

					$.each(datas[0].perusahaan_child,function(i,v){
						let template = 
						`
						<div class="mb-3 row-perusahaan perusahaan-data">
						<div class="row align-items-end">
						<div class="col-12 col-md-5">
						<label class="form-label">Nama Perusahaan</label>
						<input class="form-control form-control-lg"  name="nama[]" type="text" value="${v.nama}">
						</div>
						<div class="col-12 col-md-3">
						<label class="form-label">Asing</label>
						<input class="form-control form-control-lg numberFormat" name="asing[]" type="text" value="${v.asing}">
						</div>
						<div class="col-12 col-md-3">
						<label class="form-label">Indonesia</label>
						<input class="form-control form-control-lg numberFormat" type="text" name="indonesia[]" value="${v.indonesia}">
						</div>
						<div class="col-12 col-md-1 d-md-flex justify-content-md-end d-grid">
						<!-- Start: Button Icon Round -->
						<div class="d-none d-sm-none d-md-block">
						<a class="btn btn-danger btn-floating" role="button" onclick="removePerusahaan(this)"><i class="far fa-trash-alt"></i></a></div>
						<!-- End: Button Icon Round -->
						<button class="btn btn-danger d-block d-sm-block d-md-none text-white mt-3" type="button" onclick="removePerusahaan(this)">Hapus</button>
						</div>
						</div>
						</div>
						`
						$('.row-perusahaan').last().after(template);
					})

					$.each(datas[0].detail_child,function(i,v){
						let template2 =
						`
						<div class="mb-3 row-detail detail-data">
						<div class="row align-items-end">
						<div class="col-12 col-md-4">
						<label class="form-label">Posisi</label>
						<input class="form-control form-control-lg" type="text" name="posisi[]" value="${v.posisi}">
						</div>
						<div class="col-12 col-md-4">
						<label class="form-label">Keahlian</label>
						<input class="form-control form-control-lg" type="text" name="keahlian[]" value="${v.keahlian}">
						</div>
						<div class="col-12 col-md-3">
						<label class="form-label">Jumlah Orang Bulan</label>
						<input class="form-control form-control-lg numberFormat" type="text" name="jumlah_orang[]" value="${v.jumlah_orang}">
						</div>
						<div class="col-12 col-md-1 d-md-flex justify-content-md-end d-grid">
						<!-- Start: Button Icon Round -->
						<div class="d-none d-sm-none d-md-block">
						<a onclick="removeDetail(this)" class="btn btn-danger btn-floating" role="button"><i class="far fa-trash-alt"></i></a>
						</div>
						<!-- End: Button Icon Round -->
						<button class="btn btn-danger d-block d-sm-block d-md-none text-white mt-3" type="button" onclick="removeDetail(this)">Hapus</button>
						</div>
						</div>
						</div>
						`
						$('.row-detail').last().after(template2)

					})

					sumAsing()
					sumIndonesia()
					sumAhli()
				</script>
				@endsection