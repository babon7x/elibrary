
<script>
	$(function() {

		$('#selectAll').click(function(){
			if ($(this).is(':checked') == true){
				$('.checkRow').prop('checked',true)
			}
			else{
				$('.checkRow').prop('checked',false)
			}
		})


		$(document).on("change", "#upload", function() {
			fileName = $(this).val().replace(/C:\\fakepath\\/i, '')
			fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
			fileSize = this.files[0].size;

			if ((fileNameExt != 'png') && (fileNameExt != 'jpg') && (fileNameExt != 'jpeg') 
				&& (fileNameExt != 'pdf')
				){
				$('#upload').val('')
			Swal.fire({
				title: 'Upload File',
				text: 'Format yang di perbolehkan (png/jpg/JPEG/pdf) !',
				icon: 'warning',
				showCancelButton: false,
				confirmButtonText: 'OK',
				cancelButtonText: 'Ya',
				customClass: {
					confirmButton: 'btn btn-primary mr-2',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})
			$('#coverkw').val('file.pdf')
			return false;
		}

		if (fileSize > 5000000){
			$('#upload').val('')
			Swal.fire({
				title: 'Upload File',
				text: 'Ukuran yang di perbolehkan (5Mb) !',
				icon: 'warning',
				showCancelButton: false,
				confirmButtonText: 'OK',
				cancelButtonText: 'Ya',
				customClass: {
					confirmButton: 'btn btn-primary mr-2',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})
			$('#coverkw').val('file.pdf')

			return false;
		}

	});


		$(document).on("change", "#upload2", function() {
			fileName = $(this).val().replace(/C:\\fakepath\\/i, '')
			fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
			fileSize = this.files[0].size;

			if ((fileNameExt != 'pdf') && (fileNameExt != 'PDF')) {
				$('#upload2').val('')
				Swal.fire({
					title: 'Upload File',
					text: 'Format yang di perbolehkan (pdf) !',
					icon: 'warning',
					showCancelButton: false,
					confirmButtonText: 'OK',
					cancelButtonText: 'Ya',
					customClass: {
						confirmButton: 'btn btn-primary mr-2',
						cancelButton: 'btn btn-danger'
					},
					buttonsStyling: false
				})
				$('#kontrakkw').val('kontrak.pdf')
				return false;
			}

			if (fileSize > 5000000){
				$('#upload2').val('')
				Swal.fire({
					title: 'Upload File',
					text: 'Ukuran yang di perbolehkan (5Mb) !',
					icon: 'warning',
					showCancelButton: false,
					confirmButtonText: 'OK',
					cancelButtonText: 'Ya',
					customClass: {
						confirmButton: 'btn btn-primary mr-2',
						cancelButton: 'btn btn-danger'
					},
					buttonsStyling: false
				})
				$('#kontrakkw').val('kontrak.pdf')
				return false;
			}

		});


	});	

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('#selectYearStart').on('change',function(){
		var selectYearStart = $('#selectYearStart').val()
		var selectYear = $('#selectYear').val()
		if (selectYearStart>selectYear){
			$('#selectYear').val(selectYearStart)
			selectYear = $('#selectYear').val()
		}
		dtMasterProyek(url,dataColum,selectYearStart,selectYear,'');
	})

	$('#selectYear').on('change',function(){
		var selectYearStart = $('#selectYearStart').val()
		var selectYear = $('#selectYear').val()
		if (selectYearStart>selectYear){
			$('#selectYearStart').val(selectYear)
			selectYearStart = $('#selectYearStart').val()
		}
		dtMasterProyek(url,dataColum,selectYearStart,selectYear,'');
	})

	$('#cari').on('keyup',function(e){
		var value = $(this).val()
		// $('#datatable_filter>label>input').val(value)
		// $('#datatable_filter>label>input').keyup()
		if(e.which == 13) {
			dataSearch = value
			console.log(dataSearch)
			dtMasterProyek(url,dataColum,selectYearStart,selectYear,dataSearch);
		}
	})

	$('#btnCari').on('click',function(e){
		
		dataSearch = $('#cari').val()
		dtMasterProyek(url,dataColum,selectYearStart,selectYear,dataSearch);

	})

	$('#formProyek').submit(function(e) {
		e.preventDefault();

		var cek = 0
		var cekId = $('#idProyek').val();
		var cekName = ''
		// spinnerStart()

		$('#formProyek [name]').each(function(i,v){
			name = $(v).attr('name');
			value = $(v).val();
			if (cekId !=''){
				if ((name != 'id') && (name != 'files') && (name != 'upload_cover_proyek') && (name != 'upload_file_kontrak')  && (name != 'posisi[]')  && (name != 'keahlian[]')  && (name != 'jumlah_orang[]')  && (name != 'nama[]') && (name != 'asing[]')  && (name != 'indonesia[]') && (name != 'lingkup_produk_utama') && (name != 'nama_pimpinan_kontrak') && (name != 'alamat')  && (name != 'negara_asal')) {
					if ((value == '') && (cekName=='')){
						cekName = name
						Swal.fire({
							title: cekName,
							text: 'Tidak Boleh Kosong !',
							icon: 'warning',
							showCancelButton: false,
							confirmButtonText: 'OK',
							cancelButtonText: 'Ya',
							customClass: {
								confirmButton: 'btn btn-primary mr-2',
								cancelButton: 'btn btn-danger'
							},
							buttonsStyling: false
						})
						cek = 1
					}
				}

				if ((name == 'lingkup_produk_utama') && (value == '')) {
					$(".note-editable>p").html('-');
					$(".note-editable>p").text('-');
					$(v).val('-')
					$('.note-placeholder').remove();
					value = '-'
				}

				if ((name == 'nama_pimpinan_kontrak') && (value == '')) {
					$(v).val('-')
					value = '-'
				}
				if ((name == 'alamat') && (value == '')) {
					$(v).val('-')
					value = '-'
				}
				if ((name == 'negara_asal') && (value == '')) {
					$(v).val('-')
					value = '-'
				}


			}
			else{

				if ((name != 'id') && (name != 'files') && (name != 'posisi[]')  && (name != 'keahlian[]')  && (name != 'jumlah_orang[]')  && (name != 'nama[]') && (name != 'asing[]')  && (name != 'indonesia[]') && (name != 'lingkup_produk_utama') && (name != 'nama_pimpinan_kontrak') && (name != 'alamat')  && (name != 'negara_asal')) {

					if ((value == '') && (cekName=='')){
						cekName = name
						Swal.fire({
							title: cekName,
							text: 'Tidak Boleh Kosong !',
							icon: 'warning',
							showCancelButton: false,
							confirmButtonText: 'OK',
							cancelButtonText: 'Ya',
							customClass: {
								confirmButton: 'btn btn-primary mr-2',
								cancelButton: 'btn btn-danger'
							},
							buttonsStyling: false
						})
						cek = 1
					}
				}

				if ((name == 'lingkup_produk_utama') && (value == '')) {
					$(".note-editable>p").html('-');
					$(".note-editable>p").text('-');
					$(v).val('-')
					$('.note-placeholder').remove();
					value = '-'
				}

				if ((name == 'nama_pimpinan_kontrak') && (value == '')) {
					$(v).val('-')
					value = '-'
				}
				if ((name == 'alamat') && (value == '')) {
					$(v).val('-')
					value = '-'
				}
				if ((name == 'negara_asal') && (value == '')) {
					$(v).val('-')
					value = '-'
				}


			}

		})

		cekDetailKosong = 0;
		cekDetailIsi = 0;
		$('.detail-data').each(function(i,v){
			posisi = $(v).find('[name="posisi[]"]').val()
			keahlian = $(v).find('[name="keahlian[]"]').val()
			jumlah_orang = $(v).find('[name="jumlah_orang[]"]').val()


			if( ((posisi=='') || (posisi=='-')) && ((keahlian=='') || (keahlian=='-')) && ((jumlah_orang=='') || (jumlah_orang=='0')) ){
				if (cekDetailKosong == 1){
					$(v).remove();
				}
				else{					
					$(v).find('[name="posisi[]"]').val('-')
					$(v).find('[name="keahlian[]"]').val('-')
					$(v).find('[name="jumlah_orang[]"]').val('0')
					$(v).addClass('datakosong')
					cekDetailKosong = 1;
				}
			}else{
				if (posisi == ''){
					$(v).find('[name="posisi[]"]').val('-')
				}
				if (keahlian ==''){
					$(v).find('[name="keahlian[]"]').val('-')
				}
				if (jumlah_orang ==''){
					$(v).find('[name="jumlah_orang[]"]').val('0')
				}
				cekDetailIsi = 1;
			}
		})

		if ($('.detail-data').length == 1){
			$('.detail-data').find('.btnRemoveDetail').addClass('undisplay')
		}
		if(cekDetailIsi == 1){
			$('.detail-data.datakosong').remove()
		}

		cekPerusahaanKosong = 0;
		cekPerusahaanIsi = 0;
		$('.perusahaan-data').each(function(i,v){
			nama = $(v).find('[name="nama[]"]').val()
			asing = $(v).find('[name="asing[]"]').val()
			indonesia = $(v).find('[name="indonesia[]"]').val()


			if( ((nama=='') || (nama=='-')) && ((asing=='') || (asing=='0')) && ((indonesia=='') || (indonesia=='0')) ){
				if (cekPerusahaanKosong == 1){
					$(v).remove();
				}
				else{					
					$(v).find('[name="nama[]"]').val('-')
					$(v).find('[name="asing[]"]').val('0')
					$(v).find('[name="indonesia[]"]').val('0')
					$(v).addClass('datakosong')
					cekPerusahaanKosong = 1;
				}
			}else{
				if (nama == ''){
					$(v).find('[name="nama[]"]').val('-')
				}
				if (asing ==''){
					$(v).find('[name="asing[]"]').val('0')
				}
				if (indonesia ==''){
					$(v).find('[name="indonesia[]"]').val('0')
				}
				cekPerusahaanIsi = 1;
			}
		})

		if ($('.perusahaan-data').length == 1){
			$('.perusahaan-data').find('.btnRemovePerusahaan').addClass('undisplay')
		}
		if(cekPerusahaanIsi == 1){
			$('.perusahaan-data.datakosong').remove()
		}

		if (cek == 1){
			return false;
		}

		var urltarget = "{{ route('proyek.store') }}"
		if (cekId !=''){
			urltarget = "{{ route('proyek.updates') }}"
		}

		var formData = new FormData(this);
		
		$.ajax({

			type:'POST',
			url: urltarget,
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				console.log(response)
				Swal.fire({
					title: 'Berhasil ',
					text: 'Upload dan Simpan Data !',
					icon: 'success',
					showCancelButton: false,
					confirmButtonText: 'OK',
					cancelButtonText: 'Ya',
					customClass: {
						confirmButton: 'btn btn-primary mr-2',
						cancelButton: 'btn btn-danger'
					},
					buttonsStyling: false

				})
				.then(function() {
					if (cekId != ''){
						location.href = "{{ route('proyek.index') }}";
					}else{

						location.href = "{{ route('proyek.create') }}";
					}
				})
				spinnerEnd();
			},
			error: function(response){
				console.log(response)

				if (response.responseJSON.message == 'This PDF document probably uses a compression technique which is not supported by the free parser shipped with FPDI. (See https://www.setasign.com/fpdi-pdf-parser for more details)'){
					Swal.fire({
						title: 'File PDF tidak support',
						text: 'Harap buka file pdf dengan WPS Office , dan simpan ulang menggunakan "save as"',
						icon: 'warning',
						showCancelButton: false,
						confirmButtonText: 'OK',
						cancelButtonText: 'Ya',
						customClass: {
							confirmButton: 'btn btn-primary mr-2',
							cancelButton: 'btn btn-danger'
						},
						buttonsStyling: false
					})
				}
				else{	
					$.each(response.responseJSON.errors,function(i,v){
						title = i
						text = v[0]
					})

					Swal.fire({
						title: title,
						text: text,
						icon: 'warning',
						showCancelButton: false,
						confirmButtonText: 'OK',
						cancelButtonText: 'Ya',
						customClass: {
							confirmButton: 'btn btn-primary mr-2',
							cancelButton: 'btn btn-danger'
						},
						buttonsStyling: false
					})
				}

				spinnerEnd()
			}
		});


	});


function addDetail(){
	$('.row-detail').last().after(
		`
		<div class="mb-3 row-detail detail-data">
		<div class="row align-items-end">
		<div class="col-12 col-md-4"><label class="form-label">Posisi</label>
		<input class="form-control form-control-lg" type="text" name="posisi[]">
		</div>
		<div class="col-12 col-md-4"><label class="form-label">Keahlian</label>
		<input class="form-control form-control-lg" type="text" name="keahlian[]">
		</div>
		<div class="col-12 col-md-3"><label class="form-label">Jumlah Orang Bulan</label>
		<input class="form-control form-control-lg numberFormat" type="text" name="jumlah_orang[]">
		</div>
		<div class="col-12 col-md-1 d-md-flex justify-content-md-end d-grid">
		<div class="d-none d-sm-none d-md-block">
		<a onclick="removeDetail(this)" class="btn btn-danger btn-floating btnRemoveDetail" role="button"><i class="far fa-trash-alt"></i></a>
		</div>
		<button class="btn btn-danger d-block d-sm-block d-md-none text-white mt-3" type="button" onclick="removeDetail(this)">Hapus
		</button>
		</div>
		</div>
		</div> 
		`
			// '<div class="row-detail detail-data">'+
			// '<div class="col-md-4">'+
			// '<div class="form-group">'+
			// '<label class="control-label">posisi</label>'+
			// '<input type="text" class="form-control" name="posisi[]" value="">'+
			// '</div>'+
			// '</div>'+
			// '<div class="col-md-4">'+

			// '<div class="form-group">'+
			// '<label class="control-label">keahlian</label>'+
			// '<input type="text" class="form-control" name="keahlian[]" value="">'+
			// '</div>'+
			// '</div>'+
			// '<div class="col-md-3">'+
			// '<div class="form-group">'+
			// '<label class="control-label">jumlah orang bulan</label>'+
			// '<input type="text" class="form-control numberFormat" name="jumlah_orang[]" value="" autocomplete="off">'+
			// '</div>'+
			// '</div>'+
			// '<div class="col-md-1">'+
			// '<button onclick="removeDetail(this)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>'+
			// '</div>'+
			// '</div>'
			)
	$(".detail-data").last().find('.numberFormat2').each(function() {
		new AutoNumeric(this,
		{
			watchExternalChanges: true,
			allowDecimalPadding: true,
			digitGroupSeparator:','
		});
	})

	if ($('.detail-data').length >1){
		$('.btnRemoveDetail').removeClass('undisplay')
	}
}

function addPerusahaan(){
	$('.row-perusahaan').last().after(
		`
		<div class="mb-3 row-perusahaan perusahaan-data">
		<div class="row align-items-end">
		<div class="col-12 col-md-5"><label class="form-label">Nama Perusahaan</label>
		<input class="form-control form-control-lg"  name="nama[]" type="text">
		</div>
		<div class="col-12 col-md-3"><label class="form-label">Asing</label>
		<input class="form-control form-control-lg numberFormat" name="asing[]" type="text">
		</div>
		<div class="col-12 col-md-3"><label class="form-label">Indonesia</label>
		<input class="form-control form-control-lg numberFormat" type="text" name="indonesia[]">
		</div>
		<div class="col-12 col-md-1 d-md-flex justify-content-md-end d-grid">
		<div class="d-none d-sm-none d-md-block">
		<a class="btn btn-danger btn-floating btnRemovePerusahaan" role="button" onclick="removePerusahaan(this)"><i class="far fa-trash-alt"></i></a></div>
		<button class="btn btn-danger d-block d-sm-block d-md-none text-white mt-3" type="button" onclick="removePerusahaan(this)">Hapus</button>
		</div>
		</div>
		</div>
		`
			// '<div class="row perusahaan perusahaan-data">'+
			// '<div class="col-md-4">'+
			// '<div class="form-group">'+
			// '<label class="control-label">Nama</label>'+
			// '<input type="text" class="form-control" name="nama[]" value="">'+
			// '</div>'+
			// '</div>'+
			// '<div class="col-md-4">'+

			// '<div class="form-group">'+
			// '<label class="control-label">Asing</label>'+
			// '<input type="text" class="form-control numberFormat" name="asing[]" value="" autocomplete="off">'+
			// '</div>'+
			// '</div>'+
			// '<div class="col-md-3">'+
			// '<div class="form-group">'+
			// '<label class="control-label">Indonesia</label>'+
			// '<input type="text" class="form-control numberFormat" name="indonesia[]" value="" autocomplete="off">'+
			// '</div>'+
			// '</div>'+
			// '<div class="col-md-1">'+
			// '<button onclick="removePerusahaan(this)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>'+
			// '</div>'+
			// '</div>'
			)
	$(".perusahaan-data").last().find('.numberFormat2').each(function() {
		new AutoNumeric(this,
		{
			watchExternalChanges: true,
			allowDecimalPadding: true,
			digitGroupSeparator:','
		});
	})


	if ($('.perusahaan-data').length >1){
		$('.btnRemovePerusahaan').removeClass('undisplay')
	}
}


function removeDetail(ini){
	$(ini).closest('.row-detail').remove()
	sumAsing()
	sumIndonesia()
	if ($('.detail-data').length ==1){
		$('.btnRemoveDetail').addClass('undisplay')
	}
}

function removePerusahaan(ini){
	$(ini).closest('.row-perusahaan').remove()
	if ($('.perusahaan-data').length ==1){
		$('.btnRemovePerusahaan').addClass('undisplay')
	}
}

function setTopProyek(){
	var checkes = []
	var rowid = ''
	var datas = {}
	var totalSetTopProyek = 0;

	$('#datatable tbody tr').each(function(i,v){
		rowid = $(v).attr('rowid')
		if ($(v).children(':nth-child(1)').children().is(':checked') == true){
			checkes.push(rowid)
			totalSetTopProyek = totalSetTopProyek + 1
		}
	})


	if (checkes.length <1){
		Swal.fire({
			title: 'Gagal',
			text: 'Pilih Minimal 1 Data !',
			icon: 'warning',
			showCancelButton: false,
			confirmButtonText: 'OK',
			cancelButtonText: 'Ya',
			customClass: {
				confirmButton: 'btn btn-primary mr-2',
				cancelButton: 'btn btn-danger'
			},
			buttonsStyling: false
		})

	}
	else if (checkes.length >5){
		Swal.fire({
			title: 'Gagal',
			text: 'Pilih Maksimal 5 Data !',
			icon: 'warning',
			showCancelButton: false,
			confirmButtonText: 'OK',
			cancelButtonText: 'Ya',
			customClass: {
				confirmButton: 'btn btn-primary mr-2',
				cancelButton: 'btn btn-danger'
			},
			buttonsStyling: false
		})
	}
	else if (totalSetTopProyek+parseInt(topProyekCount) > 5 ){
		Swal.fire({
			title: 'Gagal',
			text: 'Pilih Maksimal 5 Data ! tersedia ' +(5-parseInt(topProyekCount)),
			icon: 'warning',
			showCancelButton: false,
			confirmButtonText: 'OK',
			cancelButtonText: 'Ya',
			customClass: {
				confirmButton: 'btn btn-primary mr-2',
				cancelButton: 'btn btn-danger'
			},
			buttonsStyling: false
		})			
	}
	else{
		spinnerStart();
		datas = {ids:checkes}

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: "{{ route('proyek.settopproyek') }}",
			method: 'post',
			data:datas,
			success: function(response){

				$('.checkRow').prop('checked',false)
				$('#selectAll').prop('checked',false)
				spinnerEnd()
				Swal.fire({
					title: 'Berhasil',
					text: 'Top Proyek telah di Update !',
					icon: 'success',
					showCancelButton: false,
					confirmButtonText: 'OK',
					cancelButtonText: 'Ya',
					customClass: {
						confirmButton: 'btn btn-primary mr-2',
						cancelButton: 'btn btn-danger'
					},
					buttonsStyling: false
				})
				.then(function() {
					location.href = "{{ route('proyek.index') }}";
				})
			},
			error: function(response){
				spinnerEnd()
				Swal.fire({
					title: 'Gagal',
					text: 'Terjadi Kesalahan !',
					icon: 'error',
					showCancelButton: false,
					confirmButtonText: 'OK',
					cancelButtonText: 'Ya',
					customClass: {
						confirmButton: 'btn btn-primary mr-2',
						cancelButton: 'btn btn-danger'
					},
					buttonsStyling: false
				})
			}
		})


	}
}

function downloadKontrak(){
	var checkes = []
	var rowid = ''
	var datas = {}

	$('#datatable tbody tr').each(function(i,v){
		rowid = $(v).attr('rowid')
		if ($(v).children(':nth-child(1)').children().is(':checked') == true){
			checkes.push(rowid)
		}
	})

	if (checkes.length <1){
		Swal.fire({
			title: 'Gagal',
			text: 'Pilih Minimal 1 Data !',
			icon: 'warning',
			showCancelButton: false,
			confirmButtonText: 'OK',
			cancelButtonText: 'Ya',
			customClass: {
				confirmButton: 'btn btn-primary mr-2',
				cancelButton: 'btn btn-danger'
			},
			buttonsStyling: false
		})
	}


	else{
		spinnerStart();
		datas = {ids:checkes}
		console.log(datas)

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: "{{ route('download.kontrak') }}",
			method: 'post',
			data:datas,
			success: function(response){
				console.log(response)
				// return false;
				const link = document.createElement('a');
				link.setAttribute('href', baseurl+'/file_kontrak/kontrak_'+response+'.pdf');
				link.setAttribute('download', 'kontrak_'+response+'.pdf'); 
				link.click();

				$('.checkRow').prop('checked',false)
				$('#selectAll').prop('checked',false)
				spinnerEnd()
			},
			error: function(response){
				spinnerEnd()
				Swal.fire({
					title: 'Gagal',
					text: 'Terjadi Kesalahan !',
					icon: 'error',
					showCancelButton: false,
					confirmButtonText: 'OK',
					cancelButtonText: 'Ya',
					customClass: {
						confirmButton: 'btn btn-primary mr-2',
						cancelButton: 'btn btn-danger'
					},
					buttonsStyling: false
				})
			}
		})


	}
}

function downloadProyek2(idTarget){

	var checkes = []
	var checkesNamaPaketPekerjaan = []
	var rowid = ''
	var datas = {}
	$('.table-pdf').html('')

	$('#datatable tbody tr').each(function(i,v){
		rowid = $(v).attr('rowid')
		if ($(v).children(':nth-child(1)').children().is(':checked') == true){
			checkes.push(rowid)
			checkesNamaPaketPekerjaan.push($(v).children(':nth-child(1)').children().attr('data-nama-paket-pekerjaan'))
		}
	})

	if (idTarget !=''){
		checkes.push(idTarget)
		checkesNamaPaketPekerjaan.push($('#projection_file').attr('data-nama-paket-pekerjaan'))
	}

	if ((checkes.length <1) && (idTarget == '')) {
		Swal.fire({
			title: 'Gagal',
			text: 'Pilih Minimal 1 Data !',
			icon: 'warning',
			showCancelButton: false,
			confirmButtonText: 'OK',
			cancelButtonText: 'Ya',
			customClass: {
				confirmButton: 'btn btn-primary mr-2',
				cancelButton: 'btn btn-danger'
			},
			buttonsStyling: false
		})
	}
	else{
		$('#modalBodyTahunTerakhir').html(
			'   <div class="form-group row">'+
			'<label for="inputPassword" class="col-sm-8 col-form-label text-center">Nama Paket Pekerjaan</label>'+
			'<label for="inputPassword" class="col-sm-4 col-form-label text-center">Tahun Terakhir</label>'+
			'</div>'
			)
		$.each(checkes,function(i,v){			
			$('#modalBodyTahunTerakhir').append(
				'<div class="form-group row">'+
				'<label for="'+v+'" class="col-sm-8 col-form-label text-end">'+checkesNamaPaketPekerjaan[i]+'</label>'+
				'<div class="col-sm-4">'+
				'<input type="number" class="form-control tahun-terakhir" id="'+v+'" >'+
				'</div>'+
				'</div></br>'
				)
		})
		$('#modalTahunTerakhir').modal('show')
	}

}

function validasiTahunTerakhir(){
	let cekValidasi = 0;
	$('.tahun-terakhir').each(function(i,v){
		if ($(v).val()==''){
			cekValidasi =1
		}
	})

	if (cekValidasi == 1){
		Swal.fire({
			title: 'Gagal',
			text: 'Harap Input Semua Tahun Terakhir !',
			icon: 'warning',
			showCancelButton: false,
			confirmButtonText: 'OK',
			cancelButtonText: 'Ya',
			customClass: {
				confirmButton: 'btn btn-primary mr-2',
				cancelButton: 'btn btn-danger'
			},
			buttonsStyling: false
		})
		return false;
	}
	else {
		if ($('#projection_file').attr('data-nama-paket-pekerjaan') != undefined){
			downloadProyek($('#projection_file').attr('data-id'));
			$('#modalTahunTerakhir').modal('hide')
		}
		else{
			downloadProyek('');
			$('#modalTahunTerakhir').modal('hide')
		}
	}

}

function downloadProyek(idTarget){

	spinnerStart();

	var checkes = []
	var checkesNamaPaketPekerjaan = []
	var rowid = ''
	var datas = {}
	$('.table-pdf').html('')

	$('#datatable tbody tr').each(function(i,v){
		rowid = $(v).attr('rowid')
		if ($(v).children(':nth-child(1)').children().is(':checked') == true){
			checkes.push(rowid)
		}
	})

	if ((checkes.length <1) && (idTarget == '')) {
		Swal.fire({
			title: 'Gagal',
			text: 'Pilih Minimal 1 Data !',
			icon: 'warning',
			showCancelButton: false,
			confirmButtonText: 'OK',
			cancelButtonText: 'Ya',
			customClass: {
				confirmButton: 'btn btn-primary mr-2',
				cancelButton: 'btn btn-danger'
			},
			buttonsStyling: false
		})

	}


	else{
		if (idTarget!=''){
			checkes.push(idTarget)
		}

		$('.tahun-terakhir').each(function(i,v){
			checkesNamaPaketPekerjaan.push($(v).val())
		})

		datas = {ids:checkes,tahunTerakhir:checkesNamaPaketPekerjaan}
		var templateRow=[]

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: "{{ route('create.download.proyek') }}",
			method: 'post',
			data:datas,
			success: function(response){

				urut = 0;
				var tahunpertama = '';
				var tahunterakhir = '';
				var tahunsekarang = moment().year();

				var alpabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
				var bulan = ['','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];


				$('#viewPdf').html('')

				var biggest = Math.max.apply( null, checkesNamaPaketPekerjaan );

				var templateRow2 = '';
				var templateRow3 = '';
				var templateRow4 = '';


				$.each(response.data_proyek,function(i,v){
					urut = parseInt(i) + 1;
					var totalTenagaAsing = 0;
					var totalTenagaIndonesia = 0;
					var totalTenagaAhliAsing = 0;
					var totalTenagaAhliIndonesia = 0;

					var ruanglingkup = v.lingkup_produk_utama;
					ruanglingkup = ruanglingkup.replace(/[\n\r]+/g, ' ');

					var tanggalMulai = moment(v.waktu_pelaksanaan_mulai).format('DD');
					var tanggalSelesai = moment(v.waktu_pelaksanaan_selesai).format('DD');
					var tahunMulai = moment(v.waktu_pelaksanaan_mulai).format('yyyy');
					var tahunSelesai = moment(v.waktu_pelaksanaan_selesai).format('yyyy');
					var bulanMulai =bulan[moment(v.waktu_pelaksanaan_mulai).format('M')];
					var bulanSelesai =bulan[moment(v.waktu_pelaksanaan_selesai).format('M')];

					templateRow2 =
					'<header class="headprint">'+
					'<div class="headprint1">'+          
					'<span><u>PT. <nbsp></nbsp> CIRIAJASA <nbsp></nbsp> ENGINEERING <nbsp></nbsp> CONSULTANS</u></span>'+
					'<br>'+
					'<span style="font-size:12px !important;">Management, Engineering, Social, Development, Environmental</span>'+
					'</div>'+
					'<br>'+
					'<div class="headprint2">'+          
					'<span style="font-size: 20px !important;" class="tahunterakhir">PENGALAMAN PERUSAHAAN '+checkesNamaPaketPekerjaan[i]+' TAHUN TERAKHIR</span>'+
					'<br>'+
					'<span style="font-size: 14px;">YANG MENGGAMBARKAN KUALIFIKASI TERBAIK</span>'+
					'</div>'+


					'</header>'+
					'<hr id="garishead"></hr>'+
					// '<hr id="garisfoot"></hr>'+
					'<div class="footers">'+
					'pengalaman '+checkesNamaPaketPekerjaan[i]+' tahun terakhir'+
					'</div>'+
					
					'<div class="kotak">'+

					'<div class="row">'+

					'<div class="col1">1.</div>'+
					'<div class="col2">Pengguna Jasa</div>'+
					'<div class="col3">:</div>'+
					'<div class="col4">'+v.pengguna_jasa+'</div>'+

					'</div>'+

					'<div class="row">'+

					'<div class="col1">2.</div>'+
					'<div class="col2">Nama Paket Pekerjaan</div>'+
					'<div class="col3">:</div>'+
					'<div class="col4">'+v.nama_paket_pekerjaan+'</div>'+

					'</div>'+

					'<div class="row">'+

					'<div class="col1 ruanglingkup">3.</div>'+
					'<div class="col2 ruanglingkup2">Lingkup Produk Utama</div>'+
					'<div class="col3 ruanglingkup3">:</div>'+
					'<div class="col4 rl" id="ruanglingkup">'+ruanglingkup+'</div>'+

					'</div>'+

					'<div class="row">'+

					'<div class="col1 ruanglingkup">4.</div>'+
					'<div class="col2 ruanglingkup2">Lokasi Proyek</div>'+
					'<div class="col3 ruanglingkup3">:</div>'+
					'<div class="col4" id="ruanglingkup">'+v.lokasi_proyek+'</div>'+

					'</div>'+

					'<div class="row">'+

					'<div class="col1 ruanglingkup">5.</div>'+
					'<div class="col2 ruanglingkup2">Nilai Kontrak</div>'+
					'<div class="col3 ruanglingkup3">:</div>'+
					'<div class="col4" id="ruanglingkup">'+v.nilai_kontrak+'</div>'+

					'</div>'+

					'<div class="row">'+

					'<div class="col1 ruanglingkup">6.</div>'+
					'<div class="col2 ruanglingkup2">No. Kontrak</div>'+
					'<div class="col3 ruanglingkup3">:</div>'+
					'<div class="col4" id="ruanglingkup">'+v.nomor_kontrak+'</div>'+

					'</div>'+

					'<div class="row">'+

					'<div class="col1 ruanglingkup">7.</div>'+
					'<div class="col2 ruanglingkup2">Waktu Pelaksanaan</div>'+
					'<div class="col3 ruanglingkup3">:</div>'+
					'<div class="col4" id="ruanglingkup">'+tanggalMulai+' '+bulanMulai+' '+ tahunMulai +' s/d '+tanggalSelesai+' '+bulanSelesai+' '+ tahunSelesai+'</div>'+

					'</div>'+


					'<table class="table-print">'+
					'<tbody>'+

					

					'<tr class="tr1">'+
					'<td style="text-align:right;">8.</td>'+
					'<td colspan="2">Nama Pemimpin Kemitraan</td>'+
					'<td>:</td>'+
					'<td colspan="3">'+v.nama_pimpinan_kontrak+'</td>'+
					'</tr>'+

					'<tr>'+
					'<td></td>'+
					'<td colspan="2">(jika ada)</td>'+
					'<td></td>'+
					'<td colspan="3"></td>'+
					'</tr>'+

					'<tr>'+
					'<td></td>'+
					'<td colspan="2">Alamat</td>'+
					'<td>:</td>'+
					'<td colspan="3">'+v.alamat+'</td>'+
					'</tr>'+

					'<tr>'+
					'<td></td>'+
					'<td colspan="2">Negara Asal</td>'+
					'<td>:</td>'+
					'<td colspan="3">'+v.negara_asal+'</td>'+
					'</tr>';


					templateRow4 =
					'</tbody>'+
					'</tbody>'+
					'</div>';

					// $('#viewPdf').append(templateRow)
					templateRowPerusahaan = '';
					templateRowDetail ='';

					$.each(v.perusahaan_child,function(iPerusahaan,vPerusahaan){
						templateRowPerusahaan = templateRowPerusahaan +
						'<tr class="row_perusahaan detail-tr">'+
						'<td style="text-align:right;">'+alpabet[iPerusahaan]+'</td>'+
						'<td colspan="2">'+vPerusahaan.nama+'</td>'+
						'<td colspan="2" style="text-align: center;">'+vPerusahaan.asing+' Orang Bulan</td>'+
						'<td colspan="2" style="text-align: center;">'+vPerusahaan.indonesia+' Orang Bulan</td>'+
						'</tr>';
						totalTenagaAsing = totalTenagaAsing + parseInt(vPerusahaan.asing)
						totalTenagaIndonesia = totalTenagaIndonesia + parseInt(vPerusahaan.indonesia)
					})

					$.each(v.detail_child,function(iDetail,vDetail){
						templateRowDetail = templateRowDetail +
						'<tr class="row_detail">'+
						'<td width="80" style="text-align:right;">'+alpabet[iDetail]+'</td>'+
						'<td width="250" colspan="2">'+vDetail.posisi+'</td>'+
						'<td colspan="2">'+vDetail.keahlian+'</td>'+
						'<td colspan="2" style="text-align:center">'+vDetail.jumlah_orang+'</td>'+
						'</tr>';
						totalTenagaAhliIndonesia = totalTenagaAhliIndonesia + parseInt(vDetail.jumlah_orang)

					})

					templateRow3 = 
					'<tr class="tr1 tr9">'+
					'<td rowspan="2" style="text-align:right;">9.</td>'+
					'<td rowspan="2" colspan="2">Jumlah Tenaga Ahli</td>'+
					'<td></td>'+
					'<td >Tenaga Ahli Asing</td>'+
					'<td>:</td>'+
					'<td style="text-align:center;"> 0 Orang Bulan</td>'+
					'</tr>'+

					'<tr>'+
					'<td></td>'+
					'<td>Tenaga Ahli Indonesia</td>'+
					'<td >:</td>'+
					'<td id="totalTenagaAhliIndonesia0" style="text-align:center;">'+totalTenagaAhliIndonesia+' Orang Bulan</td>'+
					'</tr>'+

					'<tr class="tr1 tr10">'+
					'<td style="text-align:right;">10.</td>'+
					'<td colspan="2">Perusahaan Mitra Kerja</td>'+
					'<td colspan="4"  style="text-align: center;">Jumlah Tenaga Ahli</td>'+
					'</tr>'+

					'<tr class="row_perusahaan">'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
					'<td colspan="2" style="text-align: center;" >Asing</td>'+
					'<td colspan="2" style="text-align: center;">Indonesia</td>'+
					'</tr>';

					templateRow5 = 
					'<tr class="tenagaahli1 tr11">'+
					'<td style="text-align:right;">11.</td>'+
					'<td colspan="6">Tenaga Ahli yang Terlibat</td>'+
					'</tr>'+
					'<tr class="tenagaahli2">'+
					'<td></td>'+
					'<td colspan="2" style="text-align:center"> Posisi</td>'+
					'<td colspan="2" style="text-align:center"> Keahlian</td>'+
					'<td colspan="2" style="text-align:center"> Jumlah Orang</td>'+
					'</tr>';					

					templateRow2 = templateRow2 + templateRow3 + templateRowPerusahaan +templateRow5 + templateRowDetail + templateRow4;
					templateRow.push(templateRow2)		

				})



$('.checkRow').prop('checked',false)
$('#selectAll').prop('checked',false)
spinnerEnd()


},
error: function(response){
	spinnerEnd()
	Swal.fire({
		title: 'Gagal',
		text: 'Terjadi Kesalahan !',
		icon: 'error',
		showCancelButton: false,
		confirmButtonText: 'OK',
		cancelButtonText: 'Ya',
		customClass: {
			confirmButton: 'btn btn-primary mr-2',
			cancelButton: 'btn btn-danger'
		},
		buttonsStyling: false
	})
}
})
.done(function(data){
	html = $('#viewPdf').html()
	datas = {html:templateRow}
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url: "{{ route('download.proyek') }}",
		method: 'post',
		data:datas,
		success: function(response){
			const linkx = document.createElement('a');
			linkx.setAttribute('href', "{{ route('download.proyek.file') }}");
			linkx.setAttribute('target', '_blank');
			linkx.click();
		}
	})

})
}
}

function deleteProyek(){
	var checkes = []
	var rowid = ''
	var datas = {}

	$('#datatable tbody tr').each(function(i,v){
		rowid = $(v).attr('rowid')
		if ($(v).children(':nth-child(1)').children().is(':checked') == true){
			checkes.push(rowid)
		}
	})

	if (checkes.length <1){
		Swal.fire({
			title: 'Gagal',
			text: 'Pilih Minimal 1 Data !',
			icon: 'warning',
			showCancelButton: false,
			confirmButtonText: 'OK',
			cancelButtonText: 'Ya',
			customClass: {
				confirmButton: 'btn btn-primary mr-2',
				cancelButton: 'btn btn-danger'
			},
			buttonsStyling: false
		})
	}
	else{
		datas = {ids:checkes}

		Swal.fire({
			title: 'Hapus '+checkes.length+' Data',
			text: 'data tidak dapat dikembalikan !',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Tidak',
			cancelButtonText: 'Ya',
			customClass: {
				confirmButton: 'btn btn-primary mr-2',
				cancelButton: 'btn btn-danger'
			},
			buttonsStyling: false
		}).then((result) => {
			if (!result.isConfirmed) {
				spinnerStart();

				$.ajax({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					url: "{{ route('proyek.destroy','delete') }}",
					method: 'delete',
					data:datas,
					success: function(response){

						$('.checkRow').prop('checked',false)
						$('#selectAll').prop('checked',false)
						dtMasterProyek(url,dataColum,selectYearStart,selectYear,'');

						spinnerEnd()
						Swal.fire({
							title: 'Berhasil',
							text: 'Hapus Data !',
							icon: 'success',
							showCancelButton: false,
							confirmButtonText: 'OK',
							cancelButtonText: 'Ya',
							customClass: {
								confirmButton: 'btn btn-primary mr-2',
								cancelButton: 'btn btn-danger'
							},
							buttonsStyling: false
						})
					},
					error: function(response){
						spinnerEnd()
						Swal.fire({
							title: 'Gagal',
							text: 'Terjadi Kesalahan !',
							icon: 'error',
							showCancelButton: false,
							confirmButtonText: 'OK',
							cancelButtonText: 'Ya',
							customClass: {
								confirmButton: 'btn btn-primary mr-2',
								cancelButton: 'btn btn-danger'
							},
							buttonsStyling: false
						})
					}
				})


			}
		});


	}
}

function editProyek(){
	var checkes = []
	var rowid = ''
	var datas = {}

	$('#datatable tbody tr').each(function(i,v){
		rowid = $(v).attr('rowid')
		if ($(v).children(':nth-child(1)').children().is(':checked') == true){
			checkes.push(rowid)
		}
	});

	console.log('TEMPE', checkes, rowid);

	if (checkes.length !=1){
		Swal.fire({
			title: 'Gagal',
			text: 'Pilih 1 Data untuk diubah !',
			icon: 'warning',
			showCancelButton: false,
			confirmButtonText: 'OK',
			cancelButtonText: 'Ya',
			customClass: {
				confirmButton: 'btn btn-primary mr-2',
				cancelButton: 'btn btn-danger'
			},
			buttonsStyling: false
		})
	}


	else{

		const link = document.createElement('a');
		link.setAttribute('href', baseurl+'/proyek/'+checkes[0]);
		link.click();


	}
}

$('.col-12').on('keyup','[name="asing[]"]',function(){
	sumAsing()
})

$('.col-12').on('keyup','[name="indonesia[]"]',function(){
	sumIndonesia()
})

$('.col-12').on('keyup','[name="jumlah_orang[]"]',function(){
	sumAhli()
})

function sumAsing(){
	let sum = 0;
	let value=0;
	$('[name="asing[]"]').each(function(i,v){
		if ($(v).val() == ''){
			value = 0	
		}
		else{			
			value = parseFloat($(v).val())
		}
		// $(v).val(value)
		sum = parseFloat(sum) + parseFloat(value)
	})
	console.log(sum)
	$('#jumlahTenagaAsing').val(sum)
}
function sumIndonesia(){
	let sum = 0;
	let value=0;
	$('[name="indonesia[]"]').each(function(i,v){
		if ($(v).val() == ''){
			value = 0	
		}
		else{			
			value = parseFloat($(v).val())
		}
		// $(v).val(value)
		sum = sum + parseFloat(value)
	})
	$('#jumlahTenagaIndonesia').val(sum)
}

function sumAhli(){
	let sum = 0;
	let value=0;
	$('[name="jumlah_orang[]"]').each(function(i,v){
		if ($(v).val() == ''){
			value = 0	
			
		}
		else{			
			value = parseFloat($(v).val())
		}
		// $(v).val(value)
		sum = sum + parseFloat(value)
	})
	$('#jumlahTenagaAhli').val(sum)
}

function kembali(){
	Swal.fire({
		title: 'Kembali ',
		text: 'Data akan hilang !',
		icon: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Tidak',
		cancelButtonText: 'Ya',
		customClass: {
			confirmButton: 'btn btn-primary mr-2',
			cancelButton: 'btn btn-danger'
		},
		buttonsStyling: false

	}).then((result) => {
		if (!result.isConfirmed) {

			location.href = "{{ route('home') }}";
		}
	})
}


</script>