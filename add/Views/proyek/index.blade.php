@extends("template.app_fe")
@section('content')

@section('customStyles')
<style>
	.dropdown-toggle-no-caret:after{
		content:none !important;
	}

	#tableViewProyek>tbody>tr>td:nth-child(1){
		width: 30%;
	}
	#tableViewProyek>tbody>tr{
		line-height: 3;
	}
	#viewProyek{
		margin-bottom: 15px;
	}

</style>
@endsection

<main class="page landing-page" style="min-height: calc(100% - 52px);">
	<section class="clean-block about-us">
		<!-- Start: search -->
        @include("template.app_search")
        <!-- End: search -->
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="d-flex justify-content-end">
						<!-- Start: Button Icon Round -->
						<div class="dropdown">
							<a class="btn btn-warning btn-floating dropdown-toggle" role="button" data-bs-toggle="dropdown"><i class="far fa-bookmark"></i></a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="#" onclick="setTopProyek()">Top Proyek</a>
								<a class="dropdown-item" href="#" id="ddListTopProyek" onclick="listTopProyek()">List Top Proyek</a></div>
						</div><!-- End: Button Icon Round -->
						<!-- Start: Button Icon Round -->
						<div><a class="btn btn-info btn-floating" role="button" id="btnEditProyek" onclick="editProyek()"><i class="fas fa-pencil-alt"></i></a></div><!-- End: Button Icon Round -->
						<!-- Start: Button Icon Round -->
						<div><a class="btn btn-danger btn-sm btn-floating" role="button" id="btnDeleteProyek" onclick="deleteProyek()"><i class="far fa-trash-alt"></i></a></div><!-- End: Button Icon Round -->
						<div><a class="btn btn-danger btn-sm btn-floating undisplay" role="button" id="btnRemoveTopProyek" onclick="removeTopProyek()"><i class="fa fa-times"></i></a></div><!-- End: Button Icon Round -->
					</div>
					<div class="table-responsive table mb-0 pt-3 pe-2">
						<table class="table table-striped table-sm my-0 table-proyek" id="datatable">
							<thead>
								<tr>
									<th id="checkAll"><input id="selectAll" type="checkbox"></th>
									<th>No.</th>
									<th>Nama Paket Pekerjaan</th>
									<th>Lokasi Proyek</th>
									<th>Waktu Pelaksanaan</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</main><!-- Start: Footer Dark -->

@include('template.footer')
@endsection
@section('js')
@include("proyek.xjs")
@include("proyek.zjs")
@endsection