@extends("template.app_fe")
@section('content')

@section('customStyles')
<style>
	.detail-project p{
		font-family: var(--bs-body-font-family) !important;
		margin-top: 0px;
		margin-bottom: 5px;
	}

	.detail-project div.lingkup-produk-utama-content > * > *{
		font-family: var(--bs-body-font-family) !important;
		font-size: var(--bs-body-font-size) !important;
	}

	.detail-project td {
		font-family: var(--bs-body-font-family) !important;
		font-size: var(--bs-body-font-size) !important;
	}

	span{
		font-family: var(--bs-body-font-family) !important;
		font-size: var(--bs-body-font-size) !important;
	}

</style>
@endsection

<main class="page landing-page">
	<section class="clean-block about-us">
		<!-- Start: search -->
		<div class="block-heading search-header with-bg mobile__plr-15 mobile__mb-0" style="padding-right: 20%;padding-left: 20%;">
			{{-- <form class="d-flex flex-column flex-md-row" method="get" action="#">
				<div class="input-group input-search-container mobile__mb-10" style="flex: 2;"><input class="form-control form-control-lg" type="text" placeholder="Cari Proyek" name="keyword"><span class="input-group-text"><i class="icon ion-android-search"></i></span></div><select class="form-select form-select-lg form-select select-search mobile__mlr-0" style="flex: 1;margin-left: 10px;" name="year">
					<option value="2019">2019</option>
					<option value="2020">2020</option>
					<option value="2021">2021</option>
					<option value="2022">2022</option>
				</select>
			</form> --}}
		</div>
		<!-- End: search -->
		<!-- Start: container-zebra -->
		
		<div class="undisplay">
			<div id="viewPdf" class="">
				<div id="view">
					<div class="row">
						<div class="col-md-12" style="text-align:end;">				
							<span style="font-size: 15px;"><u>PT. CIRIAJASA ENGINEERING CONSULTANS</u></span>
							<br>
							<span style="font-size: 10px;">Management, Engineering, Social, Development, Environmental</span>			
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-12 text-center">
							<span style="font-size: 20px;" class="tahunterakhir">PENGALAMAN PERUSAHAAN 7 TAHUN TERAKHIR</span>
							<br>
							<span style="font-size: 14px;">YANG MENGGAMBARKAN KUALIFIKASI TERBAIK</span>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-12 container-pdf">
							<div class="table-pdf"></div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="downloadPDF"></div>


		<div class="container-fluid container-zebra detail-project">
			<!-- Start: row-zebra -->
			<div class="row-zebra">
				<div class="row">
					<div class="col-md-3"><img class="img-fluid" src="{{ asset('cover_proyek') }}/{{ $datas[0]->id }}.jpg"></div>
					<div class="col-md-9">
						{{-- <p style="font-weight: bold;">{{ $datas[0]->nama_paket_pekerjaan }}</p> --}}
						<h5 class="mb-4" style="font-weight: bold;">{{ $datas[0]->nama_paket_pekerjaan }}</h5>

						@if($user_id !='')
						<a class="link-normal" href="#" id="projection_file" onclick="downloadProyek2('{{ $datas[0]->id }}')" data-nama-paket-pekerjaan="{{ $datas[0]->nama_paket_pekerjaan }}" data-id="{{ $datas[0]->id }}">
							<p style="font-style: italic;">
								<i class="far fa-file-pdf fa-fw" style="margin-right: 10px;"></i>
								{{ $datas[0]->nama_paket_pekerjaan }}.pdf</p>
							</a>
							<a class="link-normal" href="{{ url('file_kontrak/'.$datas[0]->id) }}.pdf" id="kontrak_file" download="file_kontrak.pdf">
								<p style="font-style: italic;"><i class="far fa-file-pdf fa-fw" style="margin-right: 10px;"></i>Kontrak.pdf</p>
							</a>
							@endif
						</div>
					</div>
				</div><!-- End: row-zebra -->
				<br>
				<!-- Start: row-zebra -->
				<div class="row-zebra">
					<div class="row ">
						<div class="col-md-3">
							<p style="font-weight: bold;">1. Pengguna Jasa</p>
						</div>
						<div class="col-md-9">
							<p>{{ $datas[0]->pengguna_jasa }}</p>
						</div>
					</div>
				</div><!-- End: row-zebra -->
				<!-- Start: row-zebra -->
				<div class="row-zebra">
					<div class="row  ">
						<div class="col-md-3">
							<p style="font-weight: bold;">2. Nama Paket Pekerjaan</p>
						</div>
						<div class="col-md-9">
							<p>{{ $datas[0]->nama_paket_pekerjaan }}</p>
						</div>
					</div>
				</div><!-- End: row-zebra -->
				<!-- Start: row-zebra -->
				<div class="row-zebra">
					<div class="row  ">
						<div class="col-md-3">
							<p style="font-weight: bold;">3. Lingkup Produk Utama</p>
						</div>
						<div class="col-md-9">
							<div class='lingkup-produk-utama-content'>{!! $datas[0]->lingkup_produk_utama !!}&nbsp;</div>
						</div>
					</div>
				</div><!-- End: row-zebra -->
				<!-- Start: row-zebra -->
				{{-- @if ($user_id=='')
				<div class="row-zebra">
					<div class="row  ">
						<div class="col-md-3">
							<p style="font-weight: bold;">4. Nilai Kontrak</p>
						</div>
						<div class="col-md-9">
							<p>{{ $datas[0]->nilai_kontrak }}</p>
						</div>
					</div>
				</div>
				@endif --}}


				<div class="row-zebra">
					<div class="row  ">
						<div class="col-md-3">
							<p style="font-weight: bold;">4. Lokasi Proyek</p>
						</div>
						<div class="col-md-9">
							<p>{{ $datas[0]->lokasi_proyek }}</p>
						</div>
					</div>
				</div><!-- End: row-zebra -->

				@if ($user_id=='')
				<div class="row-zebra">
					<div class="row  ">
						<div class="col-md-3">
							<p style="font-weight: bold;">5. Waktu Pelaksanaan</p>
						</div>
						<div class="col-md-9">
							<?php 
							$bulan = array('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
							$tanggalMulai =  date("d", strtotime($datas[0]->waktu_pelaksanaan_mulai));
							$tanggalSelesai=  date("d", strtotime($datas[0]->waktu_pelaksanaan_selesai));
							$tahunMulai=  date("Y", strtotime($datas[0]->waktu_pelaksanaan_mulai));
							$tahunSelesai= date("Y", strtotime($datas[0]->waktu_pelaksanaan_selesai));
							$bulanMulai=date("n", strtotime($datas[0]->waktu_pelaksanaan_mulai));
							$bulanSelesai=date("n", strtotime($datas[0]->waktu_pelaksanaan_selesai));;

							?>

							@if($tahunMulai == $tahunSelesai)
							<p> {{ $tahunMulai }}</p>
							@else
							<p> {{ $tahunMulai }} s/d {{ $tahunSelesai }}</p>
							@endif


						</div>
					</div>
				</div><!-- End: row-zebra -->

				@endif

				@if ($user_id!='')
				<!-- Start: row-zebra -->
				<div class="row-zebra">
					<div class="row  ">
						<div class="col-md-3">
							<p style="font-weight: bold;">5. Nilai Kontrak</p>
						</div>
						<div class="col-md-9">
							{{-- <p>Rp. 4.187.843.000,-<br>CEC 60% : Rp. 2,512,705,800<br>CSM 40% : Rp. 1,675,137,200</p> --}}
							<p>{{ $datas[0]->nilai_kontrak }}</p>
						</div>
					</div>
				</div><!-- End: row-zebra -->
				<!-- Start: row-zebra -->
				<div class="row-zebra">
					<div class="row  ">
						<div class="col-md-3">
							<p style="font-weight: bold;">6. Nomor Kontak</p>
						</div>
						<div class="col-md-9">
							<p>{{ $datas[0]->nomor_kontrak }}</p>
						</div>
					</div>
				</div><!-- End: row-zebra -->
				<!-- Start: row-zebra -->
				<div class="row-zebra">
					<div class="row  ">
						<div class="col-md-3">
							<p style="font-weight: bold;">7. Waktu Pelaksanaan</p>
						</div>
						<div class="col-md-9">
							<?php 
							$bulan = array('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
							$tanggalMulai =  date("d", strtotime($datas[0]->waktu_pelaksanaan_mulai));
							$tanggalSelesai=  date("d", strtotime($datas[0]->waktu_pelaksanaan_selesai));
							$tahunMulai=  date("Y", strtotime($datas[0]->waktu_pelaksanaan_mulai));
							$tahunSelesai= date("Y", strtotime($datas[0]->waktu_pelaksanaan_selesai));
							$bulanMulai=date("n", strtotime($datas[0]->waktu_pelaksanaan_mulai));
							$bulanSelesai=date("n", strtotime($datas[0]->waktu_pelaksanaan_selesai));;

							?>
							@if($tahunMulai == $tahunSelesai)
							<p> {{$tanggalMulai}} {{ $bulan[$bulanMulai] }} {{ $tahunMulai }}</p>
							@else
							<p>  {{$tanggalMulai}} {{ $bulan[$bulanMulai] }} {{ $tahunMulai }} s/d {{$tanggalSelesai}} {{ $bulan[$bulanSelesai] }} {{ $tahunSelesai }}</p>
							@endif

						</div>
					</div>
				</div><!-- End: row-zebra -->
				<!-- Start: row-zebra -->
				<div class="row-zebra">
					<div class="row  ">
						<div class="col-md-3">
							<p style="font-weight: bold;">8. Nama Pemimpin Kemitraan<br></p>
						</div>
						<div class="col-md-9">
							<p>{{ $datas[0]->nama_pimpinan_kontrak }}</p>
						</div>
					</div>
				</div><!-- End: row-zebra -->
				<!-- Start: row-zebra -->
				<div class="row-zebra">
					<div class="row  ">
						<div class="col-md-3" >
							<p style="font-weight: bold;padding-left: 20px;">Alamat Pemimpin Kemitraan</p>
						</div>
						<div class="col-md-9">
							<p>{{ $datas[0]->alamat }}</p>
						</div>
					</div>
				</div><!-- End: row-zebra -->
				<!-- Start: row-zebra -->
				<div class="row-zebra">
					<div class="row  ">
						<div class="col-md-3">
							<p style="font-weight: bold;padding-left: 20px;">Negara Asal Pemimpin Kemitraan</p>
						</div>
						<div class="col-md-9">
							<p>{{ $datas[0]->negara_asal }}</p>
						</div>
					</div>
				</div><!-- End: row-zebra -->
				<!-- Start: row-zebra -->
				<div class="row-zebra">
					<div class="row  ">
						<div class="col-md-3">
							<p style="font-weight: bold;">9. Jumlah Tenaga Ahli<br></p>
						</div>
						<div class="col-md-9">
							<p>Tenaga Ahli Asing :&nbsp;0&nbsp;Orang Bulan<br></p>
							<p>Tenaga Ahli Indonesia:&nbsp;{{ $datas[0]->totalJumlahOrang }}&nbsp; Orang Bulan&nbsp;</p>
						</div>
					</div>
				</div><!-- End: row-zebra -->
				<!-- Start: row-zebra -->
				<div class="row-zebra">
					<div class="row  ">
						<div class="col-md-3">
							<p style="font-weight: bold;">10. Jumlah Mitra Kerja<br></p>
						</div>
						<div class="col-md-9">
							<p>Tenaga Ahli asing :&nbsp;{{ $datas[0]->totalTenagaAsing }}&nbsp; Orang Bulan&nbsp;</p>
							<p>Tenaga Ahli Indonesia :&nbsp;{{ $datas[0]->totalTenagaIndonesia }}&nbsp;Orang Bulan<br></p>
						</div>
					</div>
				</div><!-- End: row-zebra -->
				<!-- Start: row-zebra -->
				<div class="row-zebra tenaga-ahli">
					<div class="row  ">
						<div class="col-md-12">
							<p style="font-weight: bold;">11. Tenaga Ahli Yang Terlibat<br></p>
						</div>
						<div class="col-md-12">
							<!-- Start: List Proyek -->
							<section class="mt-4">
								<div class="row">
									<div class="col">
										<div class="table-responsive table mb-0  pe-2">
											<table class="table table-striped table-sm my-0 mydatatable">
												<thead>
													<tr>
														<th>Posisi</th>
														<th>Keahlian</th>
														<th class="text-center">Jumlah Orang Bulan</th>
													</tr>
												</thead>
												<tbody></tbody>
												@endif
											</table>
										</div>
									</div>
								</div>
							</section><!-- End: List Proyek -->
						</div>
					</div>
				</div><!-- End: row-zebra -->
			</div><!-- End: container-zebra -->
		</section>
	</main><!-- Start: Footer Dark -->
	<footer class="page-footer" style="padding-top: 0px;position: relative;bottom: 0px;">
		<div class="footer-copyright" style="margin-top: 0px;background-color: rgba(53, 62, 147, 0.09);">
			<p style="color: #353E93;font-weight: bold;">© Ciriajasa Engineering &amp; Management Consultant. Hak Cipta Dilindungi Undang-Undang</p>
		</div>
	</footer><!-- End: Footer Dark -->


	@include('home.modal_tahun_terakhir')

	@endsection

	@section('js')
	@include('proyek.zjs')
	<script>

		response = <?php echo($datas); ?>;
		response = JSON.stringify(response);
		response = JSON.parse(response);
		// var response = $.parseJSON('<?php echo($datas); ?>');
		detailChild = ''
		$.each(response[0].detail_child,function(i,v){
			detailChild = detailChild+
			'<tr>'+
			'<td width="400">'+v.posisi+'</td>'+
			'<td width="400">'+v.keahlian+'</td>'+
			'<td class="text-center">'+v.jumlah_orang+'</td>'+
			'</tr>'
		})

		$('.mydatatable tbody').append(detailChild)
		
		if (response[0].detail_child.length == 0){
			$('.tenaga-ahli').remove()
		}

		$('#closeModalTahunTerakhir').click(function(){
			$('#modalTahunTerakhir').modal('hide')
		})
		$('p').removeClass('17')

	</script>
	@endsection