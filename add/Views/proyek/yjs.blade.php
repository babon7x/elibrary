<script src="{{asset('assets/dt_master.js?version=1.1')}}"></script>
<script src="{{asset('assets/data.js?version=1.1')}}"></script>
<script src="{{asset('jquery/jquery.form.min.js')}}"></script>

<script>
	$(".select2").select2({dropdownAutoWidth : true});
	$(".numberFormat2").each(function() {
		new AutoNumeric(this,
		{
			watchExternalChanges: true,
			allowDecimalPadding: true,
			digitGroupSeparator: ',',
		});
	});
	$(".datepicker").datepicker({
		format: "dd M yyyy",
		autoclose:"true"
	}).on('changeDate',function(){
		waktuMulai = moment($('#waktuPelaksanaanStart').val()).format("yyyyMMD")
		waktuSelesai = moment($('#waktuPelaksanaanEnd').val()).format("yyyyMMD")
		if ($('#waktuPelaksanaanEnd').val() == ''){
			$('#waktuPelaksanaanEnd').datepicker("setDate", $('#waktuPelaksanaanStart').val() );
			waktuSelesai = moment($('#waktuPelaksanaanEnd').val()).format("yyyyMMD")
		}
		// if (parseInt(waktuSelesai)<parseInt(waktuMulai)){
			
		// 	Swal.fire({
		// 		title: 'Format Salah',
		// 		text: 'Waktu Pelaksaan Selesai harus sama atau lebih dari Waktu Pelaksanaan Mulai  !',
		// 		icon: 'warning',
		// 		showCancelButton: false,
		// 		confirmButtonText: 'OK',
		// 		cancelButtonText: 'Ya',
		// 		customClass: {
		// 			confirmButton: 'btn btn-primary mr-2',
		// 			cancelButton: 'btn btn-danger'
		// 		},
		// 		buttonsStyling: false
		// 	})
		// 	.then(function() {
		// 		$('#waktuPelaksanaanEnd').datepicker("setDate", $('#waktuPelaksanaanStart').val() );
		// 		waktuSelesai = moment($('#waktuPelaksanaanEnd').val()).format("yyyyMMD")
		// 	})
		// }

	})

	var selectYearStart = $('#selectYearStart').val()
	var selectYear = $('#selectYear').val()
	var url = "proyek";
	var dataColum = [
	{id:null,
		"render": function ( data, type, full, meta ) {
			let baris =meta.row+1;
			let defaultContent = '<input type="checkbox" class="checkRow dt-id" is_top_proyek="'+full.top_proyek+'"></input>';
			return  defaultContent;
		}	
	},
	{id:null,
		"render": function ( data, type, full, meta ) {
			let baris =meta.row+1;
			let defaultContent = baris;
			return  defaultContent;
		}
	}];
	dataColum.push({data: 'nama_paket_pekerjaan', name: 'nama_paket_pekerjaan'})
	dataColum.push({data: 'lokasi_proyek', name: 'nama_paket_pekerjaan'})
	dataColum.push({id:null,'render': function ( data, type, full, meta ) {
		let tahunAwal = moment(full.waktu_pelaksanaan_mulai).format('yyyy');
		let tahunAkhir = moment(full.waktu_pelaksanaan_selesai).format('yyyy');
		if (tahunAwal == tahunAkhir){
			return  tahunAwal;
		}
		else{
			return tahunAwal+' - '+tahunAkhir
		}
	} });

	$('#header_upload').addClass('btn btn-primary text-white')



</script>