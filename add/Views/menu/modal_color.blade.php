<div id="modal_color" class="modal animated fadeInDown" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content " style="min-height: 500px">
			<div class="modal-header">

				<h4 >pilih warna</h4>
				<button class="btn btn-sm btn-danger " data-dismiss="modal" id="closeModal3">
					
					<i class="fa fa-times text-white"></i>

				</button>
			</div>
			<div class="modal-body">
				<div class="row justify-content-center">
					<div class=" col ">
						<div class="card">

							<div class="card-body">
								<div class="row icon-examples">
									
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" onclick="addColor(this)"  
										style="background: var(--primary);color:white;">
										<div>
											<span>primary</span>
										</div>
									</button>
								</div>

								<div class="col-lg-3 col-md-6">
									<button type="button" class="btn-icon-clipboard" onclick="addColor(this)"
									style="background: var(--info);color: white;">
									<div>
										<span>info</span>
									</div>
								</button>
							</div>


							<div class="col-lg-3 col-md-6">
								<button type="button" class="btn-icon-clipboard" onclick="addColor(this)"
								style="background: var(--success);color: white;">
								<div>
									<span>success</span>
								</div>
							</button>
						</div>

						<div class="col-lg-3 col-md-6">
							<button type="button" class="btn-icon-clipboard" onclick="addColor(this)" 
							style="background: var(--warning);color: white;">
							<div>
								<span>warning</span>
							</div>
						</button>
					</div>

					<div class="col-lg-3 col-md-6">
						<button type="button" class="btn-icon-clipboard" onclick="addColor(this)"
						style="background: var(--danger);color: white;">
						<div>
							<span>danger</span>
						</div>
					</button>
				</div>

				<div class="col-lg-3 col-md-6">
					<button type="button" class="btn-icon-clipboard" onclick="addColor(this)"
					style="background: var(--neutral);color: black;">
					<div>
						<span>neutral</span>
					</div>
				</button>
			</div>
			<div class="col-lg-3 col-md-6">
				<button type="button" class="btn-icon-clipboard" onclick="addColor(this)" 
				style="background: var(--secondary);color:black;">
				<div>
					<span>secondary</span>
				</div>
			</button>
		</div>

		<div class="col-lg-3 col-md-6">
			<button type="button" class="btn-icon-clipboard" onclick="addColor(this)"
			style="background: var(--default);color: white;">
			<div>
				<span>default</span>
			</div>
		</button>
	</div>
	<div class="col-lg-3 col-md-6">
		<button type="button" class="btn-icon-clipboard" onclick="addColor(this)"
		style="background: var(--pink);color: white;">
		<div>
			<span>pink</span>
		</div>
	</button>
</div>

<div class="col-lg-3 col-md-6">
	<button type="button" class="btn-icon-clipboard" onclick="addColor(this)"
	style="background: var(--indigo);color: white;">
	<div>
		<span>indigo</span>
	</div>
</button>
</div>

<div class="col-lg-3 col-md-6">
	<button type="button" class="btn-icon-clipboard" onclick="addColor(this)"
	style="background: var(--purple);color: white;">
	<div>
		<span>purple</span>
	</div>
</button>
</div>

<div class="col-lg-3 col-md-6">
	<button type="button" class="btn-icon-clipboard" onclick="addColor(this)"
	style="background: var(--teal);color: white;">
	<div>
		<span>teal</span>
	</div>
</button>
</div>

<div class="col-lg-3 col-md-6">
	<button type="button" class="btn-icon-clipboard" onclick="addColor(this)"
	style="background: var(--green);color: white;">
	<div>
		<span>green</span>
	</div>
</button>
</div>

<div class="col-lg-3 col-md-6">
	<button type="button" class="btn-icon-clipboard" onclick="addColor(this)"
	style="background: var(--gray);color: white;">
	<div>
		<span>gray</span>
	</div>
</button>
</div>

<div class="col-lg-3 col-md-6">
	<button type="button" class="btn-icon-clipboard" onclick="addColor(this)"
	style="background: var(--gray-dark);color: white;">
	<div>
		<span>gray-dark</span>
	</div>
</button>
</div>

<div class="col-lg-3 col-md-6">
	<button type="button" class="btn-icon-clipboard" onclick="addColor(this)"
	style="background: var(--light);color: white;">
	<div>
		<span>light</span>
	</div>
</button>
</div>

<div class="col-lg-3 col-md-6">
	<button type="button" class="btn-icon-clipboard" onclick="addColor(this)"
	style="background: var(--lighter);color: black;">
	<div>
		<span>lighter</span>
	</div>
</button>
</div>


<div class="col-lg-3 col-md-6">
	<button type="button" class="btn-icon-clipboard" onclick="addColor(this)"
	style="background: var(--dark);color: white;">
	<div>
		<span>dark</span>
	</div>
</button>
</div>

<div class="col-lg-3 col-md-6">
	<button type="button" class="btn-icon-clipboard" onclick="addColor(this)"
	style="background: var(--yellow);color: black;">
	<div>
		<span>yellow</span>
	</div>
</button>
</div>


<div class="col-lg-3 col-md-6">
	<button type="button" class="btn-icon-clipboard" onclick="addColor(this)"
	style="background: var(--darker);color: white;">
	<div>
		<span>darker</span>
	</div>
</button>
</div>


</div>
</div>

</div>
</div>

</div>
</div>

</div>
</div>
</div>