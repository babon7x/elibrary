<link rel="stylesheet" href="{{ asset('jquery/nestable.css') }}">
<script src="{{ asset('jquery/jquery.nestable.js') }}"></script>
<script src="{{ asset('jquery/json2.js') }}"></script>
<style>
  .drag_disabled{
    pointer-events: none;
  }

  .drag_enabled{
    pointer-events: all;
  }
</style>


<script>
  var sidebar   = <?php echo json_encode($sidebar); ?>;
  var fixsidebar  = "";
  var generator = {};

  $('#nestable-menu').change()
  
  var tarr =  JSON.parse(sidebar);
  var list = listify(tarr);
  $("#nestable-menu").append(list);
  $('#nestable-menu').nestable('serialize');
  
  function listify(strarr) {
    var l = $("<ol>").addClass("dd-list");
    $.each(strarr, function(i, v) {
      var c = $('<li data-nama="'+v.nama+'" data-url="'+v.url+'" data-icon="'+v.icon+'" data-color="'+v.color+'">').addClass("dd-item"),
      h = $('<div>').addClass("dd-handle text-primary").text(v.nama);
      h2 = $('<div>').addClass("dd-handle text-primary drag_disabled").text(v.nama);
      i = $('<div class="dd-button text-right" style=" position: absolute !important;top: 0px !important;right: 5px !important;">'+
        // '<button data-nama="'+v.nama+'" data-url="'+v.url+'" class=" btn btn-sm btn-primary" style="margin-right:15px;width:30px;" onclick="goShowMenu(this)"><i class="fa fa-edit"></i></button>'+
        '<button class="btn-delete-menu btn btn-sm btn-danger" data-nama="'+v.nama+'" data-url="'+v.url+'" style="width:30px;" onclick="goDeleteMenu(this)" ><i class="fa fa-times"></i></button>'
        );

      if ((v.nama != 'Menu')){
       l.append(c.append(h));
       l.append(c.append(i));
       if (!!v["children"])
        c.append(listify(v["children"]));
    }
    else{
     l.append(c.append(h2));
     if (!!v["children"])
      c.append(listify(v["children"]));  
  }
});
    return l;
  }

  var result = '';
  var updateOutput = function(e) {

    var list = e.length ? e : $(e.target),
    output = list.data('output');

    if (window.JSON) {
      output.val(window.JSON.stringify(list.nestable('serialize')));
      result=window.JSON.stringify(list.nestable('serialize'));
    } else {
      output.val('JSON browser support required for this demo.');
    }
    $('#result').val(result);

  };

  $('#nestable-menu').nestable().on('change', updateOutput);

  updateOutput($('#nestable-menu').data('output', $('#nestable-output')));

// --------------------------------------------------------------------------------------------------------------------------

$("#showOption").on("click", function(e) {
  $('[store="id"]').val('');
  $('#detail_generator tbody tr').remove();
  $('#header_generator tbody tr').remove();
  var buttons = $('<div>')
  .append(createButton('Blank', function() {

  })).append(createButton('CRUD', function() {

  })).append(createButton('Laporan', function() {

  }));


  e.preventDefault();
  Swal.fire({
    title: 'Pilih Tipe Menu',
    text: 'esc untuk batal',
    icon: 'warning',
    html: buttons,
    showConfirmButton: false,
    showCancelButton: false,
    confirmButtonText: 'Tidak',
    cancelButtonText: 'Batal',
    customClass: {
      confirmButton: 'btn btn-sm btn-neutral',
      cancelButton: 'btn btn-sm btn-danger'
    },
    buttonsStyling: false
  })
});

function createButton(text, cb) {
  return $(
    '<br/><button data-toggle="modal" data-target="#modal1" backdrop="static" onclick="optionSelected(this)" style="min-width:200px" class="btn btn-sm btn-primary">' +
    text + '</button><br/>').on('click', cb);
}

function optionSelected(ini) {

  menuPilihan = $(ini).text()
  if ((ini == 'Blank') || (ini == 'CRUD') || (ini == 'Laporan')){
    menuPilihan=ini

    $("#modal1").modal('show');
  }

  $('.modal-dialog').removeClass('modal-xlg');
  $('[store="tipe"]').val(menuPilihan)
  if (menuPilihan == 'Blank') {
    $('.modal-title').text('Menu baru')
    $('#namalabel').text('Nama Menu')
    $('#table1').addClass('undisplay')
    $('#tambahtable').addClass('undisplay')
  } else if (menuPilihan == 'CRUD') {
    $('.modal-title').text('Menu baru')
    $('#namalabel').text('Nama Master')
    $('#table1').removeClass('undisplay')
    $('#tambahtable').removeClass('undisplay')
  } else if (menuPilihan == 'Laporan') {
    $('.modal-title').text('Menu baru')
    $('#namalabel').text('Nama Master')
    $('#table1').addClass('undisplay')
    $('#tambahtable').addClass('undisplay')
  }
  $('#choosedIcon').addClass('ni ni-bold-right');
  $('#choosedIcon').css('color','var(--primary)');
  $('#choosedColor').css('background','var(--primary)');  
  swal.close();
}

function addIcon(ini){
  var iconBefore=$('#icon').val();
  var text = $(ini).text();
  $('#icon').val($.trim(text))
  $('#closeModal2').click();

  if (iconBefore==''){
    $('#choosedIcon').removeClass('ni ni-bold-right');
  }
  $('#choosedIcon').removeClass();
  $('#choosedIcon').addClass($.trim(text));
  $('#modal').focus();
}

function addColor(ini){
  var text = $(ini).text();
  $('#color').val($.trim(text));
  $('#closeModal3').click();
  $('#choosedColor').css('background','var(--'+$.trim(text)+')');  
  $('#choosedIcon').css('color','var(--'+$.trim(text)+')');
}

$('#nama').on('keyup',function(){
  $('#url').val($(this).val());
  $('#tablename1').val($(this).val());
})



function getGeneratorData(){
  generator = {};
  $('.table_baru').each(function(i,v){
   id_table= $(v).attr('id');
   id = id_table.replace("tableinput", "");
   tablename = $('#tablename'+id).val();
   generator[tablename] = []
   $('#'+id_table+' tbody tr').each(function(i2,v2){
     let tipe = $(v2).children().children('.data_tipe').val()
     let kolom = $(v2).children().children('.data_kolom').val()
     generator[tablename][i2] = {tipe:tipe,kolom:kolom}
   })

 })
  return generator;
}

var idIncrement = 0;

var table_list =
'<select class="table-list form-control form-control-sm data_kolom" placeholder="pilih table">' +
'<option value="" selected disabled>pilih table</option>' +
'@foreach($tables as $table)' +
'<option value="{{ $table->$db_name}}">{{ $table->$db_name}}</option>' +
'@endforeach' +
'</select>'
var input_basic =
'<input type="text" class="form-control form-control-sm data_kolom" value="">'

function addRowTable(id) {
  idIncrement = idIncrement + 1
  $('#tableinput'+id+' tbody').append(
    '<tr>' +
    '<td>' +
    '<select class="form-control form-control-sm data_tipe" id="tipe_data'+ idIncrement +'" onChange="changeTipeData(this)">' +

    '<option value="String">String</option>' +
    '<option value="Decimal">Integer</option>' +
    '<option value="Date">Date</option>' +
    // '<option value="DateTime">DateTime</option>' +
    '<option value="GetData">GetData</option>' +
    '</select>  ' +
    '</td>' +
    '<td id="nama_kolom' + idIncrement + '">' +
    '<input type="text" class="form-control  data_kolom" value="">' +
    '</td>' +
    '<td>' +
    '<button style="float:right;"class="btn btn-sm btn-danger hapus-row" '+
    'onclick="removeRow(this)">'+
    '<i class="fa fa-times"></i>' +
    '</button>' +
    '</td>' +
    '</tr>'
    );
  $('.select2').select2();
}

function changeTipeData(ini) {
  let id = $(ini).attr('id');
  id = id.replace("tipe_data", "");
  if ($(ini).val() == 'GetData') {
    $('#nama_kolom' + id).html(table_list);
    $('#nama_kolom' + id).children().addClass('select2');
    $('.select2').select2();
  } else {
    $('#nama_kolom' + id).html(input_basic);
  }
}


function addTable(){

  var totaltable = $('.table_custom').length;
  var newtable = totaltable+1;

  $('#table'+totaltable).after(
    '<div class="row table_custom" id="table'+newtable+'">'+
    '<div class="col-md-12 header-tab">'+
    '<div class="card">'+
    '<div class="card-body">'+
    '<div class="card-title text-center">'+
    '<div class="row">'+
    '<div class="col-md-3">'+
    '<input class="form-control form-control-sm" value="'+$('#tablename1').val()+'_detail'+newtable+'" id="tablename'+newtable+'">'+
    '</div>'+
    '<div class="col-md-6">'+
    '</div>'+
    '<div class="col-md-3">'+
    '<button class="btn btn-danger btn-sm float-right hapus-table" data-id="table'+newtable+'" onclick="removeTable(this)">hapus table</button>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '<table id="tableinput'+newtable+'" class="table table_baru">'+
    '<thead>'+
    '<tr>'+
    '<th width="150">type</th>'+
    '<th >kolom</th>'+
    '<th width="50">'+
    '<button style="float: right;" class="btn btn-sm btn-primary" onclick="addRowTable('+newtable+')">'+
    '<i class="fa fa-plus"></i>'+
    '</button>'+
    '</th>'+

    '</tr>'+
    '</thead>'+
    '<tbody></tbody>'+
    '</table>'+

    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'
    )
}

function removeTable(ini){
  id = $(ini).attr('data-id');
  $('#'+id).remove();
}

function removeRow(ini){
  $(ini).parent().parent().remove();
}
function fixSidebar(){    
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    url: '/fixsidebar',
    method: 'post',
    data:{data:fixsidebar},
    success: function(response){
      $(".preloaderlarabon").show();
      console.log(response)
      notify('top', 'center', 'success', 'inverse', 'animated fadeInDown', 'animated fadeOutDown' , 'bg-success','Fix Sidebar','success');


      $(".preloaderlarabon").fadeOut();
    }
  })
}

function clearModalMenu(){
  $('.hapus-table').each(function(i,v){
    $(v).click()
  })
  $('.hapus-row').each(function(i,v){
    $(v).click()
  })
  $('[store="nama_menu"]').val('');
  $('#tablename1').val('');
}

function copyText(ini,target){  
  let nama_menu = $(ini).val().toLowerCase();
  nama_menu = nama_menu.replace(' ','_',nama_menu);

  let datas = {}

  $(target).val(nama_menu)
}


</script>