<div id="modal_sidebar" class="modal animated fadeInDown" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content ">
			<div class="modal-header">

				<h4 class="modal-title">New Sidebar</h4>
				<button class="btn btn-sm btn-danger " data-dismiss="modal" id="closeModal">
					
					<i class="fa fa-times text-white"></i>

				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">

						<div class="form-group animated">
							<label class="control-label" >Nama</label>
							<input type="text" class="form-control" store="nama">
						</div>

					</div>

				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-success" onclick="newSidebar()">simpan
					<i class="ti-check"></i>
				</button>
			</div>
		</div>
	</div>
</div>
