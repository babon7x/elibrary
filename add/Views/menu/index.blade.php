@include('template.app')
<div class="main-content">
	<div class="container">

		<div class="row mt-2">
			<div class="col-md-12">
				<div class="card">

					<div class="card-body">
						<div class="card-title">
							<div class="row">
								<div class="col-md-6">
									<h3>List Menu</h3>
								</div>
								<div class="col-md-6 text-right">
									<button class="btn btn-primary btn-round" data-toggle="modal" data-target="#modal_menu" backdrop="static" onclick="clearModalMenu()">New Menu</button>
									<button class="btn btn-primary btn-round" data-toggle="modal" data-target="#modal_sidebar" backdrop="static">New Sidebar</button>
								</div>
							</div>
						</div>
						<br>
						<div class="dd myadmin-dd" id="nestable-menu" style="max-width: 100%;">
							
						</div>
						<br>
						<button class="btn btn-sm btn-success" onclick="goUpdateSidebar()" style="float: right;margin-right: 0px !important;">Simpan Perubahan</button>
					</div>
				</div>
				<textarea id="result" cols="30" rows="10" class="form-control undisplay"></textarea>
			</div>
		</div>


	</div>
</div>

@include('menu.script')
@include('menu.script2')
@include('menu.modal_sidebar')
@include('menu.modal_menu')
@include('menu.modal_icon')
@include('menu.modal_color')

