<div id="modal_menu" class="modal animated fadeInDown" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;overflow-y: visible;" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content " style="min-height: 500px">
			<div class="modal-header">

				<h4 class="modal-title">New Menu</h4>
				<button class="btn btn-sm btn-danger " data-dismiss="modal" id="closeModal">
					
					<i class="fa fa-times text-white"></i>

				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">

						<input type="text" class="form-control undisplay" store="id" name="id">

						<div class="form-group animated">
							<label class="control-label" >Nama</label>
							<input type="text" class="form-control" id="nama" store="nama_menu" name="nama" value="" autocomplete="off" onkeyup="copyText(this,'#tablename1')">
						</div>						

						<textarea id="generator_result" class="form-control undisplay"></textarea>



					</div>

				</div>

				<div class="row table_custom" id="table1">
					<div class="col-md-12 header-tab">
						<div class="card">
							<div class="card-body">
								<div class="card-title text-center">
									<div class="row">
										<div class="col-md-3">
											<input class="form-control form-control-sm" value="table1" id="tablename1" readonly>
										</div>
										<div class="col-md-6">
										</div>
										<div class="col-md-3">
										</div>
									</div>
								</div>
								<table id="tableinput1" class="table table_baru">
									<thead>
										<tr>
											<th width="150">type</th>
											<th >kolom</th>
											<th width="50">
												<button style="float: right;" class="btn btn-sm btn-primary" onclick="addRowTable(1)">
													<i class="fa fa-plus"></i>
												</button>
											</th>

										</tr>
									</thead>
									<tbody></tbody>
								</table>

							</div>
						</div>
					</div>
				</div>

			{{-- 	<div class="row justify-content-center">
					<div class="col-md-4 text-center">	
						<button class="btn btn-primary btn-sm" id="tambahtable" onclick="addTable()">tambah table</button>
					</div>
				</div> --}}


			</div>

			<div style="padding:20px;">
				<button type="button" class="btn btn-neutral undisplay" data-clipboard-text="active-40" title="" data-original-title="Copy to clipboard"  data-toggle="modal" data-target="#modal_icon" backdrop="static" >
					<div>
						<i class="ni ni-bold-right" id="choosedIcon"></i>
						<span>Pilih Icon</span>
					</div>
				</button>

				<button type="button" class="btn undisplay" data-clipboard-text="active-40" title="" data-original-title="Copy to clipboard"  data-toggle="modal" data-target="#modal_color" backdrop="static" id="choosedColor" style="background:var(--primary);color:white; -webkit-text-stroke: 0.2px var(--primary);">
					<div>
						<span >Pilih Warna Icon</span>
					</div>
				</button>
				<button class="btn btn-primary" id="tambahtable" onclick="addTable()">tambah table</button>
				<button class="btn btn-success" onclick="storeMenu()" style="float: right;">simpan
					<i class="ti-check"></i>
				</button>
			</div>
		</div>
	</div>
</div>

<script>
</script>
