<div id="modal_icon" class="modal animated fadeInDown" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content " style="min-height: 500px">
			<div class="modal-header">

				<h4 >pilih icon</h4>
				<button class="btn btn-sm btn-danger " data-dismiss="modal" id="closeModal2">
					
					<i class="fa fa-times text-white"></i>

				</button>
			</div>
			<div class="modal-body">
				<div class="row justify-content-center">
					<div class=" col ">
						<div class="card">
							<div class="card-header bg-transparent">
								<h3 class="mb-0">Icons</h3>
							</div>
							<div class="card-body">
								<div class="row icon-examples">
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="active-40" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-active-40"></i>
												<span>ni ni-active-40</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="air-baloon" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-air-baloon"></i>
												<span>ni ni-air-baloon</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="album-2" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-album-2"></i>
												<span>ni ni-album-2</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="align-center" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-align-center"></i>
												<span>ni ni-align-center</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="align-left-2" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-align-left-2"></i>
												<span>ni ni-align-left-2</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="ambulance" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-ambulance"></i>
												<span>ni ni-ambulance</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="app" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-app"></i>
												<span>ni ni-app</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="archive-2" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-archive-2"></i>
												<span>ni ni-archive-2</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="atom" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-atom"></i>
												<span>ni ni-atom</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="badge" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-badge"></i>
												<span>ni ni-badge</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="bag-17" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-bag-17"></i>
												<span>ni ni-bag-17</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="basket" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-basket"></i>
												<span>ni ni-basket</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="bell-55" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-bell-55"></i>
												<span>ni ni-bell-55</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="bold-down" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-bold-down"></i>
												<span>ni ni-bold-down</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="bold-left" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-bold-left"></i>
												<span>ni ni-bold-left</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="bold-right" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-bold-right"></i>
												<span>ni ni-bold-right</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="bold-up" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-bold-up"></i>
												<span>ni ni-bold-up</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="bold" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-bold"></i>
												<span>ni ni-bold</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="book-bookmark" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-book-bookmark"></i>
												<span>ni ni-book-bookmark</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="books" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-books"></i>
												<span>ni ni-books</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="box-2" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-box-2"></i>
												<span>ni ni-box-2</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="briefcase-24" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-briefcase-24"></i>
												<span>ni ni-briefcase-24</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="building" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-building"></i>
												<span>ni ni-building</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="bulb-61" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-bulb-61"></i>
												<span>ni ni-bulb-61</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="bullet-list-67" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-bullet-list-67"></i>
												<span>ni ni-bullet-list-67</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="bus-front-12" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-bus-front-12"></i>
												<span>ni ni-bus-front-12</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="button-pause" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-button-pause"></i>
												<span>ni ni-button-pause</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="button-play" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-button-play"></i>
												<span>ni ni-button-play</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="button-power" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-button-power"></i>
												<span>ni ni-button-power</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="calendar-grid-58" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-calendar-grid-58"></i>
												<span>ni ni-calendar-grid-58</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="camera-compact" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-camera-compact"></i>
												<span>ni ni-camera-compact</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="caps-small" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-caps-small"></i>
												<span>ni ni-caps-small</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="cart" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-cart"></i>
												<span>ni ni-cart</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="chart-bar-32" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-chart-bar-32"></i>
												<span>ni ni-chart-bar-32</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="chart-pie-35" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-chart-pie-35"></i>
												<span>ni ni-chart-pie-35</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="chat-round" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-chat-round"></i>
												<span>ni ni-chat-round</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="check-bold" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-check-bold"></i>
												<span>ni ni-check-bold</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="circle-08" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-circle-08"></i>
												<span>ni ni-circle-08</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="cloud-download-95" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-cloud-download-95"></i>
												<span>ni ni-cloud-download-95</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="cloud-upload-96" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-cloud-upload-96"></i>
												<span>ni ni-cloud-upload-96</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="compass-04" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-compass-04"></i>
												<span>ni ni-compass-04</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="controller" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-controller"></i>
												<span>ni ni-controller</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="credit-card" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-credit-card"></i>
												<span>ni ni-credit-card</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="curved-next" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-curved-next"></i>
												<span>ni ni-curved-next</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="delivery-fast" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-delivery-fast"></i>
												<span>ni ni-delivery-fast</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="diamond" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-diamond"></i>
												<span>ni ni-diamond</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="email-83" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-email-83"></i>
												<span>ni ni-email-83</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="fat-add" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-fat-add"></i>
												<span>ni ni-fat-add</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="fat-delete" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-fat-delete"></i>
												<span>ni ni-fat-delete</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="fat-remove" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-fat-remove"></i>
												<span>ni ni-fat-remove</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="favourite-28" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-favourite-28"></i>
												<span>ni ni-favourite-28</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="folder-17" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-folder-17"></i>
												<span>ni ni-folder-17</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="glasses-2" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-glasses-2"></i>
												<span>ni ni-glasses-2</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="hat-3" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-hat-3"></i>
												<span>ni ni-hat-3</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="headphones" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-headphones"></i>
												<span>ni ni-headphones</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="html5" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-html5"></i>
												<span>ni ni-html5</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="istanbul" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-istanbul"></i>
												<span>ni ni-istanbul</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="circle-08" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-circle-08"></i>
												<span>ni ni-circle-08</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="key-25" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-key-25"></i>
												<span>ni ni-key-25</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="laptop" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-laptop"></i>
												<span>ni ni-laptop</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="like-2" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-like-2"></i>
												<span>ni ni-like-2</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="lock-circle-open" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-lock-circle-open"></i>
												<span>ni ni-lock-circle-open</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="map-big" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-map-big"></i>
												<span>ni ni-map-big</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="mobile-button" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-mobile-button"></i>
												<span>ni ni-mobile-button</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="money-coins" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-money-coins"></i>
												<span>ni ni-money-coins</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="note-03" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-note-03"></i>
												<span>ni ni-note-03</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="notification-70" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-notification-70"></i>
												<span>ni ni-notification-70</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="palette" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-palette"></i>
												<span>ni ni-palette</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="paper-diploma" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-paper-diploma"></i>
												<span>ni ni-paper-diploma</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="pin-3" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-pin-3"></i>
												<span>ni ni-pin-3</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="planet" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-planet"></i>
												<span>ni ni-planet</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="ruler-pencil" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-ruler-pencil"></i>
												<span>ni ni-ruler-pencil</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="satisfied" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-satisfied"></i>
												<span>ni ni-satisfied</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="scissors" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-scissors"></i>
												<span>ni ni-scissors</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="send" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-send"></i>
												<span>ni ni-send</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="settings-gear-65" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-settings-gear-65"></i>
												<span>ni ni-settings-gear-65</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="settings" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-settings"></i>
												<span>ni ni-settings</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="single-02" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-single-02"></i>
												<span>ni ni-single-02</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="single-copy-04" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-single-copy-04"></i>
												<span>ni ni-single-copy-04</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="sound-wave" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-sound-wave"></i>
												<span>ni ni-sound-wave</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="spaceship" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-spaceship"></i>
												<span>ni ni-spaceship</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="square-pin" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-square-pin"></i>
												<span>ni ni-square-pin</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="support-16" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-support-16"></i>
												<span>ni ni-support-16</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="tablet-button" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-tablet-button"></i>
												<span>ni ni-tablet-button</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="tag" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-tag"></i>
												<span>ni ni-tag</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="tie-bow" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-tie-bow"></i>
												<span>ni ni-tie-bow</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="time-alarm" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-time-alarm"></i>
												<span>ni ni-time-alarm</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="trophy" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-trophy"></i>
												<span>ni ni-trophy</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="tv-2" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-tv-2"></i>
												<span>ni ni-tv-2</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="umbrella-13" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-umbrella-13"></i>
												<span>ni ni-umbrella-13</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="user-run" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-user-run"></i>
												<span>ni ni-user-run</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="vector" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-vector"></i>
												<span>ni ni-vector</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="watch-time" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-watch-time"></i>
												<span>ni ni-watch-time</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="world" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-world"></i>
												<span>ni ni-world</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="zoom-split-in" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-zoom-split-in"></i>
												<span>ni ni-zoom-split-in</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="collection" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-collection"></i>
												<span>ni ni-collection</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="image" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-image"></i>
												<span>ni ni-image</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="shop" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-shop"></i>
												<span>ni ni-shop</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="ungroup" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-ungroup"></i>
												<span>ni ni-ungroup</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="world-2" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-world-2"></i>
												<span>ni ni-world-2</span>
											</div>
										</button>
									</div>
									<div class="col-lg-3 col-md-6">
										<button type="button" class="btn-icon-clipboard" data-clipboard-text="ui-04" title="" data-original-title="Copy to clipboard" onclick="addIcon(this)">
											<div>
												<i class="ni ni-ui-04"></i>
												<span>ni ni-ui-04</span>
											</div>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>