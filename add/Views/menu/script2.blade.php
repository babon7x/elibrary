<script>

    function storeMenu(){

        spinnerStart();
        
        let nama_menu = $('[store="nama_menu"]').val().toLowerCase();
        let splitStr = nama_menu.split(' ');

        for (let i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
        }

        let nama = splitStr.join(' ');
        let nama_model = nama.replace(' ','',nama);

        nama_menu = nama_menu.replace(' ','_',nama_menu);

        let datas = {}

        datas['url'] = nama_menu; 
        datas['nama'] = nama; 
        datas['icon'] = 'fa fa-circle';
        datas['color'] = 'primary';
        datas['nama_model'] = nama_model;
        
        datas['table'] = getGeneratorData();

        if (datas['url'] == ''){
            notify('top', 'center', 'danger', 'inverse', 'animated fadeInDown', 'animated fadeOutDown'  , 'bg-success','wtf','lengkapi dong !');
            return false;
        }

        $('.table').each(function(i,v){
            totaltable=i
        })

        let cekkolom=0;
        $.each(datas['table'],function(i,v){
            if (v.length == 0){
                cekkolom = 1;
            }
        })
        if (cekkolom == 1){
            notify('top', 'center', 'danger', 'inverse', 'animated fadeInDown', 'animated fadeOutDown'  , 'bg-success','wtf','table harus punya kolom !');          
            return false;
        }

        datas['totaltable'] = totaltable;
        
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: 'menu',
            method: 'POST',
            data: datas,
            success: function (response) {

                notify('top', 'center', 'success', 'inverse', 'animated fadeInDown', 'animated fadeOutDown'  , 'bg-success','berhasil','simpan data');

                let akses = {"url":nama_menu,"nama":nama,"icon":'fa fa-circle',"color":'primary'};
                var akses_sekarang = JSON.parse($('#result').val())
                akses_sekarang.push(akses)
                $('#result').val(JSON.stringify(akses_sekarang))
                var list = listify(akses_sekarang);
                $('#nestable-menu').html('');
                $("#nestable-menu").append(list);

                goUpdateSidebar();
                spinnerEnd();
                $('#modal_menu').modal('hide');
            },
            error:function(response){
                let errors = response.responseJSON.errors;
                let urutan = 0;
                let focus = '';

                $.each(errors,function(i,v){
                    urutan=urutan+1;
                    if (urutan==1){
                        index_response = i;
                        info_response = v[0];
                    }
                })

                $('[store]').not('[store="id"]').each(function(i,v){
                    $(v).removeClass('is-invalid');
                    $(v).next('.invalid-feedback').addClass('invalid-feedback');

                });

                urutan = 0;
                $.each(errors,function(i,v){
                    urutan=urutan+1;
                    $('[store="'+i+'"]').addClass('is-invalid');
                    $('[store="'+i+'"]').next().text(v);
                    if (urutan == 1){
                        focus = i;
                    }
                })

                notify('top', 'center', 'danger', 'inverse', 'animated fadeInDown', 'animated fadeOutDown' , 'bg-danger',index_response,info_response);
                
                spinnerEnd();
                $('[store="'+focus+'"]').focus();
            }
        })

    }


    function goShowMenu(ini) {
     spinnerStart();
     let url = 'menu';
     let id = $(ini).attr("data-id");

     $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: url + "/getdata",
        method: "POST",
        data: { id: id },
        success: function (response) {
            optionSelected('Blank')
            
            $('#choosedIcon').removeClass();
            $('#choosedIcon').addClass(response[0].icon);
            
            $('#choosedColor').css('background','var(--'+response[0].color+')');  
            $('#choosedIcon').css('color','var(--'+response[0].color+')');

            $.each(response[0], function (i, v) {
                $('[store="' + i + '"]')
                .val(v)
                .change();

                // if ($('[store="'+i+'"]').hasClass('select2')){
                // $('[store="'+i+'"]').text(v)
                // }
            });
            spinnerEnd();
        },
    });
 }

 function goStoreMenu(url) {

    spinnerStart();
    let index_response = "";
    let info_response = "";
    let method = "POST";
    let value = "";
    let datas = {};
    let id = $('[store="id"]').val();
    let key = "";

    if (id != "") {
        url = url + "/" + $('[store="id"]').val();
        method = "PATCH";
        datas["id"] = id;
    }

    $("[store]")
    .not('[store="id"]')
    .each(function (i, v) {
        value = $(this).val();
        key = $(this).attr("store");
        if (key != "url") {
            $('.dd-item[data-id="' + id + '"]').data(key, value);
            $('.dd-item[data-id="' + id + '"]>.dd-handle').text(value);
        }

        if ($(this).hasClass("select2")) {
            let value_select = "";
            let text_select = $(this)
            .next(".select2-container")
            .children(".selection")
            .children(".select2-selection")
            .children()
            .attr("title");

            $(this)
            .children()
            .each(function (i2, v2) {
                if (text_select == $(v2).text()) {
                    value_select = $(v2).val();
                }
            });
            value = value_select;
        }

        $("#nestable-menu").change();
        datas[$(this).attr("store")] = value;
    });
    datas["generatorData"] = "";
    if (datas["tipe"] != "Blank") {
        // storeGenerator();

        // datas["generator"] = JSON.parse($("#generator_result").val());
        datas["generatorData"] = getGeneratorData();
    }

    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: url,
        method: method,
        data: datas,
        success: function (response) {
            console.log(response)
            index_response='Berhasil';
            info_response='simpan data';
            
            $('button[data-dismiss]').click();

            if (id == ''){
             notify('top', 'center', 'success', 'inverse', 'animated fadeInDown', 'animated fadeOutDown' , 'bg-success',index_response,info_response);
         }
         else{
             notify('top', 'center', 'success', 'inverse', 'animated fadeInDown', 'animated fadeOutDown'  , 'bg-success',index_response,info_response);
         }

         $('[store]').not('[store="id"]').each(function(i,v){
             $(v).removeClass('is-invalid');
             $(v).next('.invalid-feedback').addClass('invalid-feedback');
         });

         var akses = {
            "url":response.url,
            "nama":response.nama,
            "id":response.id,
            "icon":response.icon,
            "color":response.color
        };
        var akses_sekarang = JSON.parse($('#result').val())
        if (method == 'POST'){
            akses_sekarang.push(akses)
        }
        $('#result').val(JSON.stringify(akses_sekarang))
        var list = listify(akses_sekarang);
        $('#nestable-menu').html('<button class="btn btn-sm btn-success waves-effect waves-success mr-4 text-white" onclick="goUpdateSidebar()" style="float: right;margin-right: 0px !important;">simpan</button><br><br>');
        $("#nestable-menu").append(list);

        goUpdateSidebar()

        $('[store="id"]').val('');
        $('[store="nama"]').val('');
        $('[store="url"]').val('');
        $('[store="icon"]').val('ni ni-bold-right');
        $('[store="color"]').val('primary');
        $('#closeModal').click();   
        spinnerEnd();
    },
    error: function (response) {
        let errors = response.responseJSON.errors;
        let urutan = 0;

        $.each(errors, function (i, v) {
            urutan = urutan + 1;
            if (urutan == 1) {
                index_response = i;
                info_response = v[0];
            }
        });

        $("[store]")
        .not('[store="id"]')
        .each(function (i, v) {
            $(v).removeClass("is-invalid");
            $(v).next(".invalid-feedback").addClass("invalid-feedback");
        });

        $.each(errors, function (i, v) {
            $('[store="' + i + '"]').addClass("is-invalid");
            $('[store="' + i + '"]')
            .next()
            .text(v);
        });
        if (id == "") {
            notify(
                "top",
                "center",
                "danger",
                "inverse",
                "animated fadeInDown",
                "animated fadeOutDown",
                "bg-danger",
                index_response,
                info_response
                );
        } else {
            notify(
                "top",
                "center",
                "danger",
                "inverse",
                "animated fadeInDown",
                "animated fadeOutDown",
                "bg-danger",
                index_response,
                info_response
                );
        }

        spinnerEnd();
    },
});
}

function goUpdateSidebar() {

   spinnerStart();

   var result = "";

   let url = 'menu';
   let id = '';
   let sidebar = $("#result").val();
   let value = "";


   $.each(JSON.parse(sidebar), function (i, v) {
    if (i >= 0) {
        if (v.children == undefined) {
            value = "'" + v.url + "'";
            result =
            result +
            '<li class="nav-item link" nama="' +
            v.nama +
            '">\n' +
            '<a class="nav-link" href="/' +
            v.nama.replace(/\s+/g, '_').toLowerCase() +
            '">\n' +
            '<i class="'+v.icon+' text-'+v.color+'"></i>'+
            '<span class="nav-link-text">'+v.nama+'</span>\n'+
            "</a>\n" +
            "</li>\n\n";
        } else {
            result =
            result +
            '<li class="nav-item" nama="' +
            v.nama +
            '">\n' +
            '<a href="#' +
            v.nama.replace(/\s+/g, '_').toLowerCase() +
            '" data-toggle="collapse" aria-expanded="false" class="nav-link dropdown-toggle">\n' +
            '<i class="'+v.icon+' text-'+v.color+'"></i>'+
            '<span class="nav-link-text">'+v.nama+'</span>\n'+
            "</a>\n" +
            '<ul class="navbar-nav collapse" id="' +
            v.nama.replace(/\s+/g, '_').toLowerCase() +
            '">\n';

            $.each(v.children, function (i2, v2) {
                if (v2.children == undefined) {
                    value = "'" + v2.url + "'";
                    result =
                    result +
                    '<li class="nav-item link" nama="' +
                    v2.id +
                    '">\n' +
                    '<a class="nav-link" href="/' +
                    v2.nama.replace(/\s+/g, '_').toLowerCase() +
                    '">\n' +
                    '<i class="'+v2.icon+' text-'+v2.color+'"></i>'+
                    '<span class="nav-link-text">'+v2.nama+'</span>\n'+
                    "</a>\n" +
                    "</li>\n\n";
                } else {
                    result =
                    result +
                    '<li class="nav-item" nama="' +
                    v2.id +
                    '">\n' +
                    '<a href="#' +
                    v2.nama.replace(/\s+/g, '_').toLowerCase() +
                    '" data-toggle="collapse" aria-expanded="false" class="nav-link dropdown-toggle">\n' +
                    '<i class="'+v2.icon+' text-'+v2.color+'"></i>'+
                    '<span class="nav-link-text">'+v2.nama+'</span>\n'+
                    "</a>\n" +
                    '<ul class="navbar-nav collapse" id="' +
                    v2.nama.replace(/\s+/g, '_').toLowerCase()+
                    '">\n';
                    $.each(v2.children, function (i3, v3) {
                        if (v3.children == undefined) {
                            value = "'" + v3.url + "'";
                            result =
                            result +
                            '<li class="nav-item link" nama="' +
                            v3.id +
                            '">\n' +
                            '<a class="nav-link" href="/' +
                            v3.nama.replace(/\s+/g, '_').toLowerCase() +
                            '">\n' +
                            '<i class="'+v3.icon+' text-'+v3.color+'"></i>'+
                            '<span class="nav-link-text">'+v3.nama+'</span>\n'+
                            "</a>\n" +
                            "</li>\n\n";
                        } else {
                            result =
                            result +
                            '<li class="nav-item" nama="' +
                            v3.id +
                            '">\n' +
                            '<a href="#' +
                            v3.nama.replace(/\s+/g, '_').toLowerCase()
                            '" data-toggle="collapse" aria-expanded="false" class="nav-link dropdown-toggle">\n' +
                            '<i class="'+v3.icon+' text-'+v3.color+'"></i>'+
                            '<span class="nav-link-text">'+v3.nama+'</span>\n'+
                            "</a>\n" +
                            '<ul class="navbar-nav collapse" id="' +
                            v3.nama.replace(/\s+/g, '_').toLowerCase()+
                            '">\n';
                            $.each(v3.children, function (i4, v4) {
                                if (v4.children == undefined) {
                                    value = "'" + v4.url + "'";
                                    result =
                                    result +
                                    '<li class="nav-item link" nama="' +
                                    v4.id +
                                    '">\n' +
                                    '<a class="nav-link" href="/' +
                                    v4.nama.replace(/\s+/g, '_').toLowerCase() +
                                    '">\n' +
                                    '<i class="'+v4.icon+' text-'+v4.color+'"></i>'+
                                    '<span class="nav-link-text">'+v4.nama+'</span>\n'+
                                    "</a>\n" +
                                    "</li>\n\n";
                                } else {
                                    result =
                                    result +
                                    '<li class="nav-item" nama="' +
                                    v4.id +
                                    '">\n' +
                                    '<a href="#' +
                                    v4.nama.replace(/\s+/g, '_').toLowerCase()
                                    '" data-toggle="collapse" aria-expanded="false" class="nav-link dropdown-toggle">\n' +
                                    '<i class="'+v4.icon+' text-'+v4.color+'"></i>'+
                                    '<span class="nav-link-text">'+v4.nama+'</span>\n'+
                                    "</a>\n" +
                                    '<ul class="navbar-nav collapse" id="' +
                                    v4.nama.replace(/\s+/g, '_').toLowerCase()+
                                    '">\n';
                                    $.each(v4.children, function (i5, v5) {
                                        value = "'" + v5.url + "'";
                                        result =
                                        result +
                                        '<li class="nav-item link" nama="' +
                                        v5.id +
                                        '">\n' +
                                        '<a class="nav-link" href="/' +
                                        v5.nama.replace(/\s+/g, '_').toLowerCase() +
                                        '">\n' +
                                        '<i class="'+v5.icon+' text-'+v5.color+'"></i>'+
                                        '<span class="nav-link-text">'+v5.nama+'</span>\n'+
                                        "</a>\n" +
                                        "</li>\n\n";
                                    });
                                    result = result + "</ul>\n</li>\n\n";
                                }
                            });
                            result = result + "</ul>\n</li>\n\n";
                        }
                    });
result = result + "</ul>\n</li>\n\n";
}
});
result = result + "</ul>\n</li>\n\n";
}
}
});

$.ajax({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
    url: url + "/storesidebar",
    method: "POST",
    data: { id: id, sidebar: sidebar, result: result },
    success: function (response) {

        spinnerEnd();
        notify(
            "top",
            "center",
            "success",
            "inverse",
            "animated fadeInDown",
            "animated fadeOutDown",
            "bg-success",
            "berhasil",
            "update sidebar"
            );

        $(".sidebar-menu").html(response.result);
        // addSidebarActive(3)
    },
});
}

function goDeleteMenu(ini) {

    let url = 'menu';
    let id = $(ini).attr("data-url");
    // let ids = [];
    // ids.push($(ini).attr("data-url"));


    Swal.fire({
        title: "Hapus Data",
        text: "data tidak dapat dikembalikan !",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Tidak",
        cancelButtonText: "Ya",
        customClass: {
            confirmButton:
            "btn btn-lg btn-primary mr-4",
            cancelButton:
            "btn btn-lg btn-danger ml-4",
        },
        buttonsStyling: false,
    }).then((result) => {
        if (!result.isConfirmed) {


            $.ajax({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                        ),
                },
                url: url + "/ids",
                method: "delete",
                data: { id: id },
                success: function (response) {
                    // console.log(response)
                    // return false;
                    let domHead = $('.dd-item[data-url="' + id + '"]').parent();
                    let domBackUp = $(
                        '.dd-item[data-url="' + id + '"]>.dd-list'
                        ).children();
                    $('.dd-item[data-url="' + id + '"]').remove();
                    $(domHead).append(domBackUp);
                    $("#nestable-menu").change();
                    goUpdateSidebar();

                    notify(
                        "top",
                        "center",
                        "success",
                        "inverse",
                        "animated fadeInDown",
                        "animated fadeOutDown",
                        "bg-success",
                        "berhasil",
                        "hapus data"
                        );
                },
            });
        }
    });
}

function goRemoveMenu(ini) {
    let id = $(ini).attr("data-id");
    if ($('.dd-item[data-id="' + id + '"]').attr("data-akses") == 0) {
        $('.dd-item[data-id="' + id + '"]>.dd-handle').removeClass(
            "bg-dark text-white"
            );

        $('.dd-item[data-id="' + id + '"]').attr("data-akses", 1);
        $('.dd-item[data-id="' + id + '"]').data("akses", 1);
        notify(
            "top",
            "right",
            "success",
            "inverse",
            "animated fadeInDown",
            "animated fadeOutDown",
            "bg-success",
            "berhasil",
            "menampilkan menu"
            );
    } else {
        $('.dd-item[data-id="' + id + '"]>.dd-handle').addClass(
            "bg-dark text-white"
            );
        $('.dd-item[data-id="' + id + '"]').attr("data-akses", 0);
        $('.dd-item[data-id="' + id + '"]').data("akses", 0);
        notify(
            "top",
            "right",
            "warning",
            "inverse",
            "animated fadeInDown",
            "animated fadeOutDown",
            "bg-warning",
            "berhasil",
            "menghilangkan menu"
            );
    }

    $("#nestable-menu").change();
}

function newSidebar(){

    spinnerStart();

    let nama_menu = $('[store="nama"]').val().toLowerCase();
    let splitStr = nama_menu.split(' ');

    for (let i = 0; i < splitStr.length; i++) {
        splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
    }

    let nama = splitStr.join(' ');
    let nama_model = nama.replace(' ','',nama);

    nama_menu = nama_menu.replace(' ','_',nama_menu);

    let datas = {url:nama_menu,nama:nama,nama_model:nama_model,"icon":'fa fa-circle',"color":'primary'}


    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: "menu/newsidebar",
        method: "POST",
        data: datas,
        success: function (response) {

         let akses = {"url":nama_menu,"nama":nama,"icon":'fa fa-circle',"color":'primary'};
         let akses_sekarang = JSON.parse($('#result').val());

         akses_sekarang.push(akses)
         $('#result').val(JSON.stringify(akses_sekarang))
         var list = listify(akses_sekarang);
         $('#nestable-menu').html('');
         $("#nestable-menu").append(list);

         goUpdateSidebar()
         spinnerEnd();
     },
 });

    $('#modal_sidebar').modal('hide');

}




</script>