<!DOCTYPE html>
  <html lang="en">
  <head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>ELibrary &mdash; Entliven</title>

  <!-- General CSS Files -->

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <title>E-Library - Home</title>
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <style>
  #view{
  padding:30px;
}
.table{
  margin-bottom: 50px;
}
</style>  

</head>

<body >
<div id="app">
<div id="view">
				<div class="row">
					<div class="col-md-12" style="text-align:end;">				
						<span style="font-size: 15px;"><u>PT. CIRIAJASA ENGINEERING CONSULTANS</u></span>
						<br>
						<span style="font-size: 10px;">Management, Engineering, Social, Development, Environmental</span>			
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-12 text-center">
						<span style="font-size: 20px;" class="tahunterakhir">PENGALAMAN PERUSAHAAN 1 TERAKHIR</span>
						<br>
						<span style="font-size: 14px;">YANG MENGGAMBARKAN KUALIFIKASI TERBAIK</span>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-12 container-pdf">
						<div class="table-pdf"></div><table class="table table-bordered table-pdf"><tbody><tr><td width="80" class="text-center">1</td><td width="250">Pengguna Jasa</td><td width="30">:</td><td colspan="3">aa</td></tr><tr><td width="80" class="text-center">2</td><td width="250">Nama Paket Pekerjaan</td><td width="30">:</td><td colspan="3">aa</td></tr><tr><td width="80" class="text-center">3</td><td width="250">Lingkup Produk Utama</td><td width="30">:</td><td colspan="3">aa</td></tr><tr><td width="80" class="text-center">4</td><td width="250">Lokasi Proyek</td><td width="30">:</td><td colspan="3">aa</td></tr><tr><td width="80" class="text-center">5</td><td width="250">Nilai Kontrak</td><td width="30">:</td><td colspan="3">aa</td></tr><tr><td width="80" class="text-center">6</td><td width="250">No. Kontrak</td><td width="30">:</td><td colspan="3">aa</td></tr><tr><td width="80" class="text-center">7</td><td width="250">Waktu Pelaksanaan</td><td width="30">:</td><td colspan="3">01 Dec 2021   s/d   31 Dec 2021</td></tr><tr><td width="80" rowspan="3" class="text-center">8</td><td width="250">Nama Pemimpin Kemitraan</td><td width="30">:</td><td colspan="3">aa</td></tr><tr><td width="250">Alamat</td><td width="30">:</td><td colspan="3">aa</td></tr><tr><td width="250">Negara Asal</td><td width="30">:</td><td colspan="3">aa</td></tr><tr><td width="80" rowspan="2" class="text-center">9</td><td width="250" rowspan="2">Jumlah Tenaga Ahli</td><td width="30" rowspan="2">:</td><td width="250">Jumlah Ahli Asing</td><td width="30">:</td><td class="totalTenagaAsing">2 Orang Bulan</td></tr><tr><td width="250">Jumlah Ahli Indonesia</td><td width="30">:</td><td class="totalTenagaIndonesia">14 Orang Bulan</td></tr><tr><td width="80" rowspan="2" class="text-center">10</td><td width="280" rowspan="2" colspan="2">Perusahaan Mitra Kerja</td><td colspan="3" class="text-center">Jumlah Tenaga Ahli</td></tr><tr class="row_perusahaan"><td colspan="2" class="text-center">Asing</td><td class="text-center">Indonesia</td></tr><tr class="row_perusahaan"><td width="80" class="text-center" style="font-size:12px;">a</td><td width="280" colspan="2">a</td><td colspan="2">2 Orang Bulan</td><td>14 Orang Bulan</td></tr><tr><td width="80" rowspan="2" class="text-center">11</td><td colspan="5">Tenaga Ahli Tetap yang Terlibat</td></tr><tr class="row_detail"><td colspan="2" class="text-center">Posisi</td><td colspan="2" class="text-center">Keahlian</td><td colspan="2" class="text-center">Jumlah Orang</td></tr><tr class="row_detail"><td width="80" class="text-center" style="font-size:12px;">a</td><td width="250" colspan="2">a</td><td colspan="2">a</td><td>2</td></tr><tr class="row_detail"><td width="80" class="text-center" style="font-size:12px;">b</td><td width="250" colspan="2">a</td><td colspan="2">b</td><td>4</td></tr></tbody></table><table class="table table-bordered table-pdf"><tbody><tr><td width="80" class="text-center">1</td><td width="250">Pengguna Jasa</td><td width="30">:</td><td colspan="3">b</td></tr><tr><td width="80" class="text-center">2</td><td width="250">Nama Paket Pekerjaan</td><td width="30">:</td><td colspan="3">b</td></tr><tr><td width="80" class="text-center">3</td><td width="250">Lingkup Produk Utama</td><td width="30">:</td><td colspan="3">b</td></tr><tr><td width="80" class="text-center">4</td><td width="250">Lokasi Proyek</td><td width="30">:</td><td colspan="3">b</td></tr><tr><td width="80" class="text-center">5</td><td width="250">Nilai Kontrak</td><td width="30">:</td><td colspan="3">b</td></tr><tr><td width="80" class="text-center">6</td><td width="250">No. Kontrak</td><td width="30">:</td><td colspan="3">b</td></tr><tr><td width="80" class="text-center">7</td><td width="250">Waktu Pelaksanaan</td><td width="30">:</td><td colspan="3">19 Dec 2021   s/d   31 Dec 2021</td></tr><tr><td width="80" rowspan="3" class="text-center">8</td><td width="250">Nama Pemimpin Kemitraan</td><td width="30">:</td><td colspan="3">b</td></tr><tr><td width="250">Alamat</td><td width="30">:</td><td colspan="3">b</td></tr><tr><td width="250">Negara Asal</td><td width="30">:</td><td colspan="3">b</td></tr><tr><td width="80" rowspan="2" class="text-center">9</td><td width="250" rowspan="2">Jumlah Tenaga Ahli</td><td width="30" rowspan="2">:</td><td width="250">Jumlah Ahli Asing</td><td width="30">:</td><td class="totalTenagaAsing">28 Orang Bulan</td></tr><tr><td width="250">Jumlah Ahli Indonesia</td><td width="30">:</td><td class="totalTenagaIndonesia">49 Orang Bulan</td></tr><tr><td width="80" rowspan="2" class="text-center">10</td><td width="280" rowspan="2" colspan="2">Perusahaan Mitra Kerja</td><td colspan="3" class="text-center">Jumlah Tenaga Ahli</td></tr><tr class="row_perusahaan"><td colspan="2" class="text-center">Asing</td><td class="text-center">Indonesia</td></tr><tr class="row_perusahaan"><td width="80" class="text-center" style="font-size:12px;">a</td><td width="280" colspan="2">a</td><td colspan="2">14 Orang Bulan</td><td>13 Orang Bulan</td></tr><tr class="row_perusahaan"><td width="80" class="text-center" style="font-size:12px;">b</td><td width="280" colspan="2">b</td><td colspan="2">12 Orang Bulan</td><td>22 Orang Bulan</td></tr><tr><td width="80" rowspan="2" class="text-center">11</td><td colspan="5">Tenaga Ahli Tetap yang Terlibat</td></tr><tr class="row_detail"><td colspan="2" class="text-center">Posisi</td><td colspan="2" class="text-center">Keahlian</td><td colspan="2" class="text-center">Jumlah Orang</td></tr><tr class="row_detail"><td width="80" class="text-center" style="font-size:12px;">a</td><td width="250" colspan="2">b</td><td colspan="2">b</td><td>45</td></tr></tbody></table><br><br><br><br>
					
				</div>
			</div>
		</div>
</div>

</body>
</html>