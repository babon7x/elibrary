<link href="{{asset('datatable/datatables.css')}}"></link>
<script src="{{asset('datatable/datatables.js')}}"></script>
<script src="{{asset('assets/dt_master.js')}}"></script>
<script src="{{asset('assets/data.js')}}"></script>

<script>
	$(".select2").select2({dropdownAutoWidth : true});
	$(".numberFormat").each(function() {
		new AutoNumeric(this,
		{
			watchExternalChanges: true,
			allowDecimalPadding: false
		});
	});
	$(".datepicker").datepicker({
		format: "dd M yyyy"
	});

	$('#header_home').addClass('btn btn-primary text-white')

	var selectYearStart = $('#selectYearStart').val()
	var selectYear = $('#selectYear').val()
	var url = "proyek";
	var dataColum = [
	
	{id:null,
		"render": function ( data, type, full, meta ) {
			let baris =meta.row+1;
			let defaultContent = baris;
			return  defaultContent;
		}
	}];
	dataColum.push({data: 'nama_paket_pekerjaan', name: 'nama_paket_pekerjaan'})
	dataColum.push({data: 'lokasi_proyek', name: 'nama_paket_pekerjaan'})
	dataColum.push({id:null,'render': function ( data, type, full, meta ) {
		let tahunAwal = moment(full.waktu_pelaksanaan_mulai).format('yyyy');
		let tahunAkhir = moment(full.waktu_pelaksanaan_selesai).format('yyyy');
		if (tahunAwal == tahunAkhir){
			return  tahunAwal;
		}
		else{
			return tahunAwal+' - '+tahunAkhir
		}
	} });

	dtMasterProyek(url,dataColum,selectYearStart,selectYear,'');

	$('#datatable tbody tr').on('dblclick',function(){
		console.log('test')
	})

</script>