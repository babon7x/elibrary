<div class="modal fade" id="modalTahunTerakhir" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Input Berapa Tahun Terakhir </h5>
        <button type="button" class="btn btn-danger btn-sm btn-floating" id="closeModalTahunTerakhir">
          <i class="fa fa-times"></i>
        </button>
      </div>
      <div class="modal-body" id="modalBodyTahunTerakhir">
       <div class="form-group row">
        <label for="inputPassword" class="col-sm-8 col-form-label text-center">Nama Paket Pekerjaan</label>
        <label for="inputPassword" class="col-sm-4 col-form-label text-center">Tahun Terakhir</label>

      </div>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary text-white" id="validasiTahunTerakhir" onclick="validasiTahunTerakhir()">Download</button>
    </div>
  </div>
</div>
</div>
