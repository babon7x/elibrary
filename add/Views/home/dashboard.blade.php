@extends("template.app_fe")
@section('content')

@section('customStyles')


<style>
    .dropdown-toggle-no-caret:after {
        content: none !important;
    }

    #tableViewProyek>tbody>tr>td:nth-child(1) {
        width: 30%;
    }

    #tableViewProyek>tbody>tr {
        line-height: 3;
    }

    #viewProyek {
        margin-bottom: 15px;
    }
    .ellipsis{
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }
    #viewPDF {
        padding: 20px;
    }
    .dropdown-toggle-no-caret:after {
        content: none !important;
    }
</style>

@endsection

<main class="page landing-page">
    <section class="clean-block about-us">
        <!-- Start: search -->
        @include("template.app_search")
        <!-- End: search -->
        <div class="container">
            <div class="undisplay" id='totalTopProyek' data-total="{{count($topproyek)}}"></div>
            <!-- Start: Slider Top Proyek -->
            <div class="row slider responsive mobile__pt-20">
                @foreach($topproyek as $key => $value)
                <div class="col-12 col-md-4" style="cursor: pointer;" onclick="bannerClick({{ $value->id }})">
                    <div class="card text-center clean-card" style="border-radius: 6px;padding: 20px;">
                        <div class='slider-image-container'>

                            <img class="card-img-top w-100 d-block" src="{{ url('cover_proyek') }}/{{ $value->id }}.jpg" alt="image-{{ $value->nama_paket_pekerjaan }}">
                        </div>
                        <div class="card-body info">
                            <h4 class="card-title proyek-title"  title="{{ $value->nama_paket_pekerjaan }}" data-toggle="tooltip" data-placement="bottom" style="text-align: left;"><strong>
                                <?php $out = strlen($value->nama_paket_pekerjaan) > 65 ? substr($value->nama_paket_pekerjaan,0,65)."..." : $value->nama_paket_pekerjaan;?>
                                {{ $out }}
                            </strong></h4>
                            <hr>
                            <div class="d-flex justify-content-between">
                                <p title="{{ $value->lokasi_proyek }}" data-toggle="tooltip" data-placement="bottom" class="mb-0">
                                    <?php $lokasi = strlen($value->lokasi_proyek) > 30 ? substr($value->lokasi_proyek,0,30)."..." : $value->lokasi_proyek;?>
                                    {{ $lokasi }}
                                <p class="mb-0" id="tahunBanner">
                                    <?php 
                                    $tahunAwal = date("Y", strtotime($value->waktu_pelaksanaan_mulai));
                                    $tahunAkhir = date("Y", strtotime($value->waktu_pelaksanaan_selesai));
                                    if ($tahunAwal == $tahunAkhir){
                                        echo($tahunAwal);
                                    }
                                    else{
                                        echo ($tahunAwal.' - '.$tahunAkhir);
                                    }

                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div><!-- End: Slider Top Proyek -->
            <!-- Start: List Proyek -->

            <section class="mt-4">
                <div class="row">
                    <div class="col">
                      @if($user_id != '')
                      <div
                      class="d-flex flex-column justify-content-center flex-md-row justify-content-md-end action-table-links">
                      <a class="link-normal" href="#" onclick="downloadProyek2('')"><i class="far fa-file-pdf fa-fw"></i>Download Project
                      Sheet</a>
                      <a class="link-normal" href="#"  onclick="downloadKontrak()"><i
                        class="far fa-file-pdf fa-fw"></i>Download Kontrak</a>
                        <a class="link-normal"
                        href="#" onclick="addToChart()"><i class="fas fa-cart-plus fa-fw"></i>Tambah Ke Cart</a>
                    </div>
                    @endif
                    <div class="table-responsive table mb-0 pt-3">
                     <table class="table table-striped table-sm my-0 table-proyek" id="datatable">
                        <thead>
                           <tr>
                            @if($user_id !='')
                            <th class="text-center" ><input type="checkbox" id="selectAll"></th>
                            @endif
                            <th>No.</th>
                            <th>Nama Paket Pekerjaan</th>
                            <th>Lokasi Proyek</th>
                            <th>Waktu Pelaksanaan</th>
                        </tr>
                    </thead>
                    <tbody></tbody>

                </table>
            </div>
        </div>
    </div>
</section><!-- End: List Proyek -->

<div class="undisplay">
    <div id="viewPdf" class="">

    </div>
</div>
<div id="downloadPDF"></div>
</div>
</section>
</main><!-- Start: Footer Dark -->


@include('home.modal_tahun_terakhir')
@include('template.footer')

@endsection
@section('js')
<script src="{{ asset('ciriajasa-assets/js/home-script.js?version=1.1') }}"></script>
@include("proyek.xjs")
@include("proyek.zjs")
@include("chart.zjs")

<script type="text/javascript">
  $('#closeModalTahunTerakhir').click(function(){
    $('#modalTahunTerakhir').modal('hide')
})
  $(function () {
      $('[data-toggle="tooltip"]').tooltip()
  })
</script>
@endsection