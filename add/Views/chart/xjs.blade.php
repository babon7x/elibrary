<script src="{{asset('assets/dt_master.js?version=1.1')}}"></script>
<script src="{{asset('assets/data.js?version=1.1')}}"></script>
<script src="{{asset('jquery/jquery.form.min.js')}}"></script>
<script src="{{asset('jquery/jquery.jspdf.umd.min.js')}}"></script>

<script>
	
	$(".select2").select2({dropdownAutoWidth : true});
	$(".numberFormat").each(function() {
		new AutoNumeric(this,
		{
			watchExternalChanges: true,
			allowDecimalPadding: false
		});
	});
	$(".datepicker").datepicker({
		format: "dd M yyyy"
	});
	var baseurl = "{{ url('') }}"

	var page = '<?php echo($page); ?>';
	if(page == 'chart'){
		$('#header_chart').addClass('btn btn-primary text-white')

	}
	else if (page !='dashboard'){
		$('#header_view').addClass('btn btn-primary text-white')
	}

	var selectYearStart = $('#selectYearStart').val()
	var selectYear = $('#selectYear').val()
	var url = "{{ route('chart.list') }}";
	url = url.replace('/list','');
	var dataColum = [
	{id:null,
		"render": function ( data, type, full, meta ) {
			let baris =meta.row+1;
			let defaultContent = '<input type="checkbox" class="checkRow dt-id" is_top_proyek="'+full.top_proyek+'" data-id="'+full.proyek_id+'" data-nama-paket-pekerjaan="'+full.proyek.nama_paket_pekerjaan+'"></input>';
			return  defaultContent;
		}	
	},
	{id:null,
		"render": function ( data, type, full, meta ) {
			let baris =meta.row+1;
			let defaultContent = baris;
			return  defaultContent;
		}
	}];
	dataColum.push({data: 'proyek.nama_paket_pekerjaan', name: 'proyek.nama_paket_pekerjaan'})
	dataColum.push({data: 'proyek.lokasi_proyek', name: 'proyek.lokasi_proyek'})
	dataColum.push({id:null,'render': function ( data, type, full, meta ) {
		let tahunAwal = moment(full.proyek.waktu_pelaksanaan_mulai).format('yyyy');
		let tahunAkhir = moment(full.proyek.waktu_pelaksanaan_selesai).format('yyyy');
		if (tahunAwal == tahunAkhir){
			return  tahunAwal;
		}
		else{
			return tahunAwal+' - '+tahunAkhir
		}
	} });

	dtMasterProyek(url,dataColum,selectYearStart,selectYear,'');

	function listTopProyek(){
		dtMasterProyek(url+'/topproyek',dataColum,selectYearStart,selectYear,'');
	}

	function listAllProyek(){
		dtMasterProyek(url,dataColum,selectYearStart,selectYear,'');
	}

	

</script>