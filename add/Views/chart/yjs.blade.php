
<script>

	function downloadKontrak(){
		var checkes = []
		var rowid = ''
		var datas = {}


		$('#datatable tbody tr').each(function(i,v){
			// rowid = $(v).attr('rowid')
			if ($(v).children(':nth-child(1)').children().is(':checked') == true){
				rowid = $(v).children(':nth-child(1)').children().attr('data-id')
				checkes.push(rowid)
			}
		})

		if (checkes.length <1){
			Swal.fire({
				title: 'Gagal',
				text: 'Pilih Minimal 1 Data !',
				icon: 'warning',
				showCancelButton: false,
				confirmButtonText: 'OK',
				cancelButtonText: 'Ya',
				customClass: {
					confirmButton: 'btn btn-primary mr-2',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})
		}


		else{
			spinnerStart();
			datas = {ids:checkes}
			console.log(datas)

			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: "{{ route('download.kontrak') }}",
				method: 'post',
				data:datas,
				success: function(response){
					console.log(response)
					const link = document.createElement('a');
					link.setAttribute('href', baseurl+'/file_kontrak/kontrak_'+response+'.pdf');
					link.setAttribute('download', 'kontrak_'+response+'.pdf'); 
					link.click();

					$('.checkRow').prop('checked',false)
					$('#selectAll').prop('checked',false)
					spinnerEnd()
				},
				error: function(response){
					spinnerEnd()
					Swal.fire({
						title: 'Gagal',
						text: 'Terjadi Kesalahan !',
						icon: 'error',
						showCancelButton: false,
						confirmButtonText: 'OK',
						cancelButtonText: 'Ya',
						customClass: {
							confirmButton: 'btn btn-primary mr-2',
							cancelButton: 'btn btn-danger'
						},
						buttonsStyling: false
					})
				}
			})


		}
	}

	function downloadProyek2(idTarget){

		var checkes = []
		var checkesNamaPaketPekerjaan = []
		var rowid = ''
		var datas = {}
		$('.table-pdf').html('')

		$('#datatable tbody tr').each(function(i,v){
			rowid = $(v).attr('data-id')
			if ($(v).children(':nth-child(1)').children().is(':checked') == true){
				checkes.push($(v).children(':nth-child(1)').children().attr('data-id'))
				checkesNamaPaketPekerjaan.push($(v).children(':nth-child(1)').children().attr('data-nama-paket-pekerjaan'))
			}
		})


		if ((checkes.length <1) && (idTarget == '')) {
			Swal.fire({
				title: 'Gagal',
				text: 'Pilih Minimal 1 Data !',
				icon: 'warning',
				showCancelButton: false,
				confirmButtonText: 'OK',
				cancelButtonText: 'Ya',
				customClass: {
					confirmButton: 'btn btn-primary mr-2',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})
		}
		else{
			$('#modalBodyTahunTerakhir').html(
				'   <div class="form-group row">'+
				'<label for="inputPassword" class="col-sm-8 col-form-label text-center">Nama Paket Pekerjaan</label>'+
				'<label for="inputPassword" class="col-sm-4 col-form-label text-center">Tahun Terakhir</label>'+
				'</div>'
				)
			$.each(checkes,function(i,v){			
				$('#modalBodyTahunTerakhir').append(
					'<div class="form-group row">'+
					'<label for="'+v+'" class="col-sm-8 col-form-label text-end">'+checkesNamaPaketPekerjaan[i]+'</label>'+
					'<div class="col-sm-4">'+
					'<input type="number" class="form-control tahun-terakhir" id="'+v+'" >'+
					'</div>'+
					'</div></br>'
					)
			})
			$('#modalTahunTerakhir').modal('show')
		}

	}

	function validasiTahunTerakhir(){
		let cekValidasi = 0;
		$('.tahun-terakhir').each(function(i,v){
			if ($(v).val()==''){
				cekValidasi =1
			}
		})
		if (cekValidasi == 1){
			Swal.fire({
				title: 'Gagal',
				text: 'Harap Input Semua Tahun Terakhir !',
				icon: 'warning',
				showCancelButton: false,
				confirmButtonText: 'OK',
				cancelButtonText: 'Ya',
				customClass: {
					confirmButton: 'btn btn-primary mr-2',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})
			return false;
		}
		else {
			if ($('#projection_file').attr('data-nama-paket-pekerjaan') != undefined){
				downloadProyek($('#projection_file').attr('data-id'));
				$('#modalTahunTerakhir').modal('hide')
			}
			else{
				downloadProyek('');
				$('#modalTahunTerakhir').modal('hide')
			}
		}

	}

	function downloadProyek(idTarget){

		spinnerStart();

		var checkes = []
		var checkesNamaPaketPekerjaan = []
		var rowid = ''
		var datas = {}
		$('.table-pdf').html('')

		$('#datatable tbody tr').each(function(i,v){
			rowid = $(v).attr('rowid')
			if ($(v).children(':nth-child(1)').children().is(':checked') == true){
				checkes.push($(v).children(':nth-child(1)').children().attr('data-id'))

			}
		})

		if ((checkes.length <1) && (idTarget == '')) {
			Swal.fire({
				title: 'Gagal',
				text: 'Pilih Minimal 1 Data !',
				icon: 'warning',
				showCancelButton: false,
				confirmButtonText: 'OK',
				cancelButtonText: 'Ya',
				customClass: {
					confirmButton: 'btn btn-primary mr-2',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})

		}


		else{
			if (idTarget!=''){
				checkes.push(idTarget)
			}

			$('.tahun-terakhir').each(function(i,v){
				checkesNamaPaketPekerjaan.push($(v).val())
			})

			datas = {ids:checkes,tahunTerakhir:checkesNamaPaketPekerjaan}
			var templateRow=[]

			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: "{{ route('create.download.proyek') }}",
				method: 'post',
				data:datas,
				success: function(response){

					urut = 0;
					var tahunpertama = '';
					var tahunterakhir = '';
					var tahunsekarang = moment().year();

					var alpabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
					var bulan = ['','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];


					$('#viewPdf').html('')

					var biggest = Math.max.apply( null, checkesNamaPaketPekerjaan );

					var templateRow2 = '';
					var templateRow3 = '';
					var templateRow4 = '';


					$.each(response.data_proyek,function(i,v){
						urut = parseInt(i) + 1;
						var totalTenagaAsing = 0;
						var totalTenagaIndonesia = 0;
						var totalTenagaAhliAsing = 0;
						var totalTenagaAhliIndonesia = 0;

						var ruanglingkup = v.lingkup_produk_utama;
						ruanglingkup = ruanglingkup.replace(/[\n\r]+/g, ' ');

						var tanggalMulai = moment(v.waktu_pelaksanaan_mulai).format('DD');
						var tanggalSelesai = moment(v.waktu_pelaksanaan_selesai).format('DD');
						var tahunMulai = moment(v.waktu_pelaksanaan_mulai).format('yyyy');
						var tahunSelesai = moment(v.waktu_pelaksanaan_selesai).format('yyyy');
						var bulanMulai =bulan[moment(v.waktu_pelaksanaan_mulai).format('M')];
						var bulanSelesai =bulan[moment(v.waktu_pelaksanaan_selesai).format('M')];

						templateRow2 =
					'<header class="headprint">'+
					'<div class="headprint1">'+          
					'<span><u>PT. <nbsp></nbsp> CIRIAJASA <nbsp></nbsp> ENGINEERING <nbsp></nbsp> CONSULTANS</u></span>'+
					'<br>'+
					'<span style="font-size:12px !important;">Management, Engineering, Social, Development, Environmental</span>'+
					'</div>'+
					'<br>'+
					'<div class="headprint2">'+          
					'<span style="font-size: 20px !important;" class="tahunterakhir">PENGALAMAN PERUSAHAAN '+checkesNamaPaketPekerjaan[i]+' TAHUN TERAKHIR</span>'+
					'<br>'+
					'<span style="font-size: 14px;">YANG MENGGAMBARKAN KUALIFIKASI TERBAIK</span>'+
					'</div>'+


					'</header>'+
					'<hr id="garishead"></hr>'+
					// '<hr id="garisfoot"></hr>'+
					'<div class="footers">'+
					'pengalaman '+checkesNamaPaketPekerjaan[i]+' tahun terakhir'+
					'</div>'+
					
					'<div class="kotak">'+

					'<div class="row">'+

					'<div class="col1">1.</div>'+
					'<div class="col2">Pengguna Jasa</div>'+
					'<div class="col3">:</div>'+
					'<div class="col4">'+v.pengguna_jasa+'</div>'+

					'</div>'+

					'<div class="row">'+

					'<div class="col1">2.</div>'+
					'<div class="col2">Nama Paket Pekerjaan</div>'+
					'<div class="col3">:</div>'+
					'<div class="col4">'+v.nama_paket_pekerjaan+'</div>'+

					'</div>'+

					'<div class="row">'+

					'<div class="col1 ruanglingkup">3.</div>'+
					'<div class="col2 ruanglingkup2">Lingkup Produk Utama</div>'+
					'<div class="col3 ruanglingkup3">:</div>'+
					'<div class="col4 rl" id="ruanglingkup">'+ruanglingkup+'</div>'+

					'</div>'+

					'<div class="row">'+

					'<div class="col1 ruanglingkup">4.</div>'+
					'<div class="col2 ruanglingkup2">Lokasi Proyek</div>'+
					'<div class="col3 ruanglingkup3">:</div>'+
					'<div class="col4" id="ruanglingkup">'+v.lokasi_proyek+'</div>'+

					'</div>'+

					'<div class="row">'+

					'<div class="col1 ruanglingkup">5.</div>'+
					'<div class="col2 ruanglingkup2">Nilai Kontrak</div>'+
					'<div class="col3 ruanglingkup3">:</div>'+
					'<div class="col4" id="ruanglingkup">'+v.nilai_kontrak+'</div>'+

					'</div>'+

					'<div class="row">'+

					'<div class="col1 ruanglingkup">6.</div>'+
					'<div class="col2 ruanglingkup2">No. Kontrak</div>'+
					'<div class="col3 ruanglingkup3">:</div>'+
					'<div class="col4" id="ruanglingkup">'+v.nomor_kontrak+'</div>'+

					'</div>'+

					'<div class="row">'+

					'<div class="col1 ruanglingkup">7.</div>'+
					'<div class="col2 ruanglingkup2">Waktu Pelaksanaan</div>'+
					'<div class="col3 ruanglingkup3">:</div>'+
					'<div class="col4" id="ruanglingkup">'+tanggalMulai+' '+bulanMulai+' '+ tahunMulai +' s/d '+tanggalSelesai+' '+bulanSelesai+' '+ tahunSelesai+'</div>'+

					'</div>'+


					'<table class="table-print">'+
					'<tbody>'+

					

					'<tr class="tr1">'+
					'<td style="text-align:right;">8.</td>'+
					'<td colspan="2">Nama Pemimpin Kemitraan</td>'+
					'<td>:</td>'+
					'<td colspan="3">'+v.nama_pimpinan_kontrak+'</td>'+
					'</tr>'+

					'<tr>'+
					'<td></td>'+
					'<td colspan="2">(jika ada)</td>'+
					'<td></td>'+
					'<td colspan="3"></td>'+
					'</tr>'+

					'<tr>'+
					'<td></td>'+
					'<td colspan="2">Alamat</td>'+
					'<td>:</td>'+
					'<td colspan="3">'+v.alamat+'</td>'+
					'</tr>'+

					'<tr>'+
					'<td></td>'+
					'<td colspan="2">Negara Asal</td>'+
					'<td>:</td>'+
					'<td colspan="3">'+v.negara_asal+'</td>'+
					'</tr>';


					templateRow4 =
					'</tbody>'+
					'</tbody>'+
					'</div>';

					// $('#viewPdf').append(templateRow)
					templateRowPerusahaan = '';
					templateRowDetail ='';

					$.each(v.perusahaan_child,function(iPerusahaan,vPerusahaan){
						templateRowPerusahaan = templateRowPerusahaan +
						'<tr class="row_perusahaan detail-tr">'+
						'<td style="text-align:right;">'+alpabet[iPerusahaan]+'</td>'+
						'<td colspan="2">'+vPerusahaan.nama+'</td>'+
						'<td colspan="2" style="text-align: center;">'+vPerusahaan.asing+' Orang Bulan</td>'+
						'<td colspan="2" style="text-align: center;">'+vPerusahaan.indonesia+' Orang Bulan</td>'+
						'</tr>';
						totalTenagaAsing = totalTenagaAsing + parseInt(vPerusahaan.asing)
						totalTenagaIndonesia = totalTenagaIndonesia + parseInt(vPerusahaan.indonesia)
					})

					$.each(v.detail_child,function(iDetail,vDetail){
						templateRowDetail = templateRowDetail +
						'<tr class="row_detail">'+
						'<td width="80" style="text-align:right;">'+alpabet[iDetail]+'</td>'+
						'<td width="250" colspan="2">'+vDetail.posisi+'</td>'+
						'<td colspan="2">'+vDetail.keahlian+'</td>'+
						'<td colspan="2" style="text-align:center">'+vDetail.jumlah_orang+'</td>'+
						'</tr>';
						totalTenagaAhliIndonesia = totalTenagaAhliIndonesia + parseInt(vDetail.jumlah_orang)

					})

					templateRow3 = 
					'<tr class="tr1 tr9">'+
					'<td rowspan="2" style="text-align:right;">9.</td>'+
					'<td rowspan="2" colspan="2">Jumlah Tenaga Ahli</td>'+
					'<td></td>'+
					'<td >Tenaga Ahli Asing</td>'+
					'<td>:</td>'+
					'<td style="text-align:center;"> 0 Orang Bulan</td>'+
					'</tr>'+

					'<tr>'+
					'<td></td>'+
					'<td>Tenaga Ahli Indonesia</td>'+
					'<td >:</td>'+
					'<td id="totalTenagaAhliIndonesia0" style="text-align:center;">'+totalTenagaAhliIndonesia+' Orang Bulan</td>'+
					'</tr>'+

					'<tr class="tr1 tr10">'+
					'<td style="text-align:right;">10.</td>'+
					'<td colspan="2">Perusahaan Mitra Kerja</td>'+
					'<td colspan="4"  style="text-align: center;">Jumlah Tenaga Ahli</td>'+
					'</tr>'+

					'<tr class="row_perusahaan">'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
					'<td colspan="2" style="text-align: center;" >Asing</td>'+
					'<td colspan="2" style="text-align: center;">Indonesia</td>'+
					'</tr>';

					templateRow5 = 
					'<tr class="tenagaahli1 tr11">'+
					'<td style="text-align:right;">11.</td>'+
					'<td colspan="6">Tenaga Ahli yang Terlibat</td>'+
					'</tr>'+
					'<tr class="tenagaahli2">'+
					'<td></td>'+
					'<td colspan="2" style="text-align:center"> Posisi</td>'+
					'<td colspan="2" style="text-align:center"> Keahlian</td>'+
					'<td colspan="2" style="text-align:center"> Jumlah Orang</td>'+
					'</tr>';					

					templateRow2 = templateRow2 + templateRow3 + templateRowPerusahaan +templateRow5 + templateRowDetail + templateRow4;
					templateRow.push(templateRow2)			

				})


$('.checkRow').prop('checked',false)
$('#selectAll').prop('checked',false)
spinnerEnd()


},
error: function(response){
	spinnerEnd()
	Swal.fire({
		title: 'Gagal',
		text: 'Terjadi Kesalahan !',
		icon: 'error',
		showCancelButton: false,
		confirmButtonText: 'OK',
		cancelButtonText: 'Ya',
		customClass: {
			confirmButton: 'btn btn-primary mr-2',
			cancelButton: 'btn btn-danger'
		},
		buttonsStyling: false
	})
}
})
.done(function(data){
	html = $('#viewPdf').html()
	datas = {html:templateRow}
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url: "{{ route('download.proyek') }}",
		method: 'post',
		data:datas,
		success: function(response){
			const linkx = document.createElement('a');
			linkx.setAttribute('href', "{{ route('download.proyek.file') }}");
			linkx.setAttribute('target', '_blank');
			linkx.click();
		}
	})

})
}
}



</script>