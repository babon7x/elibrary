<script>

	$('#selectAll').click(function(){
		if ($(this).is(':checked') == true){
			$('.checkRow').prop('checked',true)
		}
		else{
			$('.checkRow').prop('checked',false)
		}
	})

	function addToChart(){
		var checkes = []
		var rowid = ''
		var datas = {}

		$('#datatable tbody tr').each(function(i,v){
			rowid = $(v).attr('rowid')
			if ($(v).children(':nth-child(1)').children().is(':checked') == true){
				checkes.push(rowid)
			}
		})

		if (checkes.length <1){
			Swal.fire({
				title: 'Gagal',
				text: 'Pilih Minimal 1 Data !',
				icon: 'warning',
				showCancelButton: false,
				confirmButtonText: 'OK',
				cancelButtonText: 'Ya',
				customClass: {
					confirmButton: 'btn btn-primary mr-2',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})
		}


		else{
			spinnerStart();
			datas = {ids:checkes}
			console.log(datas)

			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: "{{ route('chart.store') }}",
				method: 'post',
				data:datas,
				success: function(response){

					Swal.fire({
						title: 'Berhasil',
						text: 'Tambah Chart List !',
						icon: 'success',
						showCancelButton: false,
						confirmButtonText: 'OK',
						cancelButtonText: 'Ya',
						customClass: {
							confirmButton: 'btn btn-primary mr-2',
							cancelButton: 'btn btn-danger'
						},
						buttonsStyling: false
					})

					$('#chart_count').text(response)
					$('.checkRow').prop('checked',false)
					$('#selectAll').prop('checked',false)
					spinnerEnd()
				},
				error: function(response){
					spinnerEnd()
					Swal.fire({
						title: 'Gagal',
						text: 'Terjadi Kesalahan !',
						icon: 'error',
						showCancelButton: false,
						confirmButtonText: 'OK',
						cancelButtonText: 'Ya',
						customClass: {
							confirmButton: 'btn btn-primary mr-2',
							cancelButton: 'btn btn-danger'
						},
						buttonsStyling: false
					})
				}
			})


		}
	}


	function deleteChart(){
		var checkes = []
		var rowid = ''
		var datas = {}

		$('#datatable tbody tr').each(function(i,v){
			rowid = $(v).attr('rowid')
			if ($(v).children(':nth-child(1)').children().is(':checked') == true){
				checkes.push(rowid)
			}
		})

		if (checkes.length <1){
			Swal.fire({
				title: 'Gagal',
				text: 'Pilih Minimal 1 Data !',
				icon: 'warning',
				showCancelButton: false,
				confirmButtonText: 'OK',
				cancelButtonText: 'Ya',
				customClass: {
					confirmButton: 'btn btn-primary mr-2',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})
		}


		else{
			Swal.fire({
				title: 'Hapus Chart',
				text: 'Anda Yakin ?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Tidak',
				cancelButtonText: 'Ya',
				customClass: {
					confirmButton: 'btn btn-primary mr-2',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			}).then((result) => {
				if (!result.isConfirmed) {
					spinnerStart();
					datas = {ids:checkes}

					$.ajax({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
						url: "{{ route('chart.destroy','delete') }}",
						method: 'delete',
						data:datas,
						success: function(response){
							Swal.fire({
								title: 'Berhasil',
								text: 'Hapus Chart List !',
								icon: 'success',
								showCancelButton: false,
								confirmButtonText: 'OK',
								cancelButtonText: 'Ya',
								customClass: {
									confirmButton: 'btn btn-primary mr-2',
									cancelButton: 'btn btn-danger'
								},
								buttonsStyling: false
							})

							$('#chart_count').text(response)
							dtMasterProyek(url,dataColum,selectYearStart,selectYear,'');

							$('.checkRow').prop('checked',false)
							$('#selectAll').prop('checked',false)
							spinnerEnd()
						},
						error: function(response){
							spinnerEnd()
							Swal.fire({
								title: 'Gagal',
								text: 'Terjadi Kesalahan !',
								icon: 'error',
								showCancelButton: false,
								confirmButtonText: 'OK',
								cancelButtonText: 'Ya',
								customClass: {
									confirmButton: 'btn btn-primary mr-2',
									cancelButton: 'btn btn-danger'
								},
								buttonsStyling: false
							})
						}
					})

				}
			})
		}
	}


</script>		