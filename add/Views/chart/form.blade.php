@extends("template.app_fe")
@section('content')
<div class="container">
	<form action="/proyek" enctype="multipart/form-data" method="POST" id="formProyek" style="margin-top: 80px;">
		<div class="row header-data">
			<div class="col-md-12">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="text" name="id" class="undisplay" >
				<div class="form-group">
					<label class="control-label">upload cover proyek</label>
					<input type="file" class="form-control upload" name="upload_cover_proyek" value="" id="upload">
				</div>

				<div class="form-group">
					<label class="control-label">upload file kontrak</label>
					<input type="file" class="form-control upload" name="upload_file_kontrak" value="" id="upload2">
				</div>

				<div class="form-group">
					<label class="control-label">pengguna jasa</label>
					<textarea class="form-control" cols="30" rows="5" name="pengguna_jasa" ></textarea>
				</div>

				<div class="form-group">
					<label class="control-label">nama paket pekerjaan</label>
					<textarea class="form-control" cols="30" rows="5" name="nama_paket_pekerjaan" ></textarea>
				</div>


				<div class="form-group">
					<label class="control-label">lingkup produk utama</label><br>
					<textarea cols="30" rows="5" name="lingkup_produk_utama" class="summernote" ></textarea>
				</div>

				<div class="form-group">
					<label class="control-label">lokasi proyek</label>
					<input type="text" class="form-control" name="lokasi_proyek" >
				</div>

				<div class="form-group">
					<label class="control-label">nilai kontrak</label>
					<textarea class="form-control" cols="30" rows="5" name="nilai_kontrak" ></textarea>
				</div>

				<div class="form-group">
					<label class="control-label">nomor kontrak</label>
					<input type="text" class="form-control" name="nomor_kontrak" >
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">waktu pelaksanaan mulai</label>
							<input type="text" class="form-control datepicker" name="waktu_pelaksanaan_mulai" value="">
						</div>		
					</div>
					<div class="col-md-6">		
						<div class="form-group">
							<label class="control-label">waktu pelaksanaan selesai</label>
							<input type="text" class="form-control datepicker" name="waktu_pelaksanaan_selesai" value="">
						</div>
					</div>
				</div>


				<div class="form-group">
					<label class="control-label">nama pimpinan kontrak</label>
					<input type="text" class="form-control" name="nama_pimpinan_kontrak" >
				</div>

				<div class="form-group">
					<label class="control-label">alamat</label>
					<textarea class="form-control" cols="30" rows="5" name="alamat" ></textarea>
				</div>

				<div class="form-group">
					<label class="control-label">negara asal</label>
					<input type="text" class="form-control" name="negara_asal" >
				</div>

			</div>
		</div>

		<div class="row perusahaan">
			<div class="col-md-11">
				<h4>Perusahaan Mitra Kerja</h4>
			</div>
			<div class="col-md-1">
				<button type="button" onclick="addPerusahaan()" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></button>
			</div>
		</div>

		<div class="row perusahaan perusahaan-data" >
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">nama</label>
					<input type="text" class="form-control" name="nama[]" autocomplete="off" >
				</div>
			</div>
			<div class="col-md-4">

				<div class="form-group">
					<label class="control-label">asing</label>
					<input type="text" class="form-control numberFormat" name="asing[]" autocomplete="off" >
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label">indonesia</label>
					<input type="text" class="form-control numberFormat" name="indonesia[]" autocomplete="off" >
				</div>
			</div>
			<div class="col-md-1">
				<button type="button" onclick="removePerusahaan(this)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
			</div>
		</div>

		<div class="row detail">
			<div class="col-md-11">
				<h4>Tenaga Ahli Tetap Yang Terlibat</h4>
			</div>
			<div class="col-md-1">
				<button type="button" onclick="addDetail()" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></button>
			</div>
		</div>

		<div class="row detail detail-data" >
			<div class="col-md-4">
				<div class="form-group">
					<label class="control-label">posisi</label>
					<input type="text" class="form-control" name="posisi[]" autocomplete="off" >
				</div>
			</div>
			<div class="col-md-4">

				<div class="form-group">
					<label class="control-label">keahlian</label>
					<input type="text" class="form-control" name="keahlian[]" autocomplete="off" >
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label">jumlah orang bulan</label>
					<input type="text" class="form-control numberFormat" name="jumlah_orang[]" autocomplete="off" >
				</div>
			</div>
			<div class="col-md-1">
				<button type="button" onclick="removeDetail(this)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
			</div>
		</div>


		<div class="row">
			<div class="col-md-2">
				<button type="button" class="btn btn-outline-primary">kembali</button>
			</div>
			<div class="col-md-2">
				<button type="submit" class="btn btn-primary" >simpan</button>
			</div>
		</div>

	</form>

</div>

@include("template.footer")
@endsection
@section('js')
@include("proyek.yjs")
@include("proyek.zjs")
@endsection