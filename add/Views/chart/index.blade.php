@extends("template.app_fe")
@section('content')

@section('customStyles')
<style>
	.dropdown-toggle-no-caret:after {
		content: none !important;
	}

	#tableViewProyek>tbody>tr>td:nth-child(1) {
		width: 30%;
	}

	#tableViewProyek>tbody>tr {
		line-height: 3;
	}

	#viewProyek {
		margin-bottom: 15px;
	}

	#viewPDF {
		padding: 20px;
	}

	.dropdown-toggle-no-caret:after {
		content: none !important;
	}
	
</style>
@endsection

<main class="page landing-page main-content-page">
	<section class="clean-block about-us">
		<!-- Start: search -->
		@include("template.app_search")
		<!-- End: search -->
		<div class="container">
			<div class="row">
				<div class="col">
					@if($user_id != '')
					<div class="d-flex justify-content-md-end align-items-md-center action-table-links">
						<a class="link-normal" href="#" onclick="downloadProyek2('')"><i class="far fa-file-pdf fa-fw"></i>Download Project
							Sheet<br></a>
						<a class="link-normal" href="#" onclick="downloadKontrak()"><i class="far fa-file-pdf fa-fw"></i>Download Kontrak<br></a><!-- Start: Button Icon Round -->
						<div>
							<a onclick="deleteChart()" class="btn btn-danger btn-floating" role="button" data-bs-toggle="tooltip" data-bss-tooltip="" title="Hapus"><i class="far fa-trash-alt" style="margin-right: 0px;"></i></a>
						</div><!-- End: Button Icon Round -->
					</div>
					@endif
					<div class="table-responsive table mb-0 pt-3 pe-2">
						<table class="table table-striped table-sm my-0 table-proyek" id="datatable">
							<thead>
								<tr>
									<th><input type="checkbox" id="selectAll"></th>
									<th>No.</th>
									<th>Nama Paket Pekerjaan</th>
									<th>Lokasi Proyek</th>
									<th>Waktu Pelaksanaan</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="undisplay">
				<div id="viewPdf" class="">

				</div>
			</div>
			<div id="downloadPDF"></div>
		</div>
	</div>
</section>
</main>


@include('home.modal_tahun_terakhir')
@include('template.footer')


@endsection
@section('js')
@include("chart.xjs")
@include("chart.yjs")
@include("chart.zjs")

<script>
	$('#closeModalTahunTerakhir').click(function(){
		$('#modalTahunTerakhir').modal('hide')
	})
</script>

{{-- @include("proyek.zjs") --}}
@endsection