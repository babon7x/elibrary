@extends("template.app_fe")
@section('content')
<main class="page landing-page main-content-page">
	<section class="clean-block about-us">
		<!-- Start: search -->
		<div class="block-heading search-header with-bg mobile__plr-15 mobile__mb-0" style="padding-right: 20%;padding-left: 20%;">
			<form class="d-flex">
				<div class="input-group input-search-container" style="flex: 2;"><input class="form-control form-control-lg" id='cari' type="text" placeholder="Cari" name="keyword"><span class="input-group-text"><i class="icon ion-android-search"></i></span></div>
			</form>
		</div><!-- End: search -->
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="table-responsive table mb-0 pt-3 pe-2">
						<table class="table table-striped table-sm my-0" id="datatable">
							<thead>
								<tr>
									<th>No.</th>
									<th>Nama</th>
									<th>Perusahaan</th>
									<th>Email</th>
									<th>Status</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<style>
				.dropdown-toggle-no-caret:after{
					content:none !important;
				}

			</style>
		</div>
	</section>
</main><!-- Start: Footer Dark -->
@include("template.footer")
@endsection
@section('js')
@include("user.xjs")
@include("user.zjs")
@endsection