

<div class="form animated undisplay">

	<div class="row animated form-data">
		<div class="col-md-3">
			<a href="#" onclick="toFormSandi()">Ganti Kata Sandi</a>
		</div>
		<div class="col-md-6">
			<input type="text" store="id" class="undisplay">
			<div class="form-group">
				<label>Nama Lengkap</label>
				<input type="text" class="form-control" store="nama_lengkap" disabled="true">
			</div>
			<div class="form-group">
				<label>Perusahaan</label>
				<input type="text" class="form-control" store="perusahaan" disabled="true">
			</div>
			<div class="form-group">
				<label>Email</label>
				<input type="email" class="form-control" store="email" disabled="true">
			</div>
			<div class="form-group">
				<label>Status Pengguna</label>
				<select class="form-control" store="role_id" disabled="true">
					<option value="3">Admin</option>
					<option value="4">Internal</option>
				</select>
			</div>

			<button class="btn btn-primary form-control btn-edit" onclick="openFormEdit()">EDIT</button>

			<div class="row btn-edit-submit undisplay">
				<div class="col-md-6">
					<button class="btn btn-outline-primary form-control" onclick="closeFormEdit()">KEMBALI</button>
					
				</div>
				<div class="col-md-6">
					
					<button class="btn btn-primary form-control" onclick="saveFormEdit()">KIRIM</button>
				</div>
			</div>

		</div>
		<div class="col-md-3"></div>
	</div>

	<div class="row animated form-sandi undisplay">
		<div class="col-md-3">
			<a class="btn btn-secondary" href="#">Ganti Kata Sandi</a>
		</div>
		<div class="col-md-6">
			<input type="text" store="id" class="undisplay">
			<div class="form-group">
				<label>Kata Sandi Saat ini</label>
				<input type="password" class="form-control" store="password1">
			</div>
			<div class="form-group">
				<label>Kata Sandi Baru</label>
				<input type="password" class="form-control" store="password2">
			</div>
			<div class="form-group">
				<label>Konfirmasi Kata Sandi</label>
				<input type="password" class="form-control" store="password3">
			</div>
			<div class="row">
				<div class="col-md-6">
					<button class="btn btn-outline-primary form-control" onclick="toFormData()">KEMBALI</button>
				</div>
				<div class="col-md-6">
					<button class="btn btn-primary form-control" onclick="saveGantiSandi()">KIRIM</button>
				</div>
			</div>

		</div>
		<div class="col-md-3"></div>
	</div>

</div>

