@extends("template.app_fe")
@section('content')
<main class="page landing-page main-content-page">
	<section class="clean-block about-us">
		<div class="container">
			<!-- Start: required for first child -->
			<div class="block-heading">
				<h5 class="text-start" style="font-weight: bold;">Selamat Datang Di Profil Anda - {{ $datas->role->nama }}</h5>
			</div><!-- End: required for first child -->
			<p class="text-start">Informasi Pribadi</p>
			<h5>Harap dicatat, untuk 'update status' informasi di bawah ini, silakan hubungi layanan pelanggan kami melalui email berikut support@ciriajasaec.com</h5>
			<div class="mt-5">
				<h6>Pengaturan</h6>
				<div class="row edit-profile" id="editProfileContainer">
					<div class="col-12 col-md-3 mobile__mb-10"><button class="btn btn-primary text-white undisplay" id="gantiPassword" type="button">Ganti Kata Sandi</button></div>
					<div class="col-12 col-md-4">
						<!-- Start: Form Edit Profile -->
						<input type="text" store="id" class="undisplay" value="{{ $datas->id }}">
						<form id="formEditProfile">

							<div class="mb-3"><label class="form-label" for="userName">Nama</label><input class="form-control form-control-lg form-control" type="text" id="userName" store="nama_lengkap" value="{{ $datas->nama_lengkap }} "></div>
							<div class="mb-3"><label class="form-label" for="company">Perusahaan</label><input class="form-control form-control-lg form-control" type="text" id="company" store="perusahaan" value="{{ $datas->perusahaan }}"></div>
							<div class="mb-3"><label class="form-label" for="email">Email</label><input class="form-control form-control-lg form-control" type="text" id="email" store="email" disabled="true" value="{{ $datas->email }}"></div>
							@if($datas->role->id != 4)
							<div class="mb-3"><label class="form-label" for="userStatus">Status Pengguna</label>
								<select class="form-select form-select-lg" store="role_id" >
									<option value="{{ $datas->role_id }} " selected>{{ $datas->role->nama }}</option>
									<option value="3">Admin</option>
									<option value="4">Internal User</option>
								</select>
							</div>
							@else
							<div class="mb-3"><label class="form-label" for="userStatus">Status Pengguna</label>
								<select class="form-select form-select-lg" store="role_id" >
									<option value="{{ $datas->role_id }} " selected>{{ $datas->role->nama }}</option>
								</select>
							</div>
							@endif
							<!-- Start: Submit Button Edit Profile -->
							<div class="d-grid gap-2">
								<button class="btn btn-primary text-white my-button-primary" type="button" onclick="saveFormEdit()">Ubah</button>
							</div>
								<!-- End: Submit Button Edit Profile -->
						</form><!-- End: Form Edit Profile -->
						<!-- Start: Form Edit Password -->
						<form id="formEditPassword">
							<div class="mb-3"><label class="form-label" for="userName">Kata Sandi Saat Ini</label><input class="form-control form-control-lg" type="password" store="password1"></div>
							<div class="mb-3"><label class="form-label" for="company">Kata Sandi Baru</label><input class="form-control form-control-lg" type="password" store="password2"></div>
							<div class="mb-3"><label class="form-label" for="email">Konfirmasi Kata Sandi Baru</label><input class="form-control form-control-lg" type="password" store="password3"></div>
							<div class="row">
								<div class="col-md-6 d-grid"><button class="btn btn-outline-primary my-button-outline-primary" id="backToEditProfile" type="submit">Kembali</button></div>
								<div class="col-md-6 d-grid"><button class="btn btn-primary my-button-primary" type="button" onclick="saveGantiSandi()">Kirim</button></div>
							</div>
						</form><!-- End: Form Edit Password -->
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
@endsection
@section('js')
	<script src="{{ asset('ciriajasa-assets/js/akun.js') }}"></script>
	@include("user.xjs")
@endsection
