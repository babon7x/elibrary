<script src="{{ asset('assets/dt_master.js?version=1.1') }}"></script>
<script src="{{ asset('assets/data.js?version=1.1') }}"></script>

<script>
    $(".select2").select2({
        dropdownAutoWidth: true
    });
    $(".numberFormat").each(function() {
        new AutoNumeric(this, {
            watchExternalChanges: true,
            allowDecimalPadding: false
        });
    });
    $(".datepicker").datepicker({
        format: "dd M yyyy"
    });

    $('#header_user').addClass('btn btn-primary text-white')

    var baseurl = "{{ url('') }}"

    user_id = '<?php echo $user_id; ?>';
    target_id = '<?php echo $target_id; ?>';

    if (user_id == target_id) {
        $('#gantiPassword').removeClass('undisplay')
    }

    var selectYear = $('#selectYear').val()
    var url = "user";
    var dataColum = [{
        id: null,
        "render": function(data, type, full, meta) {
            let baris = meta.row + 1;
            let defaultContent = baris;
            return defaultContent;
        }
    }];
    dataColum.push({
        data: 'nama_lengkap',
        name: 'nama_lengkap'
    })
    dataColum.push({
        data: 'perusahaan',
        name: 'perusahaan'
    })
    dataColum.push({
        data: 'email',
        name: 'email'
    })
    dataColum.push({
        id: null,
        'render': function(data, type, full, meta) {

            if (full.aktif == '0') {
                return 'new';
            } else {
                if (full.role_id == '3') {
                    return 'admin';
                } else if (full.role_id == '4') {
                    return 'internal';
                }
            }
        }
    });
    dataColum.push({
        id: null,
        'render': function(data, type, full, meta) {
            if (full.aktif == '1') {
                // return '<div>'+
                // '<a href="/akun/'+full.id+'"><i class="fa fa-edit"></i></a>'+
                // '<a href="#" onclick="deleteDataUser(this)" data-id="'+full.id+'"><i class="fa fa-trash"></i></a>'+
                // '</div>'
                // <a href="/akun/${full.id}" class="btn btn-info text-white" role="button" style="margin-right: 10px;"><i class="fas fa-pencil-alt" style="margin-right: 0px;"></i></a>
                var urlx = '{{ route('akun', ':id') }}';
                urlx = urlx.replace(':id', full.id);
                return `
				<div>

				<a href="` + urlx + `" class="btn btn-info text-white" role="button" style="margin-right: 10px;"><i class="fas fa-pencil-alt" style="margin-right: 0px;"></i></a>
				<a onclick="deleteDataUser(this)" data-id="${full.id}"  class="btn btn-danger" role="button"><i class="far fa-trash-alt text-white" style="margin-right: 0px;"></i></a>
				</div>
				`
            } else {
                // return '<div>'+
                // '<a href="/akun/'+full.id+'"><i class="fa fa-edit"></i></a>'+
                // '<a href="#" onclick="deleteDataUser(this)" data-id="'+full.id+'"><i class="fa fa-trash"></i></a>'+
                // '<a href="#" onclick="approveUser(this)" data-id="'+full.id+'"><i class="fa fa-check"></i></a>'+
                // '</div>'
                var urlx = '{{ route('akun', ':id') }}';
                urlx = urlx.replace(':id', full.id);
                return `
				<div>
				<a href="` + urlx + `" class="btn btn-info text-white" role="button" style="margin-right: 10px;"><i class="fas fa-pencil-alt" style="margin-right: 0px;"></i></a>
				<a onclick="deleteDataUser(this)" data-id="${full.id}" class="btn btn-danger" role="button" style="margin-right: 10px;"><i class="far fa-trash-alt text-white" style="margin-right: 0px;"></i></a>
				<a href="#" onclick="approveUser(this)" data-id="${full.id}" data-email="${full.email}"><i class="fa fa-check"></i></a>
				</div>
				`
            }
        }
    });


    dtMaster(url, dataColum);

    // function toFormSandi(){
    // 	$('.form-data').removeClass('fadeInDown')
    // 	$('.form-data').addClass('undisplay')

    // 	$('.form-sandi').removeClass('undisplay')
    // 	$('.form-sandi').addClass('fadeInDown')
    // 	$('.form-sandi').show()

    // 	$('[store="password1"]').val('');
    // 	$('[store="password2"]').val('');
    // 	$('[store="password3"]').val('');
    // }

    // function toFormData(){
    // 	$('.form-sandi').removeClass('fadeInDown')
    // 	$('.form-sandi').addClass('undisplay')

    // 	$('.form-data').removeClass('undisplay')
    // 	$('.form-data').addClass('fadeInDown')

    // }

    function openFormEdit() {
        $('[store]').prop('disabled', false)
        $('.btn-edit').addClass('undisplay')
        $('.btn-edit-submit').removeClass('undisplay')
    }

    function closeFormEdit() {
        $('[store="nama_lengkap"]').prop('disabled', true)
        $('[store="perusahaan"]').prop('disabled', true)
        $('[store="email"]').prop('disabled', true)
        $('[store="role_id"]').prop('disabled', true)
        $('.btn-edit').removeClass('undisplay')
        $('.btn-edit-submit').addClass('undisplay')
    }

    function saveFormEdit() {
        var id = $('[store="id"]').val()
        var nama_lengkap = $('[store="nama_lengkap"]').val()
        var perusahaan = $('[store="perusahaan"]').val()
        var email = $('[store="email"]').val()
        var role_id = $('[store="role_id"]').val()

        if ((nama_lengkap == '') || (perusahaan == '') || (email == '') || (role_id == null)) {
            Swal.fire({
                title: 'Peringatan !',
                text: 'Harap isi semua data !',
                icon: 'warning',
                showCancelButton: false,
                confirmButtonText: 'OK',
                cancelButtonText: 'Ya',
                customClass: {
                    confirmButton: 'btn btn-primary mr-2',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })
            return false;
        }

        Swal.fire({
            title: 'Anda Yakin !',
            text: 'Simpan Perubahan ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Tidak',
            cancelButtonText: 'Ya',
            customClass: {
                confirmButton: 'btn btn-primary mr-2',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        }).then((result) => {
            if (!result.isConfirmed) {
                spinnerStart()
                datas = {
                    id: id,
                    nama_lengkap: nama_lengkap,
                    perusahaan: perusahaan,
                    email: email,
                    role_id: role_id
                }

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ route('user.update', 'ids') }}",
                    method: 'patch',
                    data: datas,
                    success: function(response) {

                        // closeFormEdit()
                        spinnerEnd()
                        Swal.fire({
                            title: 'Berhasil !',
                            text: 'Ubah Data ',
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'OK',
                            cancelButtonText: 'Ya',
                            customClass: {
                                confirmButton: 'btn btn-primary mr-2',
                                cancelButton: 'btn btn-danger'
                            },
                            buttonsStyling: false
                        })

                    }
                })
            }
        })
    }

    function saveGantiSandi() {
        var id = $('[store="id"]').val()
        var sandi_saat_ini = $('[store="password1"]').val()
        var sandi_baru = $('[store="password2"]').val()
        var sandi_baru_konfirm = $('[store="password3"]').val()

        if ((sandi_saat_ini == '') || (sandi_baru == '') || (sandi_baru_konfirm == '')) {
            Swal.fire({
                title: 'Peringatan !',
                text: 'Sandi Tidak Boleh Kosong !',
                icon: 'warning',
                showCancelButton: false,
                confirmButtonText: 'OK',
                cancelButtonText: 'Ya',
                customClass: {
                    confirmButton: 'btn btn-primary mr-2',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })
            return false;
        }
        Swal.fire({
            title: 'Anda Yakin !',
            text: 'Merubah Sandi ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Tidak',
            cancelButtonText: 'Ya',
            customClass: {
                confirmButton: 'btn btn-primary mr-2',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        }).then((result) => {
            if (!result.isConfirmed) {
                spinnerStart()
                datas = {
                    id: id,
                    sandi_saat_ini: sandi_saat_ini,
                    sandi_baru: sandi_baru,
                    sandi_baru_konfirm: sandi_baru_konfirm
                }
                console.log(datas)

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ route('ubahsandi') }}",
                    method: 'post',
                    data: datas,
                    success: function(response) {
                        spinnerEnd()
                        Swal.fire({
                            title: response.status,
                            text: response.pesan,
                            icon: response.icon,
                            showCancelButton: false,
                            confirmButtonText: 'OK',
                            cancelButtonText: 'Ya',
                            customClass: {
                                confirmButton: 'btn btn-primary mr-2',
                                cancelButton: 'btn btn-danger'
                            },
                            buttonsStyling: false
                        })

                        if (response.status == 'Berhasil') {
                            closeFormEdit()
                            $('#logout').click();
                        }
                    }
                })
            }
        })
    }

    $('#selectAll').click(function() {
        if ($(this).is(':checked') == true) {
            $('.checkRow').prop('checked', true)
        } else {
            $('.checkRow').prop('checked', false)
        }
    })



    $('#cari').on('keyup', function() {
        var value = $(this).val()
        $('#datatable').DataTable().columns([2, 3]).search(value).draw()
    })

    function deleteDataUser(ini) {

        let ids = [];
        ids.push($(ini).attr('data-id'));

        Swal.fire({
            title: 'Hapus Data',
            text: 'data tidak dapat dikembalikan !',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Tidak',
            cancelButtonText: 'Ya',
            customClass: {
                confirmButton: 'btn btn-primary mr-2',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        }).then((result) => {
            if (!result.isConfirmed) {

                let urlz = '{{ route('user.destroy', '') }}';

                spinnerStart();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: baseurl + '/user/delete',
                    method: 'delete',
                    data: {
                        ids: ids
                    },
                    success: function(response) {
                        Swal.fire({
                            title: 'Hapus Data',
                            text: 'berhasil !',
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'OK',
                            cancelButtonText: 'Ya',
                            customClass: {
                                confirmButton: 'btn btn-primary mr-2',
                                cancelButton: 'btn btn-danger'
                            },
                            buttonsStyling: false
                        })
                        dtMaster(url, dataColum);

                    }
                })
                spinnerEnd();
            }
        });

    }

    function approveUser(ini) {

        let ids = $(ini).attr('data-id');
        let email = $(ini).attr('data-email');

        Swal.fire({
            title: 'Approve User',
            text: 'apakah anda yakin ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Tidak',
            cancelButtonText: 'Ya',
            customClass: {
                confirmButton: 'btn btn-primary mr-2',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        }).then((result) => {
            if (!result.isConfirmed) {

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ route('approve_user') }}",
                    method: 'post',
                    data: {
                        ids: ids
                    },
                    success: function(response) {
                        Swal.fire({
                            title: 'Berhasil',
                            text: 'Email Terkirim ?',
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'OK',
                            cancelButtonText: 'Ya',
                            customClass: {
                                confirmButton: 'btn btn-primary mr-2',
                                cancelButton: 'btn btn-danger'
                            },
                            buttonsStyling: false
                        })
                        $('#new_user').text(response)
                        $('#new_user2').text(response)
                        dtMaster(url, dataColum);
                        spinnerEnd();
                    },
                    error: function(response) {
                        console.log('error')
						Swal.fire({
                            title: 'Gagal',
                            text: 'Email Gagal Terkirim ?',
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonText: 'OK',
                            cancelButtonText: 'Ya',
                            customClass: {
                                confirmButton: 'btn btn-primary mr-2',
                                cancelButton: 'btn btn-danger'
                            },
                            buttonsStyling: false
                        })
                    }

                })

            }
        });

    }
</script>
