<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class clearsidebar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:sidebar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'clear sidebar like a fresh project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $file = new Filesystem;
       $sidebar = Storage::get('sidebar/sidebar_default.php');
       Storage::disk('local')->put('sidebar/sidebar.php', $sidebar);

       $path = public_path() . '/../add/Views/template/sidebar.blade.php';
       $sidebar_template = Storage::get('sidebar/sidebar.blade.php');
       File::put($path, $sidebar_template);
   }
}
