<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('home');
Auth::routes();
// Route::group(['middleware' => ['auth']], function() {
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/updatedb', 'DashboardController@updateDB')->name('updatedb');
Route::post('/download/kontrak', 'DashboardController@downloadKontrak')->name('download.kontrak');
Route::post('/create/download/proyek', 'DashboardController@CreateDownloadProyek')->name('create.download.proyek');
Route::post('/download/proyek', 'DashboardController@downloadProyek')->name('download.proyek');
Route::get('/download/proyek/file', 'DashboardController@downloadProyekBeneran')->name('download.proyek.file');
Route::get('/download/proyek/file2', 'DashboardController@downloadProyekBeneran2')->name('download.proyek.file2');

Route::get('logout', '\Add\Controllers\Auth\LoginController@logout');

Route::resource('/menu', 'MenuController');
Route::post('/menu/getdata', 'MenuController@getData');
Route::post('/menu/storesidebar', 'MenuController@storeSidebar');
Route::post('/menu/newsidebar', 'MenuController@newSidebar');

//------//
Route::resource('/role', 'RoleController');
Route::post('/role/list', 'RoleController@list')->name('role.list');
Route::post('/role/getdata', 'RoleController@getData')->name('role.getdata');


//------//
Route::resource('/proyek', 'ProyekController');
Route::post('/proyek/list', 'ProyekController@list')->name('proyek.list');
Route::post('/proyek/getdata', 'ProyekController@getData')->name('proyek.getdata');
Route::post('/proyek/updates', 'ProyekController@updates')->name('proyek.updates');
Route::post('/proyek/remove', 'ProyekController@removeTopProyek')->name('proyek.remove');
Route::post('/proyek/settopproyek', 'ProyekController@setTopProyek')->name('proyek.settopproyek');
Route::post('/proyek/topproyek/list', 'ProyekController@listTopProyek')->name('proyek.topproyek.list');
Route::get('/detail-proyek/{id}', 'ProyekController@DetailProyek')->name('detail-proyek');

//------//
Route::resource('/user', 'UserController');
Route::post('/user/list', 'UserController@list');
Route::post('/user/getdata', 'UserController@getData');
Route::get('/akun/{id}', 'UserController@akun')->name('akun');
Route::post('/ubahsandi', 'UserController@ubahSandi')->name('ubahsandi');
Route::get('/lupasandi', 'UserController@lupaSandi')->name('lupasandi');
Route::post('/approve_user', 'UserController@approveUser')->name('approve_user');
Route::post('/reset_sandi', 'UserController@resetSandi')->name('reset_sandi');

Route::resource('/chart', 'ChartController');
Route::post('/chart/list', 'ChartController@list')->name('chart.list');
Route::post('/chart/getdata', 'ChartController@getData')->name('chart.getdata');

Route::post('aktivasi','MailController@kirimEmail');

//});//endfile