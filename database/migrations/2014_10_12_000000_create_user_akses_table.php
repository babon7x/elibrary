<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAksesTable extends Migration
{

    public function up()
    {
        Schema::create('user_akses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('users_id');
            $table->string('url')->nullable();
            $table->string('lihat')->nullable()->default('0');
            $table->string('tambah')->nullable()->default('0');
            $table->string('ubah')->nullable()->default('0');
            $table->string('hapus')->nullable()->default('0');
            $table->String('download')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_akses');
    }
}
