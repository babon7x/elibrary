<?php 
use Illuminate\Support\Facades\Schema; 
use Illuminate\Database\Schema\Blueprint; 
use Illuminate\Database\Migrations\Migration; 

class CreateChartTable extends Migration 
{
	public function up(){
		Schema::create('chart', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('user_id');
			$table->integer('proyek_id');
			$table->timestamps();
		});
	}
	public function down()
	{
		Schema::dropIfExists('chart');
	}
}