<?php 
use Illuminate\Support\Facades\Schema; 
use Illuminate\Database\Schema\Blueprint; 
use Illuminate\Database\Migrations\Migration; 

class CreateProyekPerusahaanTable extends Migration 
{
	public function up(){
		Schema::create('proyek_perusahaan', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('proyek_id');
			$table->String('nama')->nullable();
			$table->String('asing')->nullable();
			$table->String('indonesia')->nullable();
			$table->Integer('created_by')->nullable();
			$table->Integer('updated_by')->nullable();
			$table->Integer('is_deleted')->nullable()->default(0);
			$table->timestamps();
		});
	}
	public function down()
	{
		Schema::dropIfExists('proyek_perusahaan');
	}
}