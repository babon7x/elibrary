<?php 
use Illuminate\Support\Facades\Schema; 
use Illuminate\Database\Schema\Blueprint; 
use Illuminate\Database\Migrations\Migration; 

class CreateProyekTable extends Migration 
{
	public function up(){
		Schema::create('proyek', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->String('pengguna_jasa')->nullable();
			$table->String('nama_paket_pekerjaan')->nullable();
			$table->String('upload_cover_proyek')->nullable();
			$table->String('upload_file_kontrak')->nullable();
			$table->longText('lingkup_produk_utama')->nullable();
			$table->String('lokasi_proyek')->nullable();
			$table->String('nilai_kontrak')->nullable();
			$table->String('nomor_kontrak')->nullable();
			$table->Date('waktu_pelaksanaan_mulai')->nullable();
			$table->Date('waktu_pelaksanaan_selesai')->nullable();
			$table->String('nama_pimpinan_kontrak')->nullable();
			$table->String('alamat')->nullable();
			$table->String('negara_asal')->nullable();
			$table->Integer('created_by')->nullable();
			$table->Integer('updated_by')->nullable();
			$table->Integer('top_proyek')->nullable()->default(0);
			$table->Integer('is_deleted')->nullable()->default(0);
			$table->timestamps();
		});
	}
	public function down()
	{
		Schema::dropIfExists('proyek');
	}
}