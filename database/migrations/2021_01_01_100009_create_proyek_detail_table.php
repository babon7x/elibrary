<?php 
use Illuminate\Support\Facades\Schema; 
use Illuminate\Database\Schema\Blueprint; 
use Illuminate\Database\Migrations\Migration; 

class CreateProyekDetailTable extends Migration 
{
	public function up(){
		Schema::create('proyek_detail', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('proyek_id');
			$table->String('posisi')->nullable();
			$table->String('keahlian')->nullable();
			$table->String('jumlah_orang')->nullable();
			$table->Integer('created_by')->nullable();
			$table->Integer('updated_by')->nullable();
			$table->Integer('is_deleted')->nullable()->default(0);
			$table->timestamps();
		});
	}
	public function down()
	{
		Schema::dropIfExists('proyek_detail');
	}
}