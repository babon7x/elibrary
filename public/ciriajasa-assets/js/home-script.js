$(function(){
  $('.slider.responsive').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    customPaging: function(slider, i) {      
      return '<button class="custom-slick-dots" id=' + i + "></button>";
    },
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false
        }
      }
    ]
  });
  
  let titleProyekMaxHeight = 0;
  $('.slider .slick-slide .card-body .proyek-title').each(function(idx){
    if(titleProyekMaxHeight < $(this).height()){
      titleProyekMaxHeight = $(this).height();
    }
  });

  if(titleProyekMaxHeight > 0){
    $('.slider .slick-slide .card-body .proyek-title').height(titleProyekMaxHeight);
  }

  let totalTopProyek = $('#totalTopProyek').data('total');
 
  if(parseInt(totalTopProyek) <= 3){
    $('.slider.responsive .slick-dots').wrap('<div class="slick-dots-custom-wrapper invisible"></div>');
  }else{
    $('.slider.responsive .slick-dots').wrap('<div class="slick-dots-custom-wrapper"></div>');
  }

  $('<a href="#" class="prev-slider-btn" role="button" ><i class="icon ion-ios-arrow-thin-left"></i></a>').insertBefore('.slider.responsive .slick-dots');
  $('<a href="#" class="next-slider-btn" role="button" ><i class="icon ion-ios-arrow-thin-right"></i></a>').insertAfter('.slider.responsive .slick-dots');

  $('.slider.responsive .prev-slider-btn').on('click', function(e){
    e.preventDefault();
    $('.slider.responsive').slick("slickPrev");
  });

  $('.slider.responsive .next-slider-btn').on('click', function(e){
    e.preventDefault();
    $('.slider.responsive').slick("slickNext");
  });
})