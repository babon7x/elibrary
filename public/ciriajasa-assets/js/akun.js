$(function(){
    $('#editProfileContainer #gantiPassword').on('click', function(e){
        e.preventDefault();
        let gantiPasswordComp = $(this);
        gantiPasswordComp.parents('#editProfileContainer').addClass('edit-profile-password');
        gantiPasswordComp.parents('#editProfileContainer').removeClass('edit-profile');
        gantiPasswordComp.attr('disabled', true);
        // if(gantiPasswordComp.parents('#editProfileContainer').hasClass('edit-profile')){
        //     gantiPasswordComp.parents('#editProfileContainer').addClass('edit-profile-password');
        //     gantiPasswordComp.parents('#editProfileContainer').removeClass('edit-profile');
        // }
        // else{
        //     gantiPasswordComp.parents('#editProfileContainer').addClass('edit-profile');
        //     gantiPasswordComp.parents('#editProfileContainer').removeClass('edit-profile-password');
        // }
    });

    $('#editProfileContainer #backToEditProfile').on('click', function(e){
        e.preventDefault();
        let backToEditProfileComp = $(this);
        backToEditProfileComp.parents('#editProfileContainer').addClass('edit-profile');
        backToEditProfileComp.parents('#editProfileContainer').removeClass('edit-profile-password');
        $('#gantiPassword').removeAttr('disabled');
    });
})