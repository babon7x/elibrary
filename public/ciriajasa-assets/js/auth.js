$(function(){
    $('.toggle-password-icon').on('click', function(){
        if('password' == $('#password').attr('type')){
            $(this).removeClass('fa-eye-slash');
            $(this).addClass('fa-eye');
            $('#password').prop('type', 'text');
        }else{
            $(this).addClass('fa-eye-slash');
            $(this).removeClass('fa-eye');
            $('#password').prop('type', 'password');
        }
    });
})