$(document).ready(function() {
    let tableHomeProyek = $('.mydatatable').DataTable({
        dom: 'Bfrtip',
        buttons: [],
        scrollY: 300,
        scrollX: true,
        destroy:true,
        autoWidth: false,
        scrollCollapse: true,
        searching: false,
        paging: true,
        order: [],
        columnDefs: [
            {
                targets: [0], //last column
                orderable: false, //set not orderable
            },
        ],
    });
    $('.mydatatable').on('click', 'tbody td:not(:first-child)', function(){
        let data = tableHomeProyek.row(this).data();
        let dataId = $(data[0]).attr('data-id');
        window.location.href = `/detail-proyek.html?id=${dataId}`;
    });
   
});