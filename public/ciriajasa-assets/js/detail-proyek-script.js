$(document).ready(function() {
    let tableHomeProyek = $('.mydatatable').DataTable({
        dom: 'Bfrtip',
        buttons: [],
        scrollY: 300,
        scrollX: true,
        destroy:true,
        autoWidth: false,
        scrollCollapse: true,
        searching: false,
        paging: false,
        order: []
    });
});