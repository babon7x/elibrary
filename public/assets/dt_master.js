
function dtMasterProyek(url,dataColum,selectYearStart,selectYear,dataSearch){
	$('#datatable').dataTable().fnDestroy();
	$('#datatable').dataTable({
		// "dom":'<"row undisplay"<"col-md-6"B>><"row"<"col-md-4"l><"col-md-4 periode"><"col-md-4 text-right"f>>tr<"row justify-content-center"<"col-md-12"p>>',
		// "dom":'<"row undisplay"<"col-md-6">>'+
		// 'tr'+
		// '<"row"<"col-md-4"i><"col-md-4 periode"><"col-md-4 text-right"p>>',
		dom: 'Bfrtip',
		buttons: [],
		// "order": [[ 1, "desc" ]],
		"columnDefs": 
		[
		{
			targets: [0],
			width: 25,
			orderable: false,
			searchable: false
		},
		{
			targets: [1,2,3],
			searchable: true
		}
		],
		"pageLength": 15,
		"language": {
			"lengthMenu": '<select class="form-control-sm">'+
			'<option value="10">10</option>'+
			'<option value="20">20</option>'+
			'<option value="30">30</option>'+
			'<option value="40">40</option>'+
			'<option value="50">50</option>'+
			'<option value="-1">All</option>'+
			'</select> data',
			"processing":
			'<i class="fa fa-cog fa-spin"></i>',
			"emptyTable":"Data Kosong",
			"searchPlaceholder": "pencarian",
			"infoEmpty":"tidak ada yang ditampilkan",
			"search":"",
			"zeroRecords":"Pencarian Kosong",
			"info":"Menampilkan _START_ - _END_ dari _TOTAL_ items",
		},
		processing: true,
		// serverSide: true,
		// searching: false,
		autoWidth: false,
		scrollY: 300,
		scrollX: true,
		ajax: {
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: url+'/list',
			method: 'post',
			data:{selectYearStart:selectYearStart,selectYear:selectYear,dataSearch:dataSearch}
		},
		scrollCollapse: true,
		order: [],
		columns:dataColum,
		"fnDrawCallback": function( oSettings ) {dtAjax_setting()},
		'fnCreatedRow': function (nRow, aData, iDataIndex) {
			$(nRow).attr('rowid', aData.id);
			$(nRow).addClass('cursor-pointer');
		},
	});
	
	$('#datatable').on('click', 'tbody td:not(:first-child)', function(){
		let comp = $(this).parent('tr');
		isChart = $(this).closest('tr').children(':first-child').children().attr('data-id')
		let rowId = comp.attr('rowid');
		if (isChart != undefined){
			rowId = isChart
		}

		window.location.href = baseurl+'/detail-proyek/'+rowId;
	});

	$('#datatable_filter>label>input').addClass('undisplay')
}

function dtMaster(url,dataColum){
	$('#datatable').dataTable().fnDestroy();
	$('#datatable').dataTable({
		// "dom":'<"row undisplay"<"col-md-6"B>><"row"<"col-md-4"l><"col-md-4 periode"><"col-md-4 text-right"f>>tr<"row justify-content-center"<"col-md-12"p>>',
		// "dom":'<"row undisplay"<"col-md-6"B>>'+
		// 'tr'+
		// '<"row"<"col-md-4"i><"col-md-4 periode"><"col-md-4 text-right"p>>',
		dom: 'Bfrtip',
		buttons: [],
		// "order": [[ 1, "desc" ]],
		"columnDefs": 
		[
		{
			targets: [0],
			width: 25,
			orderable: false,
			searchable: false
		}
		],
		"pageLength": 10,
		"language": {
			"lengthMenu": '<select class="form-control-sm">'+
			'<option value="10">10</option>'+
			'<option value="20">20</option>'+
			'<option value="30">30</option>'+
			'<option value="40">40</option>'+
			'<option value="50">50</option>'+
			'<option value="-1">All</option>'+
			'</select> data',
			"processing":
			'<i class="fa fa-cog fa-spin"></i>',
			"emptyTable":"Data Kosong",
			"searchPlaceholder": "pencarian",
			"infoEmpty":"tidak ada yang ditampilkan",
			"search":"",
			"zeroRecords":"Pencarian Kosong",
			"info":"Menampilkan _START_ - _END_ dari _TOTAL_ items",
		},
		processing: true,
		serverSide: true,
		searching: false,
		autoWidth: false,
		scrollY: 300,
		scrollX: true,
		ajax: {
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: url+'/list',
			method: 'post',
			data:{}
		},
		scrollCollapse: true,
		order: [],
		columns:dataColum,
		"fnDrawCallback": function( oSettings ) {dtAjax_setting()},
		'fnCreatedRow': function (nRow, aData, iDataIndex) {
			$(nRow).attr('rowid', aData.id);
			//$(nRow).addClass('cursor-pointer');
		},
	})
}

function dtAjax_setting(){

	setTimeout(function() { 
		if ( $('.page-item').length == 3) {
			$('.dataTables_paginate').css('display','none');
		} else {
			$('.dataTables_paginate').css('display','block');
		}

		console.log()

		$('[is_top_proyek="1"]').parent().addClass('bg-primary-top');

		$('.dataTable td > input[type="checkbox"][is_top_proyek="0"]').parent().addClass('align-content-center not-top-proyek');

		$('ul.pagination').addClass('justify-content-center')
			// $('tbody tr').hover().css('cursor','pointer');
		}, 0);
}


function bannerClick(id){
	
	// $('#datatable.table-proyek tbody tr[rowid="'+id+'"] td:not(:first-child)').click();

	let comp = $(this).parent('tr');
	isChart = $(this).closest('tr').children(':first-child').children().attr('data-id')
	let rowId = id
	if (isChart != undefined){
		rowId = isChart
	}

	window.location.href = baseurl+'/detail-proyek/'+rowId;

}

$('#datatable.table-proyek').on('click', 'tbody tr td:not(:first-child)', function(e) {
	spinnerStart()
	let rowid=$(this).closest("tr").attr('rowid');
	// $('#rowid'+rowid).show();
	datas = {ids:rowid}


	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url: url+'/getdata',
		method: 'post',
		data:datas,
		success: function(response){

			
			$('#tableDetailViewProyek tbody ').html('')
			$('#viewProyek').removeClass('undisplay')

			let projection_file = response[0].nama_paket_pekerjaan.replace(' ','--');

			$('#projection_file').text(projection_file+'.pdf')
			$('#projection_file').attr('onclick','downloadProyek("'+response[0].id+'")')

			$('#kontrak_file').attr('download','file_kontrak_'+response[0].id+'.pdf')

			$('#cover_view').attr('src','cover_proyek/'+response[0].id+'.jpg')

			let waktu_pelaksanaan = moment(response[0].waktu_pelaksanaan_mulai).format('d MMMM yyyy')+' s/d '+
			moment(response[0].waktu_pelaksanaan_selesai).format('d MMMM yyyy')

			$('#nama_proyek').text(response[0].nama_paket_pekerjaan)
			$('#pengguna_jasa').text(response[0].pengguna_jasa)
			$('#nama_paket_pekerjaan').text(response[0].nama_paket_pekerjaan)
			$('#lingkup_produk_utama').text(response[0].lingkup_produk_utama)
			$('#lokasi_proyek').text(response[0].lokasi_proyek)
			$('#nilai_kontrak').text(response[0].nilai_kontrak)
			$('#nomor_kontrak').text(response[0].nomor_kontrak)
			$('#waktu_pelaksanaan').text(waktu_pelaksanaan)
			$('#nama_pimpinan_kontrak').text(response[0].nama_pimpinan_kontrak)
			$('#alamat').text(response[0].alamat)
			$('#negara_asal').text(response[0].negara_asal)
			$('#jumlahTenagaAsing').text(response[0].totalTenagaAsing+'  orang bulan')
			$('#jumlahTenagaIndonesia').text(response[0].totalTenagaIndonesia+'  orang bulan')
			$('#totalTenagaAsing').text(response[0].totalTenagaAsing+'  orang bulan')
			$('#totalTenagaIndonesia').text(response[0].totalTenagaIndonesia+'  orang bulan')

			$.each(response[0].detail_child,function(i,v){
				$('#tableDetailViewProyek tbody ').append(
					'<tr>'+
					'<td>'+v.posisi+'</td>'+
					'<td>'+v.keahlian+'</td>'+
					'<td>'+v.jumlah_orang+'</td>'+
					'</tr>'
					)
			})

			$(window).scrollTop(0);

			$('.checkRow').prop('checked',false)
			$('#selectAll').prop('checked',false)
			spinnerEnd()
		},
		error: function(response){
			spinnerEnd()
			Swal.fire({
				title: 'Gagal',
				text: 'Terjadi Kesalahan !',
				icon: 'error',
				showCancelButton: false,
				confirmButtonText: 'OK',
				cancelButtonText: 'Ya',
				customClass: {
					confirmButton: 'btn btn-primary mr-2',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})
		}
	})
})

function hideViewProyek(){
	$('#viewProyek').addClass('fadeOutUp')
	$('#viewProyek').addClass('undisplay')
}