function newData(){
	$('.list').removeClass('fadeInDown')
	$('.list').addClass('undisplay')

	$('.form').removeClass('undisplay')
	$('.form').addClass('fadeInDown')

	$('[store]').val('');
}

function backToList(){
	$('.list').removeClass('undisplay')
	$('.list').addClass('fadeInDown')

	$('.form').addClass('undisplay')
	$('.form').removeClass('fadeInDown')
}

function editData(ini){
	$('.list').removeClass('fadeInDown')
	$('.list').addClass('undisplay')

	$('.form').removeClass('undisplay')
	$('.form').addClass('fadeInDown')

	spinnerStart();

	let id = $(ini).attr('data-id');

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url: url+'/getdata',
		method: 'POST',
		data:{id:id},
		success: function(response){

			$.each(response[0],function(i,v){
				$('[store="'+i+'"]').val(v).change();

				if ($('[store="'+i+'"]').hasClass('select2')){
					$('[store="'+i+'"]').val(v)
				}
			})
		}
	})
	spinnerEnd();

}

function goSave(url){

	spinnerStart();

	let method = 'POST';
	let value='';
	let datas = {};
	let id = $('[store="id"]').val();
	let url2 = url;

	if (id != ''){
		url = url+'/'+id;
		method = 'PATCH';
		datas['id'] = id;
	}

	$('[store]').not('[store="id"]').each(function(i,v){
		value = $(v).val();
		if ($(v).hasClass('datepicker')){
			datas[$(v).attr('store')] = moment(value).format('yyyy-M-D')
		}
		else if ($(v).hasClass('numberFormat')){
			datas[$(v).attr('store')] = value.replace(',','')
		}
		else{
			datas[$(v).attr('store')] = value;
		}
	})

	console.log(datas,method)

// spinnerEnd()
// return false;


$.ajax({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	url: url,
	method: method,
	data:datas,
	success: function(response){

		console.log(response)
		notify('top', 'center', 'success', 'inverse', 'animated fadeInDown', 'animated fadeOutDown' , 'bg-success','berhasil','simpan data');

		$('[store]').not('[store="id"]').each(function(i,v){
			$(v).removeClass('is-invalid');
			$(v).next('.invalid-feedback').addClass('invalid-feedback');
		});

		backToList();
		dtMaster(url2,dataColum);
		spinnerEnd();

	},
	error: function(response){
		let errors = response.responseJSON.errors;
		let urutan = 0;
		let focus = '';

		$.each(errors,function(i,v){
			urutan=urutan+1;
			if (urutan==1){
				index_response = i;
				info_response = v[0];
			}
		})

		$('[store]').not('[store="id"]').each(function(i,v){
			$(v).removeClass('is-invalid');
			$(v).next('.invalid-feedback').addClass('invalid-feedback');

		});

		urutan = 0;
		$.each(errors,function(i,v){
			urutan=urutan+1;
			$('[store="'+i+'"]').addClass('is-invalid');
					// $('[store="'+i+'"]').next().text(v);
					if (urutan == 1){
						focus = i;
					}
				})

		notify('top', 'center', 'danger', 'inverse', 'animated fadeInDown', 'animated fadeOutDown' , 'bg-danger',index_response,info_response);

		spinnerEnd();
		$('[store="'+focus+'"]').focus();
	}
})
}

function deleteData(ini){

	let ids=[];
	ids.push($(ini).attr('data-id'));

	Swal.fire({
		title: 'Hapus Data',
		text: 'data tidak dapat dikembalikan !',
		icon: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Tidak',
		cancelButtonText: 'Ya',
		customClass: {
			confirmButton: 'btn btn-primary mr-2',
			cancelButton: 'btn btn-danger'
		},
		buttonsStyling: false
	}).then((result) => {
		if (!result.isConfirmed) {

			spinnerStart();
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: url+'/ids',
				method: 'delete',
				data: {ids:ids},
				success: function(response){
					notify('top', 'center', 'success', 'inverse', 'animated fadeInDown', 'animated fadeOutDown' , 'bg-success','berhasil','hapus data');

					dtMaster(url,dataColum);

				}
			})
			spinnerEnd();
		}
	});

}