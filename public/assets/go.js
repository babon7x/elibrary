

function goTo(url){

	$('#app').append(
		'<div class="modal-backdrop-dashboard" style="background:var(--light);opacity: 0.5;">'+
		'<div class="loading_proses" width="device-width" height="2000">'+
		'<svg class="circular" viewBox="25 25 50 50">'+
		'<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />'+
		'</svg>'+
		'</div>'+
		'</div>'
		)

	$("body").on("mousemove",function(event) {
		if (event.pageX ==0) {
			$('#sidebar').addClass('active');
			$('.sticky-sidebar').css('z-index','99');

		}
		if (event.pageX >=350){
			$('#sidebar').removeClass('active');		
			$('.sticky-sidebar').css('z-index','0');
			$('.sidebar-list2 > * ul').removeClass('show');
		}
	});

	if ($('#sidebar').hasClass('active')){
		$('#sidebar').toggleClass('active');
	}

	$.get(url,function (data) {
		goToResponse = data;
		$(".body-content").html(data.view);
		console.log(data)	
		
		// $('.select2').select2();
		// $('.selectpicker').selectpicker();

		// $('.selectpicker').multiSelect();
		// $('.input-mask').mask('000.000.000.000.000', {reverse: true});
		// runSelect2(true)

		// $('.date-picker').flatpickr({
		// 	altInput: true,
		// 	altFormat: "j F Y",
		// 	dateFormat: "Y-m-d",
		// });

		$('.modal-backdrop').remove();
		$('.modal-backdrop-dashboard').remove();
		
	});
}


function goGantiPassword(url,id){
	let method = 'GET';
	let datas = {};
	datas['id'] = id;
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url: url+'/'+id,
		method: method,
		data:datas,
		success: function(response){
			$(".preloaderlarabon").show();
			$(".content-body").html(response.view);

			$(".preloaderlarabon").fadeOut();
		}
	})

}

function goShow(ini){
	spinnerStart();
	let id = $(ini).attr('data-id');

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url: url+'/getdata',
		method: 'POST',
		data:{id:id},
		success: function(response){

			$('#dataBaru').click();
			$.each(response[0],function(i,v){
				$('[store="'+i+'"]').val(v).change();

				if ($('[store="'+i+'"]').hasClass('select2')){
					// console.log($('[store="'+i+'"]').val())
					$('[store="'+i+'"]').val(v)
				}
			})
			$('.modal-backdrop-dashboard').remove();
		}
	})
	spinnerEnd();
}

function goStore(url){
	
	spinnerStart();
	let index_response = '';
	let info_response = '';
	let method = 'POST';
	let value='';
	let datas = {};
	let id = $('[store="id"]').val();
	let url2 = url;

    
	if (id != ''){
		url = url+'/'+$('[store="id"]').val();
		method = 'PATCH';
		datas['id'] = id;

		// let urut = datas.arrayindexOf('created_by:%');
		// datas.splice(urut,1);

		// delete datas.created_by;
	}


	$('[store]').not('[store="id"]').each(function(i,v){
		value = $(this).val();

		if ($(this).hasClass('select2')){
			let value_select = '';
			let text_select=$(this)
			.next('.select2-container')
			.children('.selection')
			.children('.select2-selection')
			.children()
			.attr('title');

			$(this).children().each(function(i2,v2){
				if (text_select == $(v2).text()){
					value_select = $(v2).val();
				}
			})
			value = value_select;
		}
		datas[$(this).attr('store')] = value;
	})

	// let arr = new Array(created_by: 1);\
	// var arr = {created_by: 1};
	// var c = datas.concat(arr);

	// console.log(datas);

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url: url,
		method: method,
		data:datas,
		success: function(response){
			index_response='Berhasil';
			info_response='simpan data';
			$('button[data-dismiss]').click();
			if (id == ''){
				notify('top', 'center', 'success', 'inverse', 'animated fadeInDown', 'animated fadeOutDown' , 'bg-success',index_response,info_response);
			}
			else{
				notify('top', 'center', 'success', 'inverse', 'animated fadeInDown', 'animated fadeOutDown'  , 'bg-success',index_response,info_response);
			}

			$('[store]').not('[store="id"]').each(function(i,v){
				$(v).removeClass('is-invalid');
				$(v).next('.invalid-feedback').addClass('invalid-feedback');
			});

			var dt = $('#datatable');
			
			if (dt != undefined){
				dtAjaxMaster(url2+'/list',dataColum);
			}

			$('.modal-backdrop-dashboard').remove();
			spinnerEnd();

		},
		error: function(response){
			let errors = response.responseJSON.errors;
			let urutan = 0;

			$.each(errors,function(i,v){
				urutan=urutan+1;
				if (urutan==1){
					index_response = i;
					info_response = v[0];
				}
			})

			$('[store]').not('[store="id"]').each(function(i,v){
				$(v).removeClass('is-invalid');
				$(v).next('.invalid-feedback').addClass('invalid-feedback');

			});

			$.each(errors,function(i,v){
				$('[store="'+i+'"]').addClass('is-invalid');
				$('[store="'+i+'"]').next().text(v);
			})
			if (id == ''){
				notify('top', 'center', 'danger', 'inverse', 'animated fadeInDown', 'animated fadeOutDown' , 'bg-danger',index_response,info_response);
			}
			else{
				notify('top', 'center', 'danger', 'inverse', 'animated fadeInDown', 'animated fadeOutDown' , 'bg-danger',index_response,info_response);
			}
			$('.modal-backdrop-dashboard').remove();
			spinnerEnd();
		}
	})
}

function goDeleteAll(){

	let ids=[];
	let timerInterval

	$('table tbody tr').each(function(i,v){
		if ($(v).children('td:first').children('input:first').is(':checked')) {
			ids.push($(v).children('td:first').children('input:first').attr('id'));				
		}
	})

	if (ids.length == 0){
		okAlert('Peringatan','centang minimal 1 data!','warning');
	} 
	else{
		Swal.fire({
			title: 'Hapus Data',
			text: 'data tidak dapat dikembalikan !',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Tidak',
			cancelButtonText: 'Ya',
			customClass: {
				confirmButton: 'btn btn-primary m-r-20',
				cancelButton: 'btn btn-danger'
			},
			buttonsStyling: false
		}).then((result) => {
			if (!result.isConfirmed) {

				Swal.fire({
					title: 'Harap Tunggu',
					html: 'memproses . . .',
					timer: 5000,
					timerProgressBar: true,
					didOpen: () => {
						Swal.showLoading();

						$.ajax({
							headers: {
								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							},
							url: url+'/ids',
							method: 'delete',
							data: {ids:ids},
							success: function(response){
								$('#datatable').DataTable().ajax.reload();
								Swal.hideLoading();
								clearInterval(timerInterval)

								Swal.fire({
									title: 'Berhasil',
									text: 'menghapus '+ids.length+' data',
									icon:'success',
									showConfirmButton:false,
									timer:1500
								});

							}
						});

						timerInterval = setInterval(() => {
							const content = Swal.getContent()
							if (content) {
								const b = content.querySelector('b')
								if (b) {
									b.textContent = Swal.getTimerLeft()
								}
							}
						}, 100)
					},
					willClose: () => {
						clearInterval(timerInterval)
					}
				}).then((result) => {
					/* Read more about handling dismissals below */
					if (result.dismiss === Swal.DismissReason.timer) {
						console.log('I was closed by the timer')
					}
				})
			}
		});
	}


}

function goDelete(ini){

	let url =$('#page').attr('table');
	let ids=[];
	ids.push($(ini).attr('data-id'));

	Swal.fire({
		title: 'Hapus Data',
		text: 'data tidak dapat dikembalikan !',
		icon: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Tidak',
		cancelButtonText: 'Ya',
		customClass: {
			confirmButton: 'btn btn-primary m-r-20',
			cancelButton: 'btn btn-danger'
		},
		buttonsStyling: false
	}).then((result) => {
		if (!result.isConfirmed) {

			spinnerStart();
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: url+'/ids',
				method: 'delete',
				data: {ids:ids},
				success: function(response){
					notify('top', 'center', 'success', 'inverse', 'animated fadeInDown', 'animated fadeOutDown' , 'bg-success','berhasil','hapus data');

					dt = $('#datatable');
					if (dt != undefined){
						dtAjaxMaster(url+'/list',dataColum);
					}
					$('.modal-backdrop-dashboard').remove();

				}
			})
			spinnerEnd();
		}
	});

}