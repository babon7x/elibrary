function notify(from, align, icon, type, animIn, animOut ,bg ,index_response,info_response){
    let iconClass='';
    if(icon=='success'){iconClass='fa fa-check'}
        else if(icon=='danger'){iconClass='fa fa-times'}
            else if(icon=='warning'){iconClass='fa fa-exclamation'}

                $.notify({
                    icon: icon,
        // title: title,
        // message: message,
        url: ''
    },
    {
        element: 'body',
        type: type,
        allow_dismiss: true,
        placement: {
            from: from,
            align: align
        },
        offset: {
            x: 15, // Keep this as default
            y: 80  // Unless there'll be alignment issues as this value is targeted in CSS
        },
        spacing: 10,
        z_index: 10031,
        delay: 1500,
        timer: 1000,
        url_target: '_blank',
        mouse_over: false,
        animate: {
            enter: animIn,
            exit: animOut
        },
        template:

        '<div class="card card-'+icon+' toastx toastx--'+icon+' " onclick="closeToast(this)">'+
        
        '<div class="row">'+
        
        '<div class="col-md-3">'+
        '<i class="'+iconClass+' text-'+icon+'" style="padding:20px;"></i>'+         
        '</div>'+
        
        '<div class="col-md-6 text-center text-'+icon+' text-bold" style="padding:10px;font-size:16px;">'+
        index_response+'<br>'+info_response+        
        '</div>'+

        '<div class="col-md-3">'+
        '</div>'+
        
        '</div>'+
        '</div>'
    });
        }

        function closeToast(ini){
            $(ini).hide()
        }
