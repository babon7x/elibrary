<h3>Created By babon7x@gmail.com</h3>

## Requirements

- php 7.3.
- mysql.
- composer.
- browser.

## Run This after u clone

- setting env file.
- composer update / composer install.
- composer dump-autoload.
- php artisan key:generate.
- php artisan db:create.
- php artisan generator:create.
- php artisan clear:file.
- php artisan optimize.
- php artisan migrate:refresh --seed.
- php artisan serv.



